<?php

use TYPO3\CMS\Core\Tests\Exception;

require_once('lib/RegionHandler.php');
require_once('lib/CategoryHandler.php');
require_once('lib/FeatureHandler.php');
require_once('lib/RoomHandler.php');
require_once('lib/AccommodationHandler.php');
require_once('lib/WildeastConnector.php');

$host = 'db1193.mydbserver.com';
$username = 'p270890d1';
$password = 'uteqaPes.466';
$database = 'usr_p270890_1';
//$db_connect = new mysqli($host, $username, $password, $database);

$host = 'localhost';
$username = 'root';
$password = 'keins';
$database = 'chillio';
$db_connect = new mysqli($host, $username, $password, $database);

//$import_type = $_GET['import'];
$import_type = 'update_rooms';

$result = $db_connect->query('SELECT session_id FROM tx_tmchillio_domain_model_wildeastsession order by uid desc limit 1;');
$wildeastSessionId = $result->fetch_object()->session_id;

if(mysqli_connect_errno()) {
	die('connection failed');
}
$wildeastConnector = new WildeastConnector();

switch($import_type) {
	case 'regions':
		$regions = $wildeastConnector->wildeastRequest('get[region]', Array());

		$regionHandling = new RegionHandler($db_connect);
		$regionHandling->saveRegions($regions);
		header('Location: index.php');
		break;
	case 'categories':
		$params = Array();
		$params['type'] = 'rooms';
		$params['language_id'] = 1;
		$categories = $wildeastConnector->wildeastRequest('get[categories]', $params);

		$categoryHandling = new CategoryHandler($db_connect);
		$categoryHandling->saveCategories($categories, 'room');

		$params['type'] = 'objects';
		$categories = $wildeastConnector->wildeastRequest('get[categories]', $params);

		$categoryHandling->saveCategories($categories, 'accommodation');
		header('Location: index.php');
		break;
	case 'features':
		$params = Array();

		$features = $wildeastConnector->wildeastRequest('get[facilities]', $params);

		$featureHandling = new FeatureHandler($db_connect);
		$featureHandling->saveFeatures($features);
		header('Location: index.php');
		break;
	case 'rooms':
		$result = $db_connect->query('SELECT * FROM zimmer_ids');

		$roomIdsForRequest = Array();
		$params = Array();
		$params['para'] = Array(
			'session_id' => $wildeastSessionId,
			'item' => Array(
				'o_id' => 1,
				'z_bezeichnung' => 1,
				'z_bemerkung' => 1,
				'z_bild' => 1,
				'k_min_personen' => 1,
				'k_max_personen' => 1,
				'k_bedrooms' => 1,
				'k_ausstattung' => 1,
				'ab_preis' => 1,
				'k_normal_personen' => 1,
				'k_normal_personen_max' => 1,
			)
		);

		$i = 1;
		$roomHandler = new RoomHandler($db_connect);

		while($obj = $result->fetch_object()) {
			$roomIdsForRequest[$obj->zimmer_id] = 1;
			if($i%100 == 0) {
				$params['para']['room'] = $roomIdsForRequest;

				//echo '<p>------------------------------------</p>';
				$roomInformation = $wildeastConnector->wildeastRequest('get[rooms]', $params);

				$roomHandler->saveRooms($roomInformation);

				$roomIdsForRequest = Array();
			}
			$i++;
			//if($i > 200) throw new Exception('stop');
		}
		//throw new Exception('stop');
		break;
	case 'update_rooms':
		$result = $db_connect->query('SELECT * FROM zimmer_ids');

		$roomIdsForRequest = Array();
		$params = Array();
		$params['para'] = Array(
			'session_id' => $wildeastSessionId,
			'item' => Array(
				'o_id' => 1,
				'z_bezeichnung' => 1,
				'z_bemerkung' => 1,
				'z_bild' => 1,
				'k_min_personen' => 1,
				'k_max_personen' => 1,
				'k_bedrooms' => 1,
				'k_groesse' => 1,
				'k_ausstattung' => 1,
				'ab_preis' => 1,
				'k_normal_personen' => 1,
				'k_normal_personen_max' => 1,
			)
		);

		$i = 1;
		$roomHandler = new RoomHandler($db_connect);

		while($obj = $result->fetch_object()) {
			$roomIdsForRequest[$obj->zimmer_id] = 1;
			if($i%100 == 0) {
				$params['para']['room'] = $roomIdsForRequest;

				//echo '<p>------------------------------------</p>';
				$roomInformation = $wildeastConnector->wildeastRequest('get[rooms]', $params);

				$roomHandler->updateRooms($roomInformation);

				$roomIdsForRequest = Array();
			}
			$i++;
			//if($i > 200) throw new Exception('stop');
		}
		//throw new Exception('stop');
		break;
	case 'accommodations':
		$result = $db_connect->query('SELECT * FROM zimmer_ids');

		$roomIdsForRequest = Array();
		$params = Array();
		$params['para'] = Array(
			'session_id' => $wildeastSessionId,
			'item' => Array(
				'o_id' => 1,
				'o_bezeichnung' => 1,
				'o_strasse' => 1,
				'o_plz' => 1,
				'o_ort' => 1,
				'o_bemerkung' => 1,
				'o_category' => 1,
				'o_gps' => 1,
				'o_bild' => 1,
				'o_haustiere_erlaubt' => 1,
				'o_ausstattung' => 1,
			)
		);

		$i = 1;
		$accommodationHandler = new AccommodationHandler($db_connect);

		while($obj = $result->fetch_object()) {
			$roomIdsForRequest[$obj->zimmer_id] = 1;
			if($i%100 == 0) {
				$params['para']['room'] = $roomIdsForRequest;

				//echo '<p>------------------------------------</p>';
				$roomInformation = $wildeastConnector->wildeastRequest('get[rooms]', $params);

				$accommodationHandler->saveAccommodations($roomInformation);

				$roomIdsForRequest = Array();
			}
			$i++;
			//if($i > 200) throw new Exception('stop');
		}
		//throw new Exception('stop');
		break;
	default:
		echo '<p><a href="index.php?import=regions">Regionen aktualisieren</a></p>';
		echo '<p><a href="index.php?import=categories">Kategorien aktualisieren</a></p>';
		echo '<p><a href="index.php?import=features">Features aktualisieren</a></p>';
		echo '<p>
				<form action="index.php">
					<input type="hidden" name="import" value="rooms" />
					<input type="text" name="wildeastSessionId" placeholder="Wildsession ID eintragen" />
					<input type="submit" value="Wohneinheiten cachen" />
				</form>
			</p>';
		break;
}
