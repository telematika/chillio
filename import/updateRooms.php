<?php

use TYPO3\CMS\Core\Tests\Exception;

require_once('lib/AccommodationHandler.php');
require_once('lib/RoomHandler.php');
require_once('lib/WildeastConnector.php');

$host = 'db1193.mydbserver.com';
$username = 'p270890d1';
$password = 'uteqaPes.466';
$database = 'usr_p270890_1';
//$db_connect = new mysqli($host, $username, $password, $database);

$host = 'localhost';
$username = 'root';
$password = 'keins';
$database = 'chillio';
$db_connect = new mysqli($host, $username, $password, $database);

if(mysqli_connect_errno()) {
	die('connection failed');
}

// init connector
$wildeastConnector = new WildeastConnector();
// create a wildeast session id
$wildeastSessionId = $wildeastConnector->getWildeastSessionId($wildeastConnector);

$db_connect->query("SET NAMES utf8");

// prepare params, which have to get saved
$params['para'] = Array(
	'session_id' => $wildeastSessionId,
	'item' => Array(
		'o_id' => 1,
		'o_bezeichnung' => 1,
		'o_strasse' => 1,
		'o_plz' => 1,
		'o_ort' => 1,
		'o_bemerkung' => 1,
		'o_category' => 1,
		'o_gps' => 1,
		'o_haustiere_erlaubt' => 1,
		'o_ausstattung' => 1,
		'z_bezeichnung' => 1,
		'z_bemerkung' => 1,
		'k_min_personen' => 1,
		'k_max_personen' => 1,
		'k_bedrooms' => 1,
		'k_groesse' => 1,
		'k_ausstattung' => 1,
		'ab_preis' => 1,
		'k_normal_personen' => 1,
		'k_normal_personen_max' => 1,
	)
);

$roomHandler = new RoomHandler($db_connect);

// get all saved room ids
$result = $db_connect->query('SELECT room_id FROM tx_tmchillio_domain_model_room');
$roomIdsForRequest = Array();
$i = 1;

// loop through all and combine 100 room ids to one request
while($obj = $result->fetch_object()) {
	$roomIdsForRequest[$obj->room_id] = 1;
	if($i%100 == 0) {
		$params['para']['room'] = $roomIdsForRequest;

		$roomInformation = $wildeastConnector->wildeastRequest('get[rooms]', $params);

		$roomHandler->updateRooms($roomInformation);

		$roomIdsForRequest = Array();
	}
	$i++;
	//if($i > 200) throw new Exception('stop');
}
