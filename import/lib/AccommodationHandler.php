<?php

class AccommodationHandler {

	/**
	 * @var string
	 */
	protected $tableName = '';

	/**
	 * @var mysqli
	 */
	protected $db_connect = null;

	protected $uid = 0;

	/**
	 * @param mysqli $db_connect
	 */
	public function __construct(mysqli $db_connect = null) {
		$this->db_connect = $db_connect;
		$this->tableName = 'tx_tmchillio_domain_model_accommodation';
	}

	/**
	 * @param $roomInformation
	 */
	public function saveAccommodations($roomInformation) {

		$roomsInformation = json_decode(json_encode($roomInformation), 1);
		//var_dump($roomsInformation);

		$o_id = $roomInformation['o_id'];
		foreach ( $roomsInformation['content']['room'] as $key => $roomInformation ) {
			if($o_id == $roomInformation['o_id'] && $key > 0) {
				continue;
			}
			else {
				$o_id = $roomInformation['o_id'];
			}
			$insertSQL = 'INSERT IGNORE INTO ';
			$insertSQL .= $this->tableName;
			$insertSQL .= ' (pid, object_id, title, street, plz, city, remark, pets, longitude, latitude, tstamp, crdate)';
			$insertSQL .= " VALUES";

			//$insertSQL .= ($key > 0 ? "," : "");
			$insertSQL .= "('18', '".$roomInformation['o_id']."', '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['o_bezeichnung'])."', '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['o_strasse'])."', '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['o_plz'])."', '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['o_ort'])."', '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['o_bemerkung'])."', '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['o_haustiere_erlaubt'])."', '";
			if(!empty($roomInformation['o_gps']['longitude']) && !empty($roomInformation['o_gps']['latitude'])) {
				$insertSQL .= $roomInformation['o_gps']['longitude']."', '";
				$insertSQL .= $roomInformation['o_gps']['latitude']."', ";
			}
			$insertSQL .= (time()-(48*60*60)) . ", ". (time()-(48*60*60)) .")";

			if(mysqli_connect_errno()) {
				die('connection failed');
			}
			//var_dump($insertSQL);
			//$err = $this->db_connect->query($insertSQL);
			$insertSQL .= ';';
			$this->db_connect->query("SET NAMES utf8");
			$success = $this->db_connect->query($insertSQL);
		}

		if(!$success) die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $insertSQL);
	}

	/**
	 * @param array $accommodations
	 */
	public function updateAccommodations($accommodations = Array()) {

		foreach ( $accommodations as $accommodationUid => $accommodation ) {
			$updateSQL = 'UPDATE ';
			$updateSQL .= $this->tableName;
			$updateSQL .= ' SET ';

			$updateSQL .= "title = '" . $this->db_connect->real_escape_string($accommodation['o_bezeichnung']) ."', ";
			$updateSQL .= "remark = '" . $this->db_connect->real_escape_string((is_string($accommodation['o_bemerkung']) ? $accommodation['o_bemerkung'] : "")) ."', ";
			$updateSQL .= "street = '" . $accommodation['o_strasse'] . "', ";
			$updateSQL .= "plz = '" . $accommodation['o_plz'] . "', ";
			$updateSQL .= "city = '" . $accommodation['o_ort'] . "', ";
			$updateSQL .= "pets = '" . $accommodation['o_haustiere_erlaubt'] . "', ";
			$updateSQL .= "tstamp = '" . time() ."', ";
			if(!empty($accommodation['o_gps']['longitude']) && !empty($accommodation['o_gps']['latitude'])) {
				if($accommodation['o_gps']['longitude'] > 0) {
					$updateSQL .= "longitude = '" . $accommodation['o_gps']['longitude'] ."', ";
				}
				else {
					$updateSQL .= "longitude = '" . ($accommodation['o_gps']['longitude'] * -1) ."', ";
				}
				$updateSQL .= "latitude = '" . $accommodation['o_gps']['latitude'] ."', ";
			}
			$updateSQL .= " WHERE uid = '". $accommodationUid . "'";

			if(mysqli_connect_errno()) {
				die('connection failed');
			}
			//$err = $this->db_connect->query($updateSQL);

			//die('stop');

			$updateSQL .= ";";

			//echo '<p>++++++++++++++++++++++++++++++++++++++++</p>';
			//var_dump($updateSQL);

			$success = $this->db_connect->query($updateSQL);

			if(!$success) die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $updateSQL);
		}

	}

	/**
	 * @param $o_id
	 *
	 * @return array
	 */
	public function getExistingAccommodation($o_id) {
		$sql = 'SELECT * FROM ' . $this->tableName;
		$sql .= ' WHERE object_id = "' . $o_id .'";';

		$result = $this->db_connect->query($sql);
		return (is_object($result) ? $result->fetch_assoc() : Array());
	}
}
