<?php

class WildeastConnector {

	/**
	 * @var int
	 */
	protected $zImWebInterfaceId = 14737;

	protected $interfaceUrl = 'https://zimmer.im-web.de/Schnittstellen/ZiW.php';

	/**
	 * constructor
	 */
	public function __construct() {
		$this->initialize();
	}

	/**
	 * init function
	 */
	public function initialize() {}

	/**
	 * basic function to establish connections
	 *
	 * @param $requestType
	 * @param $params
	 * @param bool $debugRequest
	 *
	 * @return mixed
	 */
	public function wildeastRequest( $requestType, $params, $debugRequest = false ) {
		$params['para']['interface_id'] = $this->zImWebInterfaceId;

		$url = $this->interfaceUrl;

		$url .= '?' . $requestType . '=1&';

		$url .= $this->formatParameters( $params );

		if ( $debugRequest ) {
			var_dump( $url );
		}

		$xmlDoc = file_get_contents( $url );

		$results = simplexml_load_string( $xmlDoc );

		return $results;
		/**/
	}

	/**
	 * get a session id from wildeast for following requests
	 * 
	 * @return mixed
	 */
	public function getWildeastSessionId() {
		$params['para'] = array(
			'support_email' => 'dev@telematika.de',
			'datestart' => date('Ymd', time()+(30*24*60*60)),
			'dateend' => date('Ymd', time()+(36*24*60*60)),
			'adults' => 1,
			'region' => array('we367bde633f616' => 1),
			'search_offset_nr' => 4
		);

		$result = $this->wildeastRequest('get[search]', $params);
		//var_dump($result);

		$result = json_decode(json_encode($result),TRUE);

		return $result['session_id'];
	}

	/**
	 * TODO: refactor to recursion
	 * formats the parameters for request
	 *
	 * @param array $params
	 *
	 * @return string formatted params
	 */
	protected function formatParameters( $params = array() ) {
		$result = '';
		foreach ( $params as $key => $value ) {
			if ( is_array( $value ) ) {
				foreach ( $value as $subkey => $subvalue ) {
					if ( is_array( $subvalue ) ) {
						foreach ( $subvalue as $subkey2 => $subvalue2 ) {
							if ( is_array( $subvalue2 ) ) {
								foreach ( $subvalue2 as $subkey3 => $subvalue3 ) {
									$result .= $key . '[' . $subkey . '][' . $subkey2 . '][' . $subkey3 . ']=' . urlencode( $subvalue3 ) . '&';
								}
							} else {
								if ( isset( $subvalue2 ) && ( strlen( $subvalue2 ) > 0 ) ) {
									$result .= $key . '[' . $subkey . '][' . $subkey2 . ']=' . urlencode( $subvalue2 ) . '&';
								}
							}
						}
					} else {
						if ( isset( $subvalue ) && ( strlen( $subvalue ) > 0 ) ) {
							$result .= $key . '[' . $subkey . ']=' . urlencode( $subvalue ) . '&';
						}
					}
				}
			} else if ( isset( $value ) && ( strlen( $value ) > 0 ) ) {
				$result .= $key . '=' . urlencode( $value ) . '&';
			}
		}

		return $result;
	}

	/**
	 * create log entries in a specific file
	 *
	 * @param null $fileName
	 * @param null $message
	 * @param string $level
	 */
	/*
	public function logInFile( $fileName = null, $message = null, $level = 'DEBUG' ) {
		$config = $this->configurationManager->getConfiguration( ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK );

// check config for log file
		if ( $fileName === null ) {
			$fileName = $config['settings']['default_log_file'];

			if ( empty( $fileName ) ) {
				return;
			}
		}

// check and load directory of log file
		$dir = $config['settings']['default_log_dir'];
		if ( empty( $dir ) ) {
			return;
		}

// check message
		if ( $message === null ) {
			return;
		}

// build path to log file
		$logFilePath = $dir . $fileName;

// write log entry
		if ( $fp = fopen( $logFilePath, 'a+' ) ) {
			fwrite( $fp, date( 'Y-m-d H:i:s' ) . ' | ' . strtoupper( $level ) . ' | ' . $message . "\n" );
			fclose( $fp );
		}

	}
	*/
}