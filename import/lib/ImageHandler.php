<?php


class ImageHandler {

	/**
	 * @var string
	 */
	protected $tableName = '';

	/**
	 * @var mysqli
	 */
	protected $db_connect = null;

	/**
	 * @var \AccommodationHandler
	 */
	protected $accommodation_handler = null;

	/**
	 * @param mysqli $db_connect
	 */
	public function __construct(mysqli $db_connect = null) {
		$this->db_connect = $db_connect;
		$this->tableName = 'tx_tmchillio_domain_model_image';
	}

	/**
	 * @TODO: optimize code and minimize database requests
	 * @param $roomInformation
	 */
	public function updateRoomImages($roomInformation) {
		$this->deleteImagesForRooms();
		$this->createImagesForRoom($roomInformation);
	}

	/**
	 * delete images for rooms
	 */
	public function deleteImagesForRooms() {
		// delete images and relations
		$sql = 'DELETE img, img_ro FROM tx_tmchillio_domain_model_image img
				JOIN tx_tmchillio_room_image_mm img_ro on img_ro.uid_foreign = img.uid
				JOIN tx_tmchillio_domain_model_room ro on img_ro.uid_local = ro.uid;';

		$this->db_connect->query($sql);
	}

	/**
	 * delete images for accommodations
	 *
	 * @param int $accommodationUid
	 */
	public function deleteImagesForAccommodation($accommodationUid = 0) {
		// delete images and relations
		$sql = 'DELETE img, img_acc FROM tx_tmchillio_domain_model_image img
				JOIN tx_tmchillio_accommodation_image_mm img_acc on img_acc.uid_foreign = img.uid
				JOIN tx_tmchillio_domain_model_accommodation acc on img_acc.uid_local = acc.uid
				WHERE acc.uid = ' .$accommodationUid. ';';

		$this->db_connect->query($sql);
	}

	/**
	 * @param $roomsInformation
	 */
	public function createImagesForRoom($roomsInformation) {
		//var_dump($roomsInformation);
		$roomsInformation = json_decode(json_encode($roomsInformation), 1);

		$this->db_connect->query("SET NAMES utf8");

		$accommodationImages = Array();

		foreach ( $roomsInformation['content']['room'] as $roomInformation ) {

			// get highest uid for building mm relation later
			$sqlLastUid = 'SELECT uid from tx_tmchillio_domain_model_image img order by img.uid desc limit 1';
			$lastUidBeforeInsert = $this->db_connect->query($sqlLastUid);
			$lastUidBeforeInsert = $lastUidBeforeInsert->fetch_object()->uid;

			// build insert for images
			$insertSQL = 'INSERT IGNORE INTO ';
			$insertSQL .= $this->tableName;
			$insertSQL .= " (pid, path, tstamp, crdate)";
			$insertSQL .= " VALUES";
			if(is_array($roomInformation['z_bild']['bild'])) {
				foreach($roomInformation['z_bild']['bild'] as $key => $z_bild) {
					$insertSQL .= ($key > 0 ? "," : "");
					$insertSQL .= "('19', '".$z_bild."', '" . time() . "', '" . time() . "')";
				}
			}
			else if(is_string($roomInformation['z_bild']['bild'])) {
				$insertSQL .= "('19', '".$roomInformation['z_bild']['bild']."', '" . time() . "', '" . time() . "')";
			}
			else continue;
			$insertSQL .= ";";

			// insert images
			$success = $this->db_connect->query($insertSQL);
			if(!$success) {
				var_dump($roomInformation['z_bild']['bild']);
				die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $insertSQL);
			}

			// get highest uid for building mm relation
			$lastUidAfterInsert = $this->db_connect->query($sqlLastUid);
			$lastUidAfterInsert = $lastUidAfterInsert->fetch_object()->uid;

			// get roomUid and accommodationUid by WE room id
			$roomDBInfo = $this->db_connect->query(
				"SELECT ro.uid as roomUid, acc.uid as accommodationUid
				FROM tx_tmchillio_domain_model_room ro
				JOIN tx_tmchillio_domain_model_accommodation acc on ro.accommodation = acc.uid
				WHERE ro.room_id = '" .$roomInformation['@attributes']['id']. "'
				GROUP BY ro.uid;"
			)->fetch_object();

			// collect images for accommodations
			$accommodationImages[$roomDBInfo->accommodationUid] = $roomInformation['o_bild'];

			// build relations for rooms
			$insertSQL = "INSERT IGNORE INTO ";
			$insertSQL .= "tx_tmchillio_room_image_mm";
			$insertSQL .= " (uid_local, uid_foreign)";
			$insertSQL .= " VALUES";

			for($i = $lastUidBeforeInsert; $i <= $lastUidAfterInsert; $i++) {
				$insertSQL .= ($i > $lastUidBeforeInsert ? "," : "");
				$insertSQL .= "(" .$roomDBInfo->roomUid. "," .$i. ")";
				//echo $i . '<br>';
			}
			$insertSQL .= ";";

			// insert relations
			$success = $this->db_connect->query($insertSQL);
			if(!$success) die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $insertSQL);
		}
		//echo '------------------------<br>';

		// create images for collected accommodations
		$this->createImagesForAccommodation($accommodationImages);
	}

	/**
	 * @param array $accommodationImages
	 */
	public function createImagesForAccommodation($accommodationImages = Array()) {

		$this->db_connect->query("SET NAMES utf8");

		foreach ( $accommodationImages as $accommodationUid => $accommodationImage ) {
			// delete images for accommodation
			$this->deleteImagesForAccommodation($accommodationUid);

			// get highest uid for build mm relation later
			$sqlLastUid = 'SELECT uid from tx_tmchillio_domain_model_image img order by img.uid desc limit 1';
			$lastUidBeforeInsert = $this->db_connect->query($sqlLastUid);
			$lastUidBeforeInsert = $lastUidBeforeInsert->fetch_object()->uid;

			// build insert for images
			$insertSQL = 'INSERT IGNORE INTO ';
			$insertSQL .= $this->tableName;
			$insertSQL .= " (pid, path, tstamp, crdate)";
			$insertSQL .= " VALUES";
			if(is_array($accommodationImage['bild'])) {
				foreach($accommodationImage['bild'] as $key => $o_bild) {
					$insertSQL .= ($key > 0 ? "," : "");
					$insertSQL .= "('19', '".$o_bild."', '" . time() . "', '" . time() . "')";
				}
			}
			else if(is_string($accommodationImage['bild'])) {
				$insertSQL .= "('19', '".$accommodationImage['bild']."', '" . time() . "', '" . time() . "')";
			}
			else continue;
			$insertSQL .= ";";

			// insert images
			$success = $this->db_connect->query($insertSQL);
			if(!$success) {
				var_dump($accommodationImage['bild']);
				die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $insertSQL);
			}

			// get highest uid for build mm relation
			$lastUidAfterInsert = $this->db_connect->query($sqlLastUid);
			$lastUidAfterInsert = $lastUidAfterInsert->fetch_object()->uid;

			// build relations
			$insertSQL = "INSERT IGNORE INTO ";
			$insertSQL .= "tx_tmchillio_accommodation_image_mm";
			$insertSQL .= " (uid_local, uid_foreign)";
			$insertSQL .= " VALUES";

			// insert relations
			for($i = $lastUidBeforeInsert; $i <= $lastUidAfterInsert; $i++) {
				$insertSQL .= ($i > $lastUidBeforeInsert ? "," : "");
				$insertSQL .= "(" .$accommodationUid. "," .$i. ")";
				//echo $i . '<br>';
			}
			$insertSQL .= ";";

			$success = $this->db_connect->query($insertSQL);
			if(!$success) die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $insertSQL);
		}
		//echo '------------------------<br>';
	}
}