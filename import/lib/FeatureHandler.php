<?php

use TYPO3\CMS\Core\Tests\Exception;

class FeatureHandler {

	/**
	 * @var string
	 */
	protected $tableName = '';

	/**
	 * @var mysqli
	 */
	protected $db_connect = null;

	protected $uid = 0;

	/**
	 * @param mysqli $db_connect
	 */
	public function __construct(mysqli $db_connect = null) {
		$this->db_connect = $db_connect;
		$this->tableName = 'tx_tmchillio_domain_model_feature';
	}

	/**
	 * @param $features
	 */
	public function saveFeatures($features) {

		foreach ( $features->children() as $parent => $child ) {
			$this->uid = 1;
			$insertSQL = 'INSERT INTO ';
			$insertSQL .= $this->tableName;

			$insertSQL .= ' (pid, name, id, cluster, tstamp, crdate, type)';
			$insertSQL .= " VALUES ('19', '";
			$insertSQL .= $child->attributes()->name[0]."', ";
			$insertSQL .= $child->attributes()->position[0] . ", '". $child->attributes()->cluster[0] . "', 0, 0, '" . $child->attributes()->system[0] . "');";

			if(mysqli_connect_errno()) {
				die('connection failed');
			}
			//var_dump($insertSQL);
			//$err = $this->db_connect->query($insertSQL);

			$this->db_connect->query("SET NAMES utf8");
			$this->db_connect->query($insertSQL);
		}
	}
}
