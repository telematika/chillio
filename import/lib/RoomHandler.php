<?php


class RoomHandler {

	/**
	 * @var string
	 */
	protected $tableName = '';

	/**
	 * @var mysqli
	 */
	protected $db_connect = null;

	/**
	 * @var \AccommodationHandler
	 */
	protected $accommodation_handler = null;

	/**
	 * @param mysqli $db_connect
	 */
	public function __construct(mysqli $db_connect = null) {
		$this->db_connect = $db_connect;
		$this->tableName = 'tx_tmchillio_domain_model_room';
		$this->accommodation_handler = new AccommodationHandler($this->db_connect);
	}

	/**
	 * @param $rooms
	 */
	public function saveRooms($rooms) {
		$this->accommodation_handler = new AccommodationHandler($this->db_connect);

		$roomsInformation = json_decode(json_encode($rooms), 1);
		//var_dump($roomsInformation);

		$insertSQL = 'INSERT IGNORE INTO ';
		$insertSQL .= $this->tableName;
		$insertSQL .= ' (pid, accommodation, title, remark, room_id, min_person, max_person, bedrooms, min_price, tstamp, crdate)';
		$insertSQL .= " VALUES";

		foreach ( $roomsInformation['content']['room'] as $key => $roomInformation ) {

			$accommodation = $this->accommodation_handler->getExistingAccommodation($roomInformation['o_id']);

			$insertSQL .= ($key > 0 ? "," : "");
			$insertSQL .= "('19', ".$accommodation['uid'].", '";
			$insertSQL .= $this->db_connect->real_escape_string($roomInformation['z_bezeichnung'])."', '";
			$insertSQL .= $this->db_connect->real_escape_string((is_string($roomInformation['z_bemerkung']) ? $roomInformation['z_bemerkung'] : ''))."', '";
			$insertSQL .= $roomInformation['@attributes']['id']."', '";
			$insertSQL .= $roomInformation['k_min_personen']."', '";
			$insertSQL .= $roomInformation['k_max_personen']."', '";
			$insertSQL .= $roomInformation['k_bedrooms']."', '";
			$insertSQL .= $roomInformation['ab_preis']."', ";
			$insertSQL .= (time()-(48*60*60)) . ", ". (time()-(48*60*60)) .")";

			if(mysqli_connect_errno()) {
				die('connection failed');
			}
			//$err = $this->db_connect->query($insertSQL);

			//die('stop');
		}

		$insertSQL .= ";";

		//echo '<p>++++++++++++++++++++++++++++++++++++++++</p>';
		//var_dump($insertSQL);

		$this->db_connect->query("SET NAMES utf8");
		$success = $this->db_connect->query($insertSQL);

		if(!$success) die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $insertSQL);
	}

	/**
	 * @param $rooms
	 */
	public function updateRooms($rooms) {
		$roomsInformation = json_decode(json_encode($rooms), 1);
		//var_dump($roomsInformation);

		$accommodationInformation = Array();

		foreach ( $roomsInformation['content']['room'] as $key => $roomInformation ) {
			$updateSQL = 'UPDATE ';
			$updateSQL .= $this->tableName;
			$updateSQL .= ' SET ';

			$updateSQL .= "title = '" . $this->db_connect->real_escape_string($roomInformation['z_bezeichnung']) ."', ";
			$updateSQL .= "remark = '" . $this->db_connect->real_escape_string((is_string($roomInformation['z_bemerkung']) ? $roomInformation['z_bemerkung'] : '')) ."', ";
			$updateSQL .= "min_person = '" . $roomInformation['k_min_personen'] ."', ";
			$updateSQL .= "max_person = '" . $roomInformation['k_max_personen'] ."', ";
			$updateSQL .= "bedrooms = '" . $roomInformation['k_bedrooms'] ."', ";
			$updateSQL .= "size = '" . $roomInformation['k_groesse'] ."', ";
			$updateSQL .= "tstamp = '" . time() ."', ";
			$updateSQL .= "min_price = '" . $roomInformation['ab_preis'] . "'";
			$updateSQL .= " WHERE room_id = '". $roomInformation['@attributes']['id'] . "'";

			if(mysqli_connect_errno()) {
				die('connection failed');
			}
			//$err = $this->db_connect->query($updateSQL);

			//die('stop');

			$updateSQL .= ";";

			//echo '<p>++++++++++++++++++++++++++++++++++++++++</p>';
			//var_dump($updateSQL);

			$success = $this->db_connect->query($updateSQL);

			if(!$success) die("Error: %s\n" . $this->db_connect->error . ', Query: ' . $updateSQL);

			$accommodation = $this->accommodation_handler->getExistingAccommodation($roomInformation['o_id']);
			if(!empty($accommodation['uid'])) {
				$accommodationInformation[$accommodation['uid']]['o_bezeichnung'] = $roomInformation['o_bezeichnung'];
				$accommodationInformation[$accommodation['uid']]['o_strasse'] = $roomInformation['o_strasse'];
				$accommodationInformation[$accommodation['uid']]['o_plz'] = $roomInformation['o_plz'];
				$accommodationInformation[$accommodation['uid']]['o_ort'] = $roomInformation['o_ort'];
				$accommodationInformation[$accommodation['uid']]['o_bemerkung'] = $roomInformation['o_bemerkung'];
				$accommodationInformation[$accommodation['uid']]['o_gps'] = $roomInformation['o_gps'];
				$accommodationInformation[$accommodation['uid']]['o_haustiere_erlaubt'] = $roomInformation['o_haustiere_erlaubt'];
				$accommodationInformation[$accommodation['uid']]['o_ausstattung'] = $roomInformation['o_ausstattung'];
				$accommodationInformation[$accommodation['uid']]['o_category'] = $roomInformation['o_category'];
			}
		}

		$this->accommodation_handler->updateAccommodations();
	}
}
