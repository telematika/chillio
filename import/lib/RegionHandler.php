<?php

class RegionHandler {

	/**
	 * @var string
	 */
	protected $tableName = '';

	/**
	 * @var mysqli
	 */
	protected $db_connect = null;

	protected $uid = 0;

	/**
	 * @param mysqli $db_connect
	 */
	public function __construct(mysqli $db_connect = null) {
		$this->db_connect = $db_connect;
		$this->tableName = 'tx_tmchillio_domain_model_region';
	}

	/**
	 * @param $regions
	 * @param int $parentRegion
	 */
	public function saveRegions($regions, $parentRegion = 0) {

		foreach ( $regions->children() as $parent => $child ) {
			$insertSQL = 'INSERT INTO ';
			$insertSQL .= $this->tableName;

			$insertSQL .= ' (pid, name, region, tstamp, crdate, regid, longtitude, latitude, zipcode)';
			$insertSQL .= " VALUES ('0', '";
			$insertSQL .= $child->attributes()->name[0]."', ";
			$insertSQL .= $parentRegion . ", 0, 0, '";
			$insertSQL .= $child->attributes()->regid[0]."', ";
			$insertSQL .= $child->attributes()->longitude[0].", ";
			$insertSQL .= $child->attributes()->latitude[0].", ";
			$insertSQL .= ((!empty($child->attributes()->zipcode[0])) ? $child->attributes()->zipcode[0] : 0).");";

			if(mysqli_connect_errno()) {
				die('connection failed');
			}
			//var_dump($insertSQL);
			//$err = $this->db_connect->query($insertSQL);

			$this->db_connect->query("SET NAMES utf8");
			$this->db_connect->query($insertSQL);

			if ( ! empty( $parentRegion ) ) {
				//$newRegion->setRegion( $parentRegion );
			}
			$this->uid++;
			if ( sizeof( $child->children() ) > 0 ) {
				$this->saveRegions( $child, $this->uid);
			}
		}
	}
}
