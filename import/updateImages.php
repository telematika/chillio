<?php

use TYPO3\CMS\Core\Tests\Exception;

require_once('lib/RoomHandler.php');
require_once('lib/ImageHandler.php');
require_once('lib/WildeastConnector.php');

$host = 'db1193.mydbserver.com';
$username = 'p270890d1';
$password = 'uteqaPes.466';
$database = 'usr_p270890_1';
//$db_connect = new mysqli($host, $username, $password, $database);

$host = 'localhost';
$username = 'root';
$password = 'keins';
$database = 'chillio';
$db_connect = new mysqli($host, $username, $password, $database);

if(mysqli_connect_errno()) {
	die('connection failed');
}

$time_start = microtime_float();

// init connector
$wildeastConnector = new WildeastConnector();
// create a wildeast session id
$wildeastSessionId = $wildeastConnector->getWildeastSessionId($wildeastConnector);

$imageHandler = new ImageHandler($db_connect);

// prepare params, which have to get saved
$params['para'] = Array(
	'session_id' => $wildeastSessionId,
	'item' => Array(
		'o_id' => 1,
		'z_bild' => 1,
		'o_bild' => 1
	)
);

// get all saved room ids
$result = $db_connect->query(
	'SELECT ro.uid as roomUid,room_id as roomId, acc.uid as accommodationUid
	FROM tx_tmchillio_domain_model_room ro
	JOIN tx_tmchillio_domain_model_accommodation acc on ro.accommodation = acc.uid
	GROUP BY ro.uid'
);
$roomIdsForRequest = Array();
$i = 1;

// loop through all and combine 100 room ids to one request
while($obj = $result->fetch_object()) {
	$roomIdsForRequest[$obj->roomId] = 1;
	if($i%100 == 0) {
		$params['para']['room'] = $roomIdsForRequest;

		$roomInformation = $wildeastConnector->wildeastRequest('get[rooms]', $params);

		$imageHandler->updateRoomImages($roomInformation);

		$roomIdsForRequest = Array();
	}
	$i++;
	//if($i > 200) throw new Exception('stop');
}

$time_end = microtime_float();
$time = $time_end - $time_start;
echo '<p>Dauer: ' . $time . '</p>';


/**
 * get microtime as float
 * @return float
 */
function microtime_float() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}