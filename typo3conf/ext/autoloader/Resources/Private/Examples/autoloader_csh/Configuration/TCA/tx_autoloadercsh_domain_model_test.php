<?php

/**
 * Base TCA generation for the model HDNET\\AutoloaderCsh\\Domain\\Model\\Test
 */

$base = \HDNET\Autoloader\Utility\ModelUtility::getTcaInformation('HDNET\\AutoloaderCsh\\Domain\\Model\\Test');

$custom = array();

return \HDNET\Autoloader\Utility\ArrayUtility::mergeRecursiveDistinct($base, $custom);