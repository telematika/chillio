<?php

/**
 * Base TCA generation for the model HDNET\\AutoloaderSmartobject\\Domain\\Model\\TestList
 */

$base = \HDNET\Autoloader\Utility\ModelUtility::getTcaInformation('HDNET\\AutoloaderSmartobject\\Domain\\Model\\TestList');

$custom = array();

return \HDNET\Autoloader\Utility\ArrayUtility::mergeRecursiveDistinct($base, $custom);