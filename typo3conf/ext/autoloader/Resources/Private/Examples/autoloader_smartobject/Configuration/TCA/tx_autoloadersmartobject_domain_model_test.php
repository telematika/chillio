<?php

/**
 * Base TCA generation for the model HDNET\\AutoloaderSmartobject\\Domain\\Model\\Test
 */

$base = \HDNET\Autoloader\Utility\ModelUtility::getTcaInformation('HDNET\\AutoloaderSmartobject\\Domain\\Model\\Test');

$custom = array();

return \HDNET\Autoloader\Utility\ArrayUtility::mergeRecursiveDistinct($base, $custom);