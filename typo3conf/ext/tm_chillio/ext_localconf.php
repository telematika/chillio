<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Telematika.' . $_EXTKEY,
	'SearchForm',
	array(
		'Accommodation' => 'searchForm, search, detail, roomDetail, bookingFirstStep, bookingSecondStep, bookingThirdStep, bookingFinalStep, bookingCompleteStep, wishlist, getRoomAvailabilities, getPricingForRoom',
		'Region' => 'get2ndLevelRegions, get3rdLevelRegions, get4thLevelRegions, get5thLevelRegions',
	),
	// non-cacheable actions
	array(
		'Accommodation' => 'searchForm, search, detail, roomDetail, bookingFirstStep, bookingSecondStep, bookingThirdStep, bookingFinalStep, bookingCompleteStep, wishlist, getRoomAvailabilities, getPricingForRoom',
		'Region' => 'get2ndLevelRegions, get3rdLevelRegions, get4thLevelRegions, get5thLevelRegions',
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Telematika.' . $_EXTKEY,
	'StartpageForm',
	array(
		'Accommodation' => 'showStartpageForm',
		'Region' => 'searchForRegions',
	),
	// non-cacheable actions
	array(
		'Accommodation' => 'showStartpageForm',
		'Region' => 'searchForRegions',
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Telematika.' . $_EXTKEY,
	'LandingPage',
	array(
		'Region' => 'showLandingText',
	),
	// non-cacheable actions
	array(
		'Region' => 'showLandingText',
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Telematika.' . $_EXTKEY,
	'Wishlist',
	array(
		'Accommodation' => 'wishlist'
	),
	// non-cacheable actions
	array(
		'Accommodation' => 'wishlist'
	)
);
