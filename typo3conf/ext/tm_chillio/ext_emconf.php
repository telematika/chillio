<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "tm_chillio"
 *
 * Auto generated by Extension Builder 2015-02-04
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Chillio Extension',
	'description' => 'Manages accommodations via Wildeast Interface. ',
	'category' => 'plugin',
	'author' => 'Thomas Reichwein',
	'author_email' => 'reichwein@telematika.de',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);