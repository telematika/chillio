$(document).ready(function() {
    initLeaflet();
});

window.map = null;

function initLeaflet() {
    var initMapOptions = {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        key: 'thomaslav.l28m5f39',
        accessToken: 'pk.eyJ1IjoidGhvbWFzbGF2IiwiYSI6ImNraTJRWTAifQ.FnfqV-2BDM-KwsppZq105w#11',
        center: [52.5075419, 13.4251364],
        zoom: 13
    };

    if(jQuery('#map')) {
        window.map = L.map('map').setView([52.5075419, 13.4251364], 13);
        L.tileLayer(
            'http://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token={accessToken}',
            initMapOptions
        ).addTo(window.map);
    }

}


function setMapPosition() {

}

