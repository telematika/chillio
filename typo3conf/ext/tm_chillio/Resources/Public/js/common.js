L_DISABLE_3D = true;
L_PREFER_CANVAS = true;

(function ($, window, undefined) {
    function ChillioApplication() {
        var ref = this;
        window.chillio = this;

        this.list_button = null;
        this.map_button = null;
        this.map_area = null;
        this.list_area = null;

        this.search_region = null;
        this.search_city = null;
        this.search_form = null;
        this.start_form = null;

        this.search_childs = null;

        this.search_sorter = null;

        this.textfield_from = null;
        this.textfield_to = null;

        this.ajaxDelay = null;
        this.ajaxTimeout = null;

        this.marker = null;

        this.init = function () {
            this.list_button = jQuery('#switch_bar_listbutton');
            this.map_button = jQuery('#switch_bar_mapbutton');
            this.map_area = jQuery('#map');
            this.list_area = jQuery('#result_list');
            this.search_region = jQuery('#search_region');
            this.search_city = jQuery('#search_city');
            this.search_childs = jQuery('#search_childs');
            this.search_form = jQuery('#search').find('> form');
            this.start_form = jQuery('#searchbar').find('form');

            this.search_sorter = jQuery('#sort_results');
            this.load_layer = jQuery('#result_list_loading');
            this.textfield_from = jQuery('#textfield-from');
            this.textfield_to = jQuery('#textfield-to');
            this.ajaxDelay = 2000;
            this.ajaxLoadingImage = '<img src="typo3conf/ext/tm_chillio/Resources/Public/img/ajax-loader.gif" alt="wird geladen" id="ajax_loading_image" />';

            if(ref.map_area.length > 0) this.initLeaflet();
            this.initStartForm();
            this.initDatepicker();
            this.initTextStyle();
            this.initHoverTopNavi();
            this.initSelectMenus();
            this.initFormLoad();
            this.init1stLevelRegionFilter();
            this.init2ndLevelRegionFilter();
            this.init3rdLevelRegionFilter();
            this.init4thLevelRegionFilter();
            this.init5thLevelRegionFilter();
            this.initAdultNumberFilter();
            this.initChildNumberFilter();
            this.initCloseButton();
            this.initSwitchViewButtons();
            this.initResultListOrdering();
            this.initSearchFilter();
            this.initAjaxDetailLinks();
            this.initReadMore();
            this.initScrollWindow();
            this.moveBackground();

            this.search_region.parent('fieldset').remove();
            this.search_city.parent('fieldset').remove();
        };

        this.initStartForm = function() {
            this.start_form.find('.where').autocomplete({
                source: function( request, response ) {
                    var url = ref.start_form.find('#searchregionurl').attr('value');
                    $.ajax({
                        url: url,
                        data: {
                            search : ref.start_form.find('.where').val()
                        },
                        success: function( data ) {
                            response(data);
                        },
                        error: function() {
                            console.log('errorUrl: ' + url + '&search=' + ref.start_form.find('.where').val());
                        }
                    });
                },
                minLength: 3,
                select: function( event, ui ) {
                    ref.start_form.find('.where').val(ui.item.label);
                },
                open: function() {
                    $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                },
                close: function() {
                    $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                }
            });
        };

        this.initDatepicker = function () {
            $('.datepicker').datepicker({
                dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                changeMonth: true,
                changeYear: true,
                dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                minDate: new Date()
            });
            $('#belegungskalender').datepicker();
            $('input.from').datepicker({
                dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                changeMonth: false,
                changeYear: false,
                minDate: '+1',
                onClose: function (selectedDate) {
                    $("input.to").datepicker("option", "minDate", selectedDate);
                }
            });

            $('input.to').datepicker({
                dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                changeMonth: false,
                changeYear: false,
                minDate: '+2'
            });
            var toDay = new Date();
            toDay = toDay.toLocaleDateString();
            var toDayArr = toDay.split('.');
            toDayArr[0] = parseInt(toDayArr[0]) + parseInt(1);
            toDay = toDayArr[0] + '.' + (toDayArr[1]) + '.' + toDayArr[2];
            $('#search_when_arrival .from').attr('placeholder', toDay);
        };

        this.initTextStyle = function() {
            $('.region_text a.blauerPfeil').append('<i class="fa fa-arrow-circle-right"></i>');
        };

        this.initHoverTopNavi = function () {
            $('#top_bar').find('#navi > ul > li').hoverIntent(
                function () {
                    $(this).find('ul').slideToggle();
                },
                function () {
                    $(this).find('ul').slideToggle();
                }
            )
        };

        this.initSelectMenus = function () {
            $('.selectMenu').selectmenu();
        };

        this.initCloseButton = function () {
            $('#close_search_button').click(function () {
                var close_search_button = $(this);
                var search = $('#search');

                search.find('div').each(function () {
                    if (close_search_button != $(this)) {
                        $(this).hide();
                    }
                });

                search.animate(
                    {
                        width: "20px"
                    },
                    1000
                );
                $('#map').animate(
                    {
                        width: "880px"
                    },
                    1000,
                    function () {
                        window.map.updateSize();
                    }
                );
            });
        };

        this.initSwitchViewButtons = function () {
            ref.list_button.bind('click', function () {
                ref.list_button.addClass('active');
                ref.list_area.show();
                ref.map_button.removeClass('active');
                ref.map_area.hide();
                window.map.remove();
                // trigger resize because of wrong scaling images by slick
                $(window).trigger('resize');
            });

            ref.map_button.bind('click', function () {
                ref.list_button.removeClass('active');
                ref.list_area.hide();
                ref.map_button.addClass('active');
                ref.map_area.show();
                ref.initLeaflet();
                ref.getGpsForAllObjects();
            });

            //ref.list_button.trigger('click');
        };

        this.addElementViaAjax = function (url, element, emptyElement, onComplete) {
            jQuery.ajax({
                type: "POST",
                url: url,
                success: function (result) {
                    if (emptyElement) {
                        element.html('');
                    }
                    element.append(result);
                    if (onComplete) {
                        onComplete();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        };

        this.initFormLoad = function () {
            ref.search_form.find('.search_update').click(function() {
                ref.doAjaxForm(ref.search_form.attr('action'), ref.search_form, ref.list_area, ref.initResultList, null, true);
                ref.load_layer.show();
                return false;
            });

            if($('.block_search').html() != 1) {
                ref.search_form.find('.search_update').trigger('click');
            }
        };

        this.init1stLevelRegionFilter = function () {
            jQuery('#search_country').selectmenu({
                change: function (event, data) {
                    jQuery('#search_region').parent('div.fieldset').remove();
                    jQuery('#search_city').parent('div.fieldset').remove();
                    jQuery('#search_town').parent('div.fieldset').remove();
                    jQuery('#search_town_region').parent('div.fieldset').remove();
                    var ajaxUrlCountry = jQuery(this).find('option[value="' + data.item.value + '"]').attr('data-subregionurl');

                    ref.addElementViaAjax(ajaxUrlCountry, jQuery('#search_where'), false, ref.init2ndLevelRegionFilter);
                }
            });
        };

        this.init2ndLevelRegionFilter = function () {
            jQuery('#search_region').selectmenu({
                change: function (event, data) {
                    jQuery('#search_city').parent('div.fieldset').remove();
                    jQuery('#search_town').parent('div.fieldset').remove();
                    jQuery('#search_town_region').parent('div.fieldset').remove();
                    var ajaxUrlRegion = jQuery(this).find('option[value="' + data.item.value + '"]').attr('data-subregionurl');
                    ref.addElementViaAjax(ajaxUrlRegion, jQuery('#search_where'), false, ref.init3rdLevelRegionFilter);
                }
            });
        };

        this.init3rdLevelRegionFilter = function () {
            jQuery('#search_city').selectmenu({
                change: function (event, data) {
                    jQuery('#search_town').parent('div.fieldset').remove();
                    jQuery('#search_town_region').parent('div.fieldset').remove();
                    var ajaxUrlRegion = jQuery(this).find('option[value="' + data.item.value + '"]').attr('data-subregionurl');
                    ref.addElementViaAjax(ajaxUrlRegion, jQuery('#search_where'), false, ref.init4thLevelRegionFilter);
                }
            });
        };

        this.init4thLevelRegionFilter = function () {
            jQuery('#search_town').selectmenu({
                change: function (event, data) {
                    jQuery('#search_town_region').parent('div.fieldset').remove();
                    var ajaxUrlRegion = jQuery(this).find('option[value="' + data.item.value + '"]').attr('data-subregionurl');
                    ref.addElementViaAjax(ajaxUrlRegion, jQuery('#search_where'), false, ref.init5thLevelRegionFilter);
                }
            });
        };

        this.init5thLevelRegionFilter = function () {
            jQuery('#search_town_region').selectmenu();
        };

        this.initAdultNumberFilter = function () {
            jQuery('#search_adults').selectmenu();
        };

        this.initChildNumberFilter = function () {
            jQuery('#search_childs').selectmenu({
                change: function (event, data) {
                    if(ref.search_childs.val() > 0) {
                        ref.initModalChildAges(ref.search_childs.val(), $('#search_who_child_modal'));
                    }
                }
            });

            $('#search_who_child_link').click(function() {
                ref.initModalChildAges();
                return false;
            });
        };

        this.initModalChildAges = function(numberOfChilds, modalNode) {
            var selectedNumberOfChilds = numberOfChilds;
            var search_who_child_modal = modalNode;

            // reset form visibility
            search_who_child_modal.find('.age-child-item').hide(); // hide all fields
            search_who_child_modal.find('.age-child-item select').attr('disabled', 'disabled'); // disable all fields
            search_who_child_modal.find('.modal-body div > p').hide(); // hide text, if no childs are selected
            if(selectedNumberOfChilds == 0) {   // show text, if no childs are selected
                search_who_child_modal.find('.modal-body div > p').show();
            }
            else {                              // show select fields according to the number of childs
                search_who_child_modal.find('.age-child-item').each(function(index, element) {
                    if(index < selectedNumberOfChilds) {
                        $(element).show();
                        $(element).find('select').removeAttr('disabled');
                    }
                });
            }

            search_who_child_modal.modal();
        };

        this.doAjaxForm = function (url, form, resultbox, onComplete, loader, withoutDelay) {
            if (ref.ajaxTimeout) {
                clearTimeout(ref.ajaxTimeout);
            }
            if(loader) {
                loader.show();
            }
            var data = ref.getFormInputs(form);

            var delay;
            if(withoutDelay) {
                delay = 0;
            }
            else {
                delay = ref.ajaxDelay;
            }

            ref.ajaxTimeout = setTimeout(function () {
                var htmlResult = null;

                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (result) {
                        htmlResult = result;
                        if (resultbox) {
                            resultbox.html(result);
                        }
                        if (onComplete) {
                            onComplete();
                            ref.load_layer.hide();11
                        }
                    },
                    beforeSend: function () {
                        if (resultbox) {
                            resultbox.html('');
                        }
                        //console.log(url + data);
                    },
                    error: function () {
                        //alert(url + data);
                        //jQuery('body').prepend(url + data);
                    }
                });
                //return htmlResult;
            }, delay);
        };

        this.doAjax = function (url, resultbox, onComplete) {
            var htmlResult = null;

            jQuery.ajax({
                type: "POST",
                url: url,
                success: function (result) {
                    htmlResult = result;
                    if (resultbox) {
                        resultbox.html(result);
                    }
                    if (onComplete) {
                        onComplete();
                    }
                },
                beforeSend: function () {
                    if (resultbox) {
                        resultbox.html('');
                    }
                },
                error: function () {
                    //alert(url + data);
                    jQuery('body').prepend(url);
                }
            });
            return htmlResult;
        };

        // init all links on the page, which have a class doAjax
        this.initAjaxDetailLinks = function (onComplete) {
            jQuery('a.doAjax').each(function () {
                var currentElement = jQuery(this);

                currentElement.bind('click', function () {
                    ref.doAjax(currentElement.attr('href'), ref.list_area, onComplete);

                    return false;
                });
            });
        };

        this.initWishListLinks = function () {
            jQuery('#result_list').find('.merkliste_Link i.fa').each(function(index, element) {
                element.bind('click', function() {

                });
            });
        };

        this.getFormInputs = function (form) {
            var inputString = '&';

            jQuery(form).find('input[type="hidden"]').each(function (index, el) {
                if (el.value.length > 0) {
                    inputString += this.name + '=' + this.value + '&';
                }
            });
            jQuery(form).find('input[type="text"]').each(function (index, el) {
                if (el.value.length > 0) {
                    inputString += this.name + '=' + this.value + '&';
                }
            });
            jQuery(form).find('input[type="checkbox"]:checked').each(function (index, el) {
                if (el.value.length > 0) {
                    inputString += this.name + '=' + this.value + '&';
                }
            });
            jQuery(form).find('select').each(function () {
                inputString += this.name + '=' + jQuery(this).find(':selected').attr('value') + '&';
            });
            //console.log(inputString);
            return inputString;
        };

        // init gallery for result list and room details
        this.initGallery = function () {
            var timeout = window.setTimeout(function() {
                jQuery('.accommodation_item_previewImage').each(function () {
                    var index = $('.accommodation_item_previewImage').index(this);
                    index = index.toString();

                    var Slider = 'Slider_' + index;
                    var SliderBot = 'SliderBot_' + index;
                    var Slidercl = '.' + Slider;
                    var SliderBotcl = '.' + SliderBot;
                    jQuery(this).addClass(Slider);
                    jQuery(this).next().addClass(SliderBot);
                    jQuery(this).slick({
                        slide: 'img',
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: false,
                        autoplay: false,
                        fade: true,
                        draggable: false,
                        asNavFor: SliderBotcl
                    });
                    jQuery(this).next().slick({
                        slide: 'img',
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        dots: false,
                        autoplay: false,
                        arrows: true,
                        focusOnSelect: true,
                        vertical: false,
                        draggable: true,
                        centerMode: true,
                        /* appendArrows: SliderBotcl,*/
                        nextArrow: '<button type="button" class="slick-next"><i class="fa  fa-angle-left fa-2x"></i></button>',
                        prevArrow: '<button type="button" class="slick-prev"><i class="fa  fa-angle-right fa-2x"></i></button>',
                        asNavFor: Slidercl
                    });


                });
                jQuery('.accommodation_item_previewImage_detail').each(function () {
                    var index = $('.accommodation_item_previewImage_detail').index(this);
                    index = index.toString();

                    var Slider = 'SliderDetail_' + index;
                    var SliderBot = 'SliderDetailBot_' + index;
                    var Slidercl = '.' + Slider;
                    var SliderBotcl = '.' + SliderBot;
                    jQuery(this).addClass(Slider);
                    jQuery(this).next().addClass(SliderBot);
                    jQuery(this).slick({
                        slide: 'img',
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                        appendArrows: Slidercl,
                        nextArrow: '<button type="button" class="slick-next"><i class="fa  fa-angle-left fa-3x"></i></button>',
                        prevArrow: '<button type="button" class="slick-prev"><i class="fa  fa-angle-right fa-3x"></i></button>',
                        dots: false,
                        autoplay: false,
                        fade: true,
                        draggable: false,
                        asNavFor: SliderBotcl
                    });
                    jQuery(this).next().slick({
                        slide: 'img',
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        dots: false,
                        autoplay: false,
                        arrows: false,
                        focusOnSelect: true,
                        vertical: false,
                        draggable: true,
                        centerMode: true,
                        /* appendArrows: SliderBotcl,*/
                        asNavFor: Slidercl
                    });
                });
                /*this.initDetailSlider();*/
                window.clearTimeout(timeout);
            }, 300);


        };

        // init all js, after result list is loaded
        this.initResultList = function () {
            ref.initGallery();
            ref.getGpsForAllObjects();
            ref.initAjaxDetailLinks(ref.initDetails);
        };

        // init select field on the top, which influences ordering of results
        this.initResultListOrdering = function () {
            ref.search_sorter.selectmenu({
                change: function (event, data) {
                    ref.search_form.find('#search_sortby').attr('value', ref.search_sorter.val());
                    ref.doAjaxForm(ref.search_form.attr('action'), ref.search_form, ref.list_area, ref.initResultList, null, true);
                }
            });
        };

        // init all elements on the detail page for a room
        this.initDetails = function () {
            var timeout = window.setTimeout(function() {
                ref.initGallery();
                ref.initReadMore();
                ref.initScrollWindow();
                ref.featureSort();
                ref.modalShowBookingProcess();
                ref.loadCalendar();
                ref.initAjaxDetailLinks(ref.initResultList);
                ref.goToCalendar();
                ref.goToTop();

                var gpsLati = parseFloat($('#gps-lati').val());
                var gpsLong = parseFloat($('#gps-long').val().substr(1, 12));
                //console.log(gpsLati + ' ,' + gpsLong);
                var initMapOptions = {
                    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                    maxZoom: 18,
                    key: 'thomaslav.l28m5f39',
                    accessToken: 'pk.eyJ1IjoidGhvbWFzbGF2IiwiYSI6ImNraTJRWTAifQ.FnfqV-2BDM-KwsppZq105w#11',
                    center: [gpsLati, gpsLong],
                    zoom: 10
                };

                if (jQuery('#karte')) {
                    //console.log(gpsLati + ' ,' + gpsLong);
                    window.karte = L.map('karte').setView([gpsLati, gpsLong], 6);
                    L.tileLayer(
                        'http://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token={accessToken}',
                        initMapOptions,
                        marker
                    ).addTo(window.karte);
                    var marker = L.marker([gpsLati, gpsLong]).addTo(window.karte);
                }

                window.clearTimeout(timeout);
            }, 100);
        };

        // show button to hide and show search filter for mobile devices
        this.initSearchFilter = function () {
            $('#buttonSearchFilter').click(function () {
                $('#leftcontent').toggle();
            });
            $("#room-details-opener").click(function() {
                $("#room-details-container").css({display: 'block'});
            });
        };

        this.initReadMore = function () {
            $('#features_more_DOR').click(function () {
                $('.detail_view_feature_list').animate({'height': '100%'}, 'slow');
                $('#features_more_DOR').css({display: 'none'});
                $('#features_less_DOR').css({display: 'block'});
            });
            $('#features_less_DOR').click(function () {
                $('.detail_view_feature_list').animate({'height': '77px'}, 'slow');
                $('#features_less_DOR').css({display: 'none'});
                $('#features_more_DOR').css({display: 'block'});
            });
            $('#read_more_DOR').click(function () {
                $('#detailObjectRemark').animate({'height': '100%'}, 'slow');
                $('#read_more_DOR').css({display: 'none'});
                $('#read_less_DOR').css({display: 'block'});
            });
            $('#read_less_DOR').click(function () {
                $('#detailObjectRemark').animate({'height': '103px'}, 'slow');
                $('#read_less_DOR').css({display: 'none'});
                $('#read_more_DOR').css({display: 'block'});
            });
            $('#showMap_DOR').click(function () {
                $('.detail_content #karte').show();
                $('#showMap_DOR').css({display: 'none'});
                $('#hideMap_DOR').css({display: 'inline'});
            });
            $('#hideMap_DOR').click(function () {
                $('.detail_content #karte').hide();
                $('#hideMap_DOR').css({display: 'none'});
                $('#showMap_DOR').css({display: 'inline'});
            });
            $('#showCalender_DOR').click(function () {
                $('.detail_content #belegungskalender').show();
                $('.detail_content #belegungskalender-legende').show();
                $('#showCalender_DOR').css({display: 'none'});
                $('#hideCalender_DOR').css({display: 'inline'});
            });
            $('#hideCalender_DOR').click(function () {
                $('.detail_content #belegungskalender').hide();
                $('.detail_content #belegungskalender-legende').hide();
                $('#hideCalender_DOR').css({display: 'none'});
                $('#showCalender_DOR').css({display: 'inline'});
            });
            $('#showRating_DOR').click(function () {
                $('.detail_content #objectRating').show();
                $('#showRating_DOR').css({display: 'none'});
                $('#hideRating_DOR').css({display: 'inline'});
            });
            $('#hideRating_DOR').click(function () {
                $('.detail_content #objectRating').hide();
                $('#hideRating_DOR').css({display: 'none'});
                $('#showRating_DOR').css({display: 'inline'});
            });
        };

        this.initScrollWindow = function () {
            $(window).scroll(function () {
                var a = $(document).scrollTop();
                //console.log(a);
                var h = a;
                //console.log(h);
                if (a > 710) {
                    //$('.detail_content_right').offset({top: h});
                    $('.detail_content_right').css({position: 'fixed', top: '0'});
                    $('#scrollToTop').css({display: 'block', opacity: 0.66});

                } else {
                    $('.detail_content_right').css({position: 'relative', top: '0'});
                    $('#scrollToTop').css({opacity: 0, display: 'none'});
                }
            });
        };

        this.featureSort = function () {
            $('.detail_view_feature_list li').each(function () {
                var a = $('.detail_view_feature_list li').index(this);

                a = (a / 4).toString();
                var splitter = a.indexOf(".");
                a = a.substr(splitter, 3);
                var featureName = $(this).text();
                var b = featureName.substr(0, 13);
                if (featureName.length < 15) {
                    b = '<span  data-toggle="tooltip" data-placement="top" title="' + featureName + '\"><i class="fa fa-check"></i>' + b + '</span>';
                    $(this).html(b);

                    if (a == '.25') {
                        $(this).addClass('secondFour');
                    }
                    if (a == '.5') {
                        $(this).addClass('thirdFour');
                    }
                    if (a == '.75') {
                        $(this).addClass('fourthFour');
                    }
                    else {
                        $(this).addClass('firstFour');
                    }
                    //console.log(a);
                } else {
                    b = '<span  data-toggle="tooltip" data-placement="top" title="' + featureName + '\"><i class="fa fa-check"></i>' + b + '.</span>';
                    //console.log(b);
                    $(this).html(b);
                    if (a == '.25') {
                        $(this).addClass('secondFour');
                    }
                    if (a == '.5') {
                        $(this).addClass('thirdFour');
                    }
                    if (a == '.75') {
                        $(this).addClass('fourthFour');
                    }
                    else {
                        $(this).addClass('firstFour');
                    }
                    //console.log(a);
                }
                if (a == '.25') {
                    $(this).addClass('secondFour');
                }
                if (a == '.5') {
                    $(this).addClass('thirdFour');
                }
                if (a == '.75') {
                    $(this).addClass('fourthFour');
                }
                //console.log(a);
            })
        };

        // open modal window for the booking process for a room
        this.modalShowBookingProcess = function () {
            $('.detail_content_right .showDetails').click(function () {
                var firstBookingStepUrl = $('.detail_content_right .showDetails a').attr('href');
                ref.modalBox = $('#bw-modal-booking1');

                ref.modalBox.modal();
                ref.addAjaxLoadingImage(ref.modalBox.find('.modal-body'));
                ref.doAjax(firstBookingStepUrl, ref.modalBox.find('.modal-body'), ref.initFirstBookingStep);

                return false;
            });
            $('#bw-modal-detail-button-').click(function () {
                $('#bw-modal-detail').modal();
            });
        };

        this.parseDate = function(str) {
            var mdy = str.split('.');
            return new Date(mdy[2], mdy[1], mdy[0] - 1);
        };

        this.daydiff = function(first, second) {
            return (second - first) / (1000 * 60 * 60 * 24);
        };

        // init fields in the first booking step
        this.initFirstBookingStep = function() {
            //ref.removeAjaxLoadingImage($('bw-modal-booking1'));

            var fromDateField = $('#bw-modal-booking1-date-from');
            var toDateField = $('#bw-modal-booking1-date-to');

            var bookingSuggestionContainer = $('#bw-modal-booking1-form .bw-modal-booking1-object');

            var search_update_btn = $('#bw-modal-booking1-form .search_update');
            search_update_btn.hide();

            fromDateField.datepicker({
                dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                changeMonth: false,
                changeYear: false,
                minDate: '+1',
                onSelect: function() {
                    toDateField.datepicker('option', 'minDate', fromDateField.val());
                    toDateField.val('');
                    bookingSuggestionContainer.html('');
                    search_update_btn.show();
                }
            });

            toDateField.datepicker({
                dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
                firstDay: 1,
                dateFormat: 'dd.mm.yy',
                dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                changeMonth: false,
                changeYear: false,
                minDate: '+1',
                onSelect: function() {
                    bookingSuggestionContainer.html('');
                    search_update_btn.show();
                }
            });

            fromDateField.datepicker('option', 'minDate', new Date());

            if(fromDateField.val().length > 0) {
                fromDateField.datepicker('setDate', fromDateField.val());
                toDateField.datepicker('option', 'minDate', fromDateField.val());
            }
            else {
                toDateField.datepicker('option', 'minDate', new Date());
            }
            if(toDateField.val().length > 0) {
                toDateField.datepicker('setDate', toDateField.val());
            }

            ref.initSelectMenus();

            $('#bw-modal-booking1-form #bw-modal-booking1-search-adults').selectmenu({
                change: function (event, data) {
                    bookingSuggestionContainer.html('');
                    search_update_btn.show();
                }
            });

            // init child selection Filter
            $('#bw-modal-booking1-search-childs').selectmenu({
                change: function (event, data) {
                    var selectedNumberOfChilds = $('#bw-modal-booking1-search-childs').val();
                    var child_ages = $('.child_ages');

                    // reset form visibility
                    child_ages.find('.age-child-item').hide(); // hide all fields
                    child_ages.find('.age-child-item select').attr('disabled', 'disabled'); // disable all fields
                    child_ages.find('p').hide(); // hide text, if no childs are selected
                    //console.log(selectedNumberOfChilds);
                    if(selectedNumberOfChilds == 0) {   // show text, if no childs are selected
                        console.log(selectedNumberOfChilds);
                        child_ages.find('p').show();
                    }
                    else {
                        // show select fields according to the number of childs
                        child_ages.find('.age-child-item').each(function(index, element) {
                            if(index < selectedNumberOfChilds) {
                                $(element).show();
                                $(element).find('select').removeAttr('disabled');
                            }
                        });
                    }

                    if(child_ages.css('display') == 'none') child_ages.slideToggle();
                    bookingSuggestionContainer.html('');
                    search_update_btn.show();
                }
            });

            $('#booking_child_link').click(function() {
                $('.child_ages').slideToggle();
                return false;
            });

            $("#bw-modal-booking1-form").submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    'tx_tmchillio_searchform[datestart]': "required",
                    'tx_tmchillio_searchform[dateend]': "required",
                    'tx_tmchillio_searchform[room]': "required"
                },
                submitHandler: function (form) {
                    ref.modalBox.find('.modal-body').html(ref.ajaxLoadingImage);
                    ref.doAjaxForm(jQuery(form).attr('action'), jQuery(form), ref.modalBox.find('.modal-body'), ref.initSecondBookingStep);
                    return false;
                }
            });

            search_update_btn.click(function() {
                var child_age = [];
                $('.child_ages .child_age:enabled').each(function(index, el) {
                    child_age[index] = $(el).val();
                });

                var url = $('#bw-modal-booking1-form .search_update').attr('data-url');

                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        'datestart': $('#bw-modal-booking1-date-from').val(),
                        'dateend': $('#bw-modal-booking1-date-to').val(),
                        'adults': $('#bw-modal-booking1-search-adults').val(),
                        'childs': $('#bw-modal-booking1-search-childs').val(),
                        'child_age' : child_age
                    },
                    success: function (result) {
                        bookingSuggestionContainer.html(result);
                    },
                    beforeSend: function () {
                        bookingSuggestionContainer.html(ref.ajaxLoadingImage);
                        search_update_btn.hide();
                    },
                    error: function () {
                        //alert(url + data);
                        bookingSuggestionContainer.html(url);
                        search_update_btn.show();
                    }
                });
                return false;
            });
        };

        this.initSecondBookingStep = function() {
            jQuery('#bw-modal-step2-form').submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {},
                submitHandler: function (form) {
                    ref.doAjaxForm(jQuery(form).attr('action'), jQuery(form), ref.modalBox.find('.modal-body'), ref.initFinalBookingStep);
                    return false;
                }
            });
        };

        this.initFinalBookingStep = function() {
            $('#bw-modal-detail-button-final').click(function() {
                var url = $(this).attr('data-url');

                jQuery.ajax({
                    url: url,
                    success: function (result) {
                        ref.modalBox.find('.modal-body').html(result);
                    },
                    beforeSend: function () {
                        ref.modalBox.find('.modal-body').html(ref.ajaxLoadingImage);
                    },
                    error: function () {
                        //alert(url + data);
                        ref.modalBox.find('.modal-body').html(url);
                    }
                });
            });
        };

        this.addAjaxLoadingImage = function(container) {
            container.prepend('<img src="' + ref.ajaxLoadingImage + '" alt="wird geladen" class="ajax_loading_image" />');
        };

        this.removeAjaxLoadingImage = function(container) {
            container.find('.ajax_loading_image').remove();
        };

        this.loadCalendar = function () {
            var url = $('#belegungskalender').data("availabilityUrl");
            url = 'http://chillio.telematika.de/' + url;
            //console.log('test:' + url);

            var belegt = [];
            var auswahl = [];
            var startdate = $('#textfield-from').val();
            var enddate = $('#textfield-to').val();
            if (enddate.length == 0) {
                enddate = new Date().toLocaleDateString();
                startdate = new Date().toLocaleDateString();
            }
            var endarr = enddate.split(".");
            enddate = new Date(endarr[2].concat('/').concat(endarr[1]).concat('/').concat(endarr[0]));
            var startarr = startdate.split(".");
            startdate = new Date(startarr[2].concat('/').concat(startarr[1]).concat('/').concat(startarr[0]));
            var dayCount = Math.round(Math.abs(enddate - startdate) / (1000 * 60 * 60 * 24 )) + 1;
            //console.log(check1 + ' ,' + check2 + ' ,Tage' + dayCount);
            for (var j = 0; j < dayCount; j++) {
                var element = startdate;
                if (j === 0) {
                    element.setDate(element.getDate());
                    element = element.toLocaleDateString();
                    element = element.split(".");
                    if (element[1] < 10) {
                        element[1] = '0' + element[1];
                    }
                    if (element[0] < 10) {
                        element[0] = '0' + element[0];
                    }
                    element = element[2].concat('/').concat(element[1]).concat('/').concat(element[0]);
                    element.toString();

                    auswahl.push(element);
                } else {
                    element.setDate(element.getDate() + 1);
                    element = element.toLocaleDateString();
                    element = element.split(".");
                    if (element[1] < 10) {
                        element[1] = '0' + element[1];
                    }
                    if (element[0] < 10) {
                        element[0] = '0' + element[0];
                    }
                    element = element[2].concat('/').concat(element[1]).concat('/').concat(element[0]);
                    element.toString();

                    auswahl.push(element);
                }
            }

            /*console.log(auswahl);
             console.log(startdate + ' ,' + enddate);*/
            var result = $.ajax({
                url: url, dataType: 'text', success: function (data) {
                    //var json = JSON.parse(data);
                    var a = data.toString();
                    var n = a.indexOf("[");
                    var m = a.indexOf("]") + 1;
                    a = a.substring(n, m);
                    var items = JSON.parse(a);
                    //console.log(items);
                    for (var i = 0; i < items.length; i++) {
                        var check1 = items[i].departure;
                        var check2 = items[i].arrival;
                        if (check2 == null) {
                            check2 = new Date().toLocaleDateString();
                            check1 = new Date().getTime() + 365 * 24 * 60 * 60 * 1000;
                            check1 = new Date(check1).toLocaleDateString();
                            //console.log(check1 + ' ,' + check2);
                        }
                        var check1arr = check1.split(".");
                        check1 = new Date(check1arr[2].concat('/').concat(check1arr[1]).concat('/').concat(check1arr[0]));
                        var check2arr = check2.split(".");
                        check2 = new Date(check2arr[2].concat('/').concat(check2arr[1]).concat('/').concat(check2arr[0]));
                        var dayCount = Math.round(Math.abs(check1 - check2) / (1000 * 60 * 60 * 24 )) + 1;
                        //console.log(check1 + ' ,' + check2 + ' ,Tage' + dayCount);
                        for (var j = 0; j < dayCount; j++) {
                            var element = check2;
                            if (j === 0) {
                                element.setDate(element.getDate());
                                element = element.toLocaleDateString();
                                element = element.split(".");
                                if (element[1] < 10) {
                                    element[1] = '0' + element[1];
                                }
                                if (element[0] < 10) {
                                    element[0] = '0' + element[0];
                                }
                                element = element[2].concat('/').concat(element[1]).concat('/').concat(element[0]);
                                element.toString();

                                belegt.push(element);
                            } else {
                                element.setDate(element.getDate() + 1);
                                element = element.toLocaleDateString();
                                element = element.split(".");
                                if (element[1] < 10) {
                                    element[1] = '0' + element[1];
                                }
                                if (element[0] < 10) {
                                    element[0] = '0' + element[0];
                                }
                                element = element[2].concat('/').concat(element[1]).concat('/').concat(element[0]);
                                element.toString();

                                belegt.push(element);
                            }

                        }
                    }
                    // console.log(belegt);
                    $('#belegungskalender').datepicker({
                        dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                        monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                        monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],

                        dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                        dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
                        dateFormat: "yy/mm/dd",
                        numberOfMonths: 12,
                        firstDay: 1,
                        beforeShowDay: function (date) {
                            var month = ('0' + (date.getMonth() + 1)).slice(-2);
                            var day = ('0' + date.getDate()).slice(-2);
                            var year = date.getFullYear();
                            var formated_date = year + '/' + month + '/' + day;
                            if ($.inArray(formated_date, auswahl) !== -1) {
                                return [false, 'ui-state-sel', 'Ihr gewählter Zeitraum.'];
                            }
                            if ($.inArray(formated_date, belegt) !== -1) {
                                return [false, 'ui-state-free', 'An diesem Tag haben wir noch freie Zimmer.'];
                            }

                            return [false, 'ui-state-notfree', 'An diesem Tag sind wir belegt.'];
                        }
                    });
                    jQuery('.ui-datepicker-multi').css({width: '100%'});

                }
            });

        };

        this.goToCalendar = function () {
            $('.to_calendar').click(function () {
                $('.detail_content #belegungskalender').show();
                $('.detail_content #belegungskalender-legende').show();
                $('#showCalender_DOR').css({display: 'none'});
                $('#hideCalender_DOR').css({display: 'inline'});
                $('html, body').animate({
                    scrollTop: $("#belegungskalender").offset().top
                }, 2000);

            });
        };

        this.goToTop = function () {
            $('#scrollToTop').click(function () {
                $('html, body').animate({
                    scrollTop: 0
                }, 1200);

            });
        };

        this.getGpsForAllObjects = function () {

            //if map already contains markers, remove them first
            if(ref.markers) { deleteMarkers(); }

            ref.markers = L.markerClusterGroup();
            var link;

            var gpsDatenArr = [];
            var name;
            var gpsDatenBL = [
                ['Bitte Region wählen', 51.165691, 10.451526000000058, 6],
                ['Baden-Württemberg', 48.6616037, 9.350133599999936, 9],
                ['Bayern', 48.7904472, 11.497889499999928, 7],
                ['Berlin', 52.52000659999999, 13.404953999999975, 8],
                ['Brandenburg', 52.1313922, 13.216249400000038, 8],
                ['Bremen', 52.1313922, 13.216249400000038, 10],
                ['Hamburg', 53.5510846, 9.99368179999999, 10],
                ['Hessen', 50.6520515, 9.162437599999976, 8],
                ['Mecklenburg-Vorpommern', 53.6126505, 12.42959529999996, 8],
                ['Niedersachsen', 52.6367036, 9.845076500000005, 8],
                ['Nordrhein-Westfalen', 51.43323669999999, 7.6615937999999915, 8],
                ['Rheinland-Pfalz', 50.118346, 7.308952699999963, 8],
                ['Saarland', 49.3964234, 7.022960699999999, 10],
                ['Sachsen', 51.1045407, 13.201738400000067, 8],
                ['Sachsen-Anhalt', 51.9502649, 11.692273500000056, 8],
                ['Schleswig-Holstein', 54.21936720000001, 9.696116699999948, 8],
                ['Thüringen', 51.0109892, 10.845346000000063, 8]
            ];

            function deleteMarkers() {
                if(window.map.hasLayer(ref.markers)) {
                    window.map.removeLayer(ref.markers);
                }
            }

            function readGPSData() {
                // deleteMarkers();
                var gpsDatenElement = [];
                $('.accommodation_item').each(function () {
                    gpsDatenElement = [];
                    $(this).children('.showDetails').each( function(){
                        link = $(this).children('a').attr('href');
                    });
                    $(this).children('.gps-daten-object').children('span.gps').each(function () {
                        gpsDatenElement.push($(this).attr('data-value'));
                    });
                    gpsDatenElement.push($(this).find('.gps-preview').html());
                    gpsDatenArr.push(gpsDatenElement);
                });
                //console.log(gpsDatenArr);
                for (var h = 0; h < gpsDatenArr.length; h++) {
                    var latitude = gpsDatenArr[h][0];
                    var longitude = gpsDatenArr[h][1];
                    latitude = parseFloat(latitude);
                    longitude = parseFloat(longitude.substr(1, 12));
                    var title = gpsDatenArr[h][2];
                    var imgPath = gpsDatenArr[h][3];
                    var marker = L.marker(new L.LatLng(latitude, longitude, {title: title}));
                    //marker.bindPopup("<h4>" + title + "</h4><br><img class='map-popup-image' src='" + imgPath + "' /><div class='map-popup-right'><button class='popup-detail-button' value='" + link + "'><a class='doAjax' href='" + link + "'>Details</a></button> </div><div class='clear'></div>");
                    marker.bindPopup(gpsDatenArr[h][4]);
                    ref.markers.addLayer(marker);
                }
                //add new clusterlayer to map
                window.map.addLayer(ref.markers);
                //zomm to bounds of new clusterlayer
                var bounds = ref.markers.getBounds();
                var minZoom = window.map.getBoundsZoom(bounds);
                window.map.fitBounds(bounds);
                window.map.setZoom(minZoom);


                /*for (var i = 0; i < gpsDatenArr.length; i++) {
                 var latitude = gpsDatenArr[i][0];
                 var longitude = gpsDatenArr[i][1];
                 latitude = parseFloat(latitude);
                 longitude = parseFloat(longitude.substr(1, 12));
                 var title = gpsDatenArr[i][2];
                 var imgPath = gpsDatenArr[i][3];
                 console.log(latitude + ' ,' + longitude + ' ,' + title);
                 var marker = L.marker([latitude, longitude]).bindPopup("<h4>" + title + "</h4><br><img class='map-popup-image' src='" + imgPath + "' />");
                 markers.push(marker);

                 }*/
                //console.log(markers);

            }

            function centerMap() {

                var newname = $('#search_country option:selected').text();
                //console.log('name: ' + name);
                if (name != newname) {
                    name = newname;
                    var gpsBLlati = 0;
                    var gpsBLlong = 0;
                    var zoom;
                    gpsDatenArr = [];
                    //console.log(name);
                    for (var i = 0; i < gpsDatenBL.length; i++) {
                        if (name === gpsDatenBL[i][0]) {
                            gpsBLlati = gpsDatenBL[i][1];
                            //gpsBLlati = parseFloat(gpsBLlati);
                            gpsBLlong = gpsDatenBL[i][2];
                            zoom = gpsDatenBL[i][3];
                            //gpsBLlong = parseFloat(gpsBLlong.substr(1, 12));
                            //console.log(gpsBLlati + ' ,' + gpsBLlong);

                        }
                        //window.map.remove();
                        //window.map.panTo(new L.LatLng(gpsBLlati, gpsBLlong));

                    }
                    window.map.setView(new L.LatLng(gpsBLlati, gpsBLlong), zoom);
                    readGPSData();
                }

            }

            centerMap();


        };

        this.initLeaflet = function () {
            var initMapOptions = {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18,
                key: 'thomaslav.l28m5f39',
                accessToken: 'pk.eyJ1IjoidGhvbWFzbGF2IiwiYSI6ImNraTJRWTAifQ.FnfqV-2BDM-KwsppZq105w#11',
                center: [52.5075419, 13.4251364],
                zoom: 13
            };
            /*openstreetmap sweden tile layer
             var initMapOptions = {
             attribution: 'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
             maxZoom: 18,
             center: [52.5075419, 13.4251364],
             zoom: 13
             };
             //'http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png'
             */

            window.map = L.map('map').setView([52.5075419, 13.4251364], 13);
            L.tileLayer(
                'http://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token={accessToken}'
                ,
                initMapOptions
            ).addTo(window.map);

            window.map.on('popupopen', function() {
                ref.initAjaxDetailLinks(ref.initDetails);
                ref.map_area.find('.leaflet-popup-content a.doAjax').click(
                    function() {
                        ref.map_area.hide();
                        ref.list_area.show();
                    }
                );
            });
        };

        this.moveBackground = function() {
            var mousePosX = 0;
            var b = 0;
            var a = 0;
            var i = 0;
            function swipe(e){
                a = e.pageX;
                var c = a-b;
                if(i>0){
                    mousePosX = mousePosX + c;
                }
                //console.log(e.pageX +', '+mousePosX+' ,'+a+' ,'+b +' ,' + c);
                $("#start_slider").css({"animation":"none", cursor: 'move', 'background-position-x': mousePosX +'px'})
                b = a;
                i++;
            }
            $("#start_slider").mousedown(function() {
                $(this).bind("mousemove", swipe);

            });
            $("#start_slider").mouseup(function(){
                $( this ).unbind("mousemove", swipe);
                $("#start_slider").css({"animation":"animatedSlider 120s linear infinite", cursor: 'default', 'background-position-x': mousePosX +'px'});
                i=0;
                startChange();
            });
            function findKeyframesRule(rule)
            {
                var ss = document.styleSheets;
                //console.log(ss);
                for (var i = 0; i < ss.length; ++i) {

                    for (var j = 0; j < ss[i].cssRules.length; ++j) {
                        console.log(ss[i].cssRules.length);
                        if (ss[i].cssRules[j].type == '7'&&ss[i].cssRules[j].name == 'animatedSlider'){
                            console.log(ss[i].cssRules[j]);
                            return ss[i].cssRules[j];
                        }

                    }
                }

                return null;
            }

            function change(anim)
            {
                var keyframes = findKeyframesRule(anim);

                keyframes.deleteRule("from");
                //keyframes.deleteRule("100%");
                // var newmousePosX = mousePosX
                keyframes.appendRule("from { background-posiion: "+mousePosX+" px 0); }");
                //keyframes.insertRule("to { background-posiion: 100% 0 }");

                document.getElementById('start_slider').style.webkitAnimationName = anim;
            }

            function startChange()
            {
                document.getElementById('start_slider').style.webkitAnimationName = "none";

                setTimeout(function(){change("animatedSlider");}, 0);
            }

        };
        (function () {
            ref.init();
        })();
    }

    $(document).ready(function () {
        var chillioApp = new ChillioApplication();


        $(window).unload(function () {
            chillioApp = null;
        });
        function picToCover() {
            $('#sideholder.special').css({
                'max-width': '100%'
            });
            var innerHeight = $(window).height();
            var innerHeight1 = $('#special-second-frame').height();
            var innerHeight2 = $('#special-fifth-frame').height();
            var innerHeight5 = $('.special-offers-item').height();
            var innerHeight6 = $('.start-offers-item').height();
            innerHeight5 = innerHeight5 - 20;
            innerHeight6 = innerHeight6 - 20;
            var innerHeight3 = (innerHeight - innerHeight1);
            var innerHeight4 = (innerHeight - innerHeight2);
            //console.log(innerHeight);
            $('#special-first-frame').innerHeight(innerHeight3);
            $('#special-fourth-frame').innerHeight(innerHeight4);
            $('.special-offers-item-more').css({'margin': innerHeight5 + 'px 0 0 35%'});
            $('.start-offers-item-more').css({'margin': innerHeight6 + 'px 0 0 35%'});
            $('#special-first-frame .csc-textpic-text').addClass('special-first-frame-image-caption');
            $('#special-fourth-frame p.bodytext span').addClass('col-lg-12');
            $('.special-fifth-frame-text .csc-default').addClass('col-lg-6');
            $('#special-fifth-frame .csc-textpic-imagewrap img').addClass('special-fifth-frame-image-img col-lg-12');
        }

        $(window).resize(function () {
            var innerW = $('#wrapper').innerWidth();
            if (innerW > 768) {
                $('#leftcontent').show();
            } else {
                $('#leftcontent').hide();
            }
            picToCover();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();

        });
        $(function () {
            $('.special-next-frame a').bind('click', function (event) {
                var $anchor = $(this);
                //console.log($($anchor.attr('target')));
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('target')).offset().top
                }, 1500, 'easeInOutExpo');
                /*
                 if you don't want to use the easing effects:
                 $('html, body').stop().animate({
                 scrollTop: $($anchor.attr('href')).offset().top
                 }, 1000);
                 */
                event.preventDefault();
            });
        });
        picToCover();
        /**/
    });
})(jQuery, window);