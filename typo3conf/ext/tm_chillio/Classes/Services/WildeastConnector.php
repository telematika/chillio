<?php
namespace Telematika\TmChillio\Services;

/***************************************************************
 *  Copyright notice
 *
 * 2014 @author Thomas Reichwein
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;


/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class WildeastConnector {

	/**
	 * @var int
	 */
	protected $zImWebInterfaceId = 18360;

	/**
	 * @var string
	 */
	protected $wildEastInterfaceUrl = 'https://zimmer.im-web.de/Schnittstellen/ZiW.php';

	/**
	 * constructor
	 */
	public function __construct() {
		$this->initialize();
	}

	/**
	 * init function
	 */
	public function initialize() {
	}

	/**
	 * @var ConfigurationManager
	 */
	protected $configurationManager = null;

	/**
	 * xml based wildeast search
	 *
	 * @param $params
	 *
	 * @return \SimpleXMLElement
	 */
	public function wildeastSearchRequest($params) {
		$wildEastInterfaceUrl = $this->wildEastInterfaceUrl;
		$params['interface_id'] = $this->zImWebInterfaceId;
		$params['support_email'] = 'dev@telematika.de';

		$xml = new \SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><search xmlns="https://zimmer.im-web.de/Schnittstellen/xmlschema"></search>');

		$this->generateXML($params, $xml);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $wildEastInterfaceUrl);
		curl_setopt($ch, CURLOPT_POST, true);
		$post = array('xml'=> $xml->asXML());
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);

		$responseXML = new \SimpleXMLElement($response);

		return $responseXML;
	}

	/**
	 * build a xml tree from an Array (recursive)
	 *
	 * @param array $treeArray
	 * @param \SimpleXMLElement $parentNode
	 */
	protected function generateXML($treeArray, \SimpleXMLElement $parentNode) {
		foreach($treeArray as $key => $val) {
			if(is_array($val)) {
				if(is_int($key)) {  // exclude pure integer values
					$this->generateXML($val, $parentNode);
				}
				else {
					$node = $parentNode->addChild($key);
					$this->generateXML($val, $node);
				}
			}
			else if(!empty($val)) {
				$parentNode->addChild($key, $val);
			}
		}
	}

	/**
	 * basic function to establish connections
	 * @param $requestType
	 * @param $params
	 * @param bool $debugRequest
	 * @return mixed
	 */
	public function wildeastRequest($requestType, $params, $debugRequest = false) {
		$wildEastInterfaceUrl = $this->wildEastInterfaceUrl;
		//$this->configurationManager = new ConfigurationManager();
		//$config = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);

		//$params['para']['interface_id'] = $config['settings']['wildEastInterfaceId'];
		$params['para']['interface_id'] = $this->zImWebInterfaceId;

		// init base url for wild east inteface
		//$url = $config['settings']['wildEastInterfaceUrl'];
		$url = $wildEastInterfaceUrl;

		$url .= '?'.$requestType.'=1&para[support_email]=dev@telematika.de&';

		$url .= $this->formatParameters($params);

		if($debugRequest) {
			debug($url);
		}

		$xmlDoc = file_get_contents($url);

		$results = simplexml_load_string($xmlDoc);

		return $results;
		/**/
	}

	/**
	 * TODO: refactor to recursion
	 * formats the parameters for request
	 *
	 * @param array $params
	 * @return string formatted params
	 */
	protected function formatParameters($params = array() ) {
		$result = '';
		foreach($params as $key => $value) {
			if(is_array($value)) {
				foreach($value as $subkey => $subvalue) {
					if(is_array($subvalue)) {
						foreach($subvalue as $subkey2 => $subvalue2) {
							if(is_array($subvalue2)) {
								foreach($subvalue2 as $subkey3 => $subvalue3) {
									$result .= $key. '[' .$subkey. ']['.$subkey2.']['.$subkey3.']=' .urlencode($subvalue3). '&';
								}
							}
							else {
								if(isset($subvalue2) && (strlen($subvalue2) > 0)) {
									$result .= $key. '[' .$subkey. ']['.$subkey2.']=' .urlencode($subvalue2). '&';
								}
							}
						}
					}
					else {
						if(isset($subvalue) && (strlen($subvalue) > 0)) {
							$result .= $key. '[' .$subkey. ']=' .urlencode($subvalue). '&';
						}
					}
				}
			}
			else if(isset($value) && (strlen($value) > 0)) {
				$result .= $key. '='. urlencode($value). '&';
			}
		}
		return $result;
	}

	/**
	 * create log entries in a specific file
	 *
	 * @param null $fileName
	 * @param null $message
	 * @param string $level
	 */
	public function logInFile ($fileName = null, $message = null, $level = 'DEBUG') {
		$config = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);

		// check config for log file
		if($fileName === null) {
			$fileName = $config['settings']['default_log_file'];

			if(empty($fileName)) {
				return;
			}
		}

		// check and load directory of log file
		$dir = $config['settings']['default_log_dir'];
		if(empty($dir)) {
			return;
		}

		// check message
		if($message === null) {
			return;
		}

		// build path to log file
		$logFilePath = $dir.$fileName;

		// write log entry
		if($fp = fopen($logFilePath, 'a+')) {
			fwrite($fp, date('Y-m-d H:i:s') . ' | ' . strtoupper($level) . ' | ' . $message . "\n");
			fclose($fp);
		}

	}
}