<?php
namespace Telematika\TmChillio\Services;

/***************************************************************
 *  Copyright notice
 *
 * 2013 @author Thomas Reichwein
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package als
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class HelperFunc {


    /**
     * Schaut, ob der String leer ist.
     * Hierbei wird auch überprüft, dass der STring nicht nur aus
     * Anganben wie z.B. "-" oder " " besteht.
     */
    public static function isEmptyString( $string ) {

        $string = trim($string);

        if( empty($string) ) {
            return true;
        }

        switch ($string) {
            case '-':
            case '0':
            case '.':
                return true;
        }

        return false;
    }

    /**
     * helper function to validate a date
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function check_date($date, $format = 'd.m.Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * helper func to check emails
     *
     * @param $email string
     * @return bool
     */
    public static function check_email_address($email) {
        // First, we check that there's one @ symbol,
        // and that the lengths are right.
        if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
            // Email invalid because wrong number of characters
            // in one section or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\")$/",$local_array[$i])) {
                return false;
            }
        }
        // Check if domain is IP. If not,
        // it should be valid domain name
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) {
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
    //				if(!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/",$domain_array[$i])) {
                if(!preg_match("/^(([A-Za-z0-9äüößÜÄÖ][A-Za-z0-9äüößÜÄÖ-]{0,61}[A-Za-z0-9äüößÜÄÖ-])|([A-Za-z0-9]+))$/",$domain_array[$i])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * helper function to fix the problem of getting arguments
     * from a request, that are not destinated for a action
     * be careful with use
     *
     * @param string $argument
     * @return mixed
     */
    static public function getRequestArgument($argument = '') {
        if(!empty($argument)) {
            return \TYPO3\CMS\Core\Utility\GeneralUtility::_GET($argument);
        }
        else {
            return \TYPO3\CMS\Core\Utility\GeneralUtility::_GET();
        }
    }

}