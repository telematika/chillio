<?php
namespace Telematika\TmChillio\Domain\Model;


	/***************************************************************
	 *
	 *  Copyright notice
	 *
	 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Category
 * @lazy
 */
class Feature extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * id
	 *
	 * @var integer
	 */
	protected $id = 0;

	/**
	 * type
	 *
	 * @var string
	 */
	protected $type = '';

	/**
	 * cluster
	 *
	 * @var string
	 */
	protected $cluster = '';

	/**
	 * show on detail page
	 *
	 * @var int
	 */
	protected $showOnDetailpage = 1;

	/**
	 * show in search filter
	 *
	 * @var int
	 */
	protected $showInSearchfilter = 0;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the id
	 *
	 * @return integer $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Sets the id
	 *
	 * @param integer $id
	 * @return void
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * Returns the type
	 *
	 * @return string $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Sets the type
	 *
	 * @param string $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getCluster() {
		return $this->cluster;
	}

	/**
	 * @param string $cluster
	 */
	public function setCluster( $cluster ) {
		$this->cluster = $cluster;
	}

	/**
	 * @return int
	 */
	public function getShowOnDetailpage() {
		return $this->showOnDetailpage;
	}

	/**
	 * @param int $showOnDetailpage
	 */
	public function setShowOnDetailpage( $showOnDetailpage ) {
		$this->showOnDetailpage = $showOnDetailpage;
	}

	/**
	 * @return int
	 */
	public function getShowInSearchfilter() {
		return $this->showInSearchfilter;
	}

	/**
	 * @param int $showInSearchfilter
	 */
	public function setShowInSearchfilter( $showInSearchfilter ) {
		$this->showInSearchfilter = $showInSearchfilter;
	}

}