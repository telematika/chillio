<?php
namespace Telematika\TmChillio\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Region
 * @lazy
 */
class Region extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * region
	 *
	 * @var \Telematika\TmChillio\Domain\Model\Region
	 */
	protected $region = NULL;

	/**
	 * regid
	 *
	 * @var string
	 */
	protected $regid = NULL;

	/**
	 * zipcode
	 *
	 * @var int
	 */
	protected $zipcode = NULL;

	/**
	 * longtitude
	 *
	 * @var string
	 */
	protected $longtitude = '';

	/**
	 * latitude
	 *
	 * @var string
	 */
	protected $latitude = '';

	/**
	 * @var string
	 */
	protected $landingText = '';

	/**
	 * @var string
	 */
	protected $pageTitle = '';

	/**
	 * @var string
	 */
	protected $metaDescription = '';

	/**
	 * @var int
	 */
	protected $indexing = 0;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the region
	 *
	 * @return \Telematika\TmChillio\Domain\Model\Region $region
	 */
	public function getRegion() {
		return $this->region;
	}

	/**
	 * Sets the region
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 * @return void
	 */
	public function setRegion(\Telematika\TmChillio\Domain\Model\Region $region) {
		$this->region = $region;
	}

	/**
	 * @return string
	 */
	public function getRegid() {
		return $this->regid;
	}

	/**
	 * @param string $regid
	 */
	public function setRegid( $regid ) {
		$this->regid = $regid;
	}

	/**
	 * @return int
	 */
	public function getZipcode() {
		return $this->zipcode;
	}

	/**
	 * @param int $zipcode
	 */
	public function setZipcode( $zipcode ) {
		$this->zipcode = $zipcode;
	}

	/**
	 * @return string
	 */
	public function getLongtitude() {
		return $this->longtitude;
	}

	/**
	 * @param string $longtitude
	 */
	public function setLongtitude( $longtitude ) {
		$this->longtitude = $longtitude;
	}

	/**
	 * @return string
	 */
	public function getLatitude() {
		return $this->latitude;
	}

	/**
	 * @param string $latitude
	 */
	public function setLatitude( $latitude ) {
		$this->latitude = $latitude;
	}

	/**
	 * @return string
	 */
	public function getLandingText() {
		return $this->landingText;
	}

	/**
	 * @param string $landingText
	 */
	public function setLandingText( $landingText ) {
		$this->landingText = $landingText;
	}

	/**
	 * @return string
	 */
	public function getPageTitle() {
		return $this->pageTitle;
	}

	/**
	 * @param string $pageTitle
	 */
	public function setPageTitle( $pageTitle ) {
		$this->pageTitle = $pageTitle;
	}

	/**
	 * @return string
	 */
	public function getMetaDescription() {
		return $this->metaDescription;
	}

	/**
	 * @param string $metaDescription
	 */
	public function setMetaDescription( $metaDescription ) {
		$this->metaDescription = $metaDescription;
	}

	/**
	 * @return int
	 */
	public function getIndexing() {
		return $this->indexing;
	}

	/**
	 * @param int $indexing
	 */
	public function setIndexing( $indexing ) {
		$this->indexing = $indexing;
	}

}