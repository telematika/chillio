<?php
namespace Telematika\TmChillio\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Room
 * @lazy
 */
class Room extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * remark
	 *
	 * @var string
	 */
	protected $remark = '';

	/**
	 * roomId
	 *
	 * @var string
	 */
	protected $roomId = '';

	/**
	 * category
	 *
	 * @lazy
	 * @var \Telematika\TmChillio\Domain\Model\Category
	 */
	protected $category = NULL;

	/**
	 * minimum persons
	 *
	 * @var int
	 */
	protected $minPerson = 0;

	/**
	 * maximum persons
	 *
	 * @var int
	 */
	protected $maxPerson = 0;

	/**
	 * bedrooms
	 *
	 * @var int
	 */
	protected $bedrooms = 0;

	/**
	 * min price
	 *
	 * @var float
	 */
	protected $minPrice = 0.00;

	/**
	 * size in m^2
	 *
	 * @var int
	 */
	protected $size = 0;

	/**
	 * @return int
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * @param int $size
	 */
	public function setSize( $size ) {
		$this->size = $size;
	}

	/**
	 * image
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Image>
	 * @cascade remove
	 */
	protected $image = NULL;

	/**
	 * feature
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Feature>
	 * @cascade remove
	 */
	protected $feature = NULL;

	/**
	 * accommodation
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Accommodation>
	 */
	protected $accommodation = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->image = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->feature = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->accommodation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a Feature
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Feature $feature
	 * @return void
	 */
	public function addFeature(\Telematika\TmChillio\Domain\Model\Feature $feature) {
		$this->feature->attach($feature);
	}

	/**
	 * Removes a Feature
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Feature $featureToRemove The Feature to be removed
	 * @return void
	 */
	public function removeFeature(\Telematika\TmChillio\Domain\Model\Feature $featureToRemove) {
		$this->feature->detach($featureToRemove);
	}

	/**
	 * Returns the feature
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Feature> $feature
	 */
	public function getFeature() {
		return $this->feature;
	}

	/**
	 * Sets the feature
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Feature> $feature
	 * @return void
	 */
	public function setFeature(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $feature) {
		$this->feature = $feature;
	}

	/**
	 * Adds a Image
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Image $image
	 * @return void
	 */
	public function addImage(\Telematika\TmChillio\Domain\Model\Image $image) {
		$this->image->attach($image);
	}

	/**
	 * Removes a Image
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Image $imageToRemove The Image to be removed
	 * @return void
	 */
	public function removeImage(\Telematika\TmChillio\Domain\Model\Image $imageToRemove) {
		$this->image->detach($imageToRemove);
	}

	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Image> $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Image> $image
	 * @return void
	 */
	public function setImage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $image) {
		$this->image = $image;
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the remark
	 *
	 * @return string $remark
	 */
	public function getRemark() {
		return $this->remark;
	}

	/**
	 * Sets the remark
	 *
	 * @param string $remark
	 * @return void
	 */
	public function setRemark($remark) {
		$this->remark = $remark;
	}

	/**
	 * Returns the roomId
	 *
	 * @return string $roomId
	 */
	public function getRoomId() {
		return $this->roomId;
	}

	/**
	 * Sets the roomId
	 *
	 * @param string $roomId
	 * @return void
	 */
	public function setRoomId($roomId) {
		$this->roomId = $roomId;
	}

	/**
	 * Returns the category
	 *
	 * @return \Telematika\TmChillio\Domain\Model\Category $category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Sets the category
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Category $category
	 * @return void
	 */
	public function setCategory(\Telematika\TmChillio\Domain\Model\Category $category) {
		$this->category = $category;
	}

	/**
	 * @return int
	 */
	public function getMinPerson() {
		return $this->minPerson;
	}

	/**
	 * @param int $minPerson
	 */
	public function setMinPerson( $minPerson ) {
		$this->minPerson = $minPerson;
	}

	/**
	 * @return int
	 */
	public function getMaxPerson() {
		return $this->maxPerson;
	}

	/**
	 * @param int $maxPerson
	 */
	public function setMaxPerson( $maxPerson ) {
		$this->maxPerson = $maxPerson;
	}

	/**
	 * @return int
	 */
	public function getBedrooms() {
		return $this->bedrooms;
	}

	/**
	 * @param int $bedrooms
	 */
	public function setBedrooms( $bedrooms ) {
		$this->bedrooms = $bedrooms;
	}

	/**
	 * @return float
	 */
	public function getMinPrice() {
		return $this->minPrice;
	}

	/**
	 * @param float $minPrice
	 */
	public function setMinPrice( $minPrice ) {
		$this->minPrice = $minPrice;
	}

	/**
	 * @return mixed
	 */
	public function getAccommodation() {
		return $this->accommodation;
	}

	/**
	 * @param mixed $accommodation
	 */
	public function setAccommodation( $accommodation ) {
		$this->accommodation = $accommodation;
	}

}