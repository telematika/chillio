<?php
namespace Telematika\TmChillio\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Accommodation
 * @lazy
 */
class Accommodation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * objectId
	 *
	 * @var string
	 */
	protected $objectId = '';

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * street
	 *
	 * @var string
	 */
	protected $street = '';

	/**
	 * plz
	 *
	 * @var string
	 */
	protected $plz = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $city = '';

	/**
	 * remark
	 *
	 * @var string
	 */
	protected $remark = '';

	/**
	 * pets
	 *
	 * @var string
	 */
	protected $pets = '';

	/**
	 * category
	 *
	 * @lazy
	 * @var \Telematika\TmChillio\Domain\Model\Category
	 */
	protected $category = NULL;

	/**
	 * room
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Room>
	 * @cascade remove
	 */
	protected $room = NULL;

	/**
	 * image
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Image>
	 * @cascade remove
	 */
	protected $image = NULL;

	/**
	 * feature
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Feature>
	 * @cascade remove
	 */
	protected $feature = NULL;

	/**
	 * rating
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Rating>
	 * @cascade remove
	 */
	protected $rating = NULL;

	/**
	 * region
	 *
	 * @lazy
	 * @var \Telematika\TmChillio\Domain\Model\Region
	 */
	protected $region = NULL;

	/**
	 * min person by all room's min person
	 *
	 * @var int
	 */
	protected $minPerson = 0;

	/**
	 * max person by all room's max person
	 *
	 * @var int
	 */
	protected $maxPerson = 0;

	/**
	 * min bedrooms
	 *
	 * @var int
	 */
	protected $minBedRooms = 0;

	/**
	 * max bedrooms
	 *
	 * @var int
	 */
	protected $maxBedRooms = 0;

	/**
	 * min price for all rooms
	 *
	 * @var float
	 */
	protected $minPrice = 0.00;

	/**
	 * rating over all
	 *
	 * @var int
	 */
	protected $ratingOverall = 0;

	/**
	 * number of votes
	 *
	 * @var int
	 */
	protected $votes = 0;

	/**
	 * longtitude
	 *
	 * @var string
	 */
	protected $longitude = '';

	/**
	 * latitude
	 *
	 * @var string
	 */
	protected $latitude = '';

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->room = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->image = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->feature = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->rating = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->category = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the objectId
	 *
	 * @return string $objectId
	 */
	public function getObjectId() {
		return $this->objectId;
	}

	/**
	 * Sets the objectId
	 *
	 * @param string $objectId
	 * @return void
	 */
	public function setObjectId($objectId) {
		$this->objectId = $objectId;
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the street
	 *
	 * @return string $street
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * Sets the street
	 *
	 * @param string $street
	 * @return void
	 */
	public function setStreet($street) {
		$this->street = $street;
	}

	/**
	 * Returns the plz
	 *
	 * @return string $plz
	 */
	public function getPlz() {
		return $this->plz;
	}

	/**
	 * Sets the plz
	 *
	 * @param string $plz
	 * @return void
	 */
	public function setPlz($plz) {
		$this->plz = $plz;
	}

	/**
	 * Returns the city
	 *
	 * @return string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the remark
	 *
	 * @return string $remark
	 */
	public function getRemark() {
		return $this->remark;
	}

	/**
	 * Sets the remark
	 *
	 * @param string $remark
	 * @return void
	 */
	public function setRemark($remark) {
		$this->remark = $remark;
	}

	/**
	 * Returns the pets
	 *
	 * @return string $pets
	 */
	public function getPets() {
		return $this->pets;
	}

	/**
	 * Sets the pets
	 *
	 * @param string $pets
	 * @return void
	 */
	public function setPets($pets) {
		$this->pets = $pets;
	}

	/**
	 * Adds a Category
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(\Telematika\TmChillio\Domain\Model\Category $category) {
		$this->category->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Category $categoryToRemove The Category to be removed
	 * @return void
	 */
	public function removeCategory(\Telematika\TmChillio\Domain\Model\Category $categoryToRemove) {
		$this->category->detach($categoryToRemove);
	}

	/**
	 * Returns the category
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Category> $category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Sets the category
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Category> $category
	 * @return void
	 */
	public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category) {
		$this->category = $category;
	}

	/**
	 * Adds a Room
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Room $room
	 * @return void
	 */
	public function addRoom(\Telematika\TmChillio\Domain\Model\Room $room) {
		$this->room->attach($room);
	}

	/**
	 * Removes a Room
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Room $roomToRemove The Room to be removed
	 * @return void
	 */
	public function removeRoom(\Telematika\TmChillio\Domain\Model\Room $roomToRemove) {
		$this->room->detach($roomToRemove);
	}

	/**
	 * Returns the room
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Room> $room
	 */
	public function getRoom() {
		return $this->room;
	}

	/**
	 * Sets the room
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Room> $room
	 * @return void
	 */
	public function setRoom(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $room) {
		$this->room = $room;
	}

	/**
	 * Adds a Feature
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Feature $feature
	 * @return void
	 */
	public function addFeature(\Telematika\TmChillio\Domain\Model\Feature $feature) {
		$this->feature->attach($feature);
	}

	/**
	 * Removes a Feature
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Feature $featureToRemove The Feature to be removed
	 * @return void
	 */
	public function removeFeature(\Telematika\TmChillio\Domain\Model\Feature $featureToRemove) {
		$this->feature->detach($featureToRemove);
	}

	/**
	 * Returns the feature
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Feature> $feature
	 */
	public function getFeature() {
		return $this->feature;
	}

	/**
	 * Sets the feature
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Feature> $feature
	 * @return void
	 */
	public function setFeature(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $feature) {
		$this->feature = $feature;
	}

	/**
	 * Adds a Rating
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Rating $rating
	 * @return void
	 */
	public function addRating(\Telematika\TmChillio\Domain\Model\Rating $rating) {
		$this->rating->attach($rating);
	}

	/**
	 * Removes a Rating
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Rating $ratingToRemove The Rating to be removed
	 * @return void
	 */
	public function removeRating(\Telematika\TmChillio\Domain\Model\Rating $ratingToRemove) {
		$this->rating->detach($ratingToRemove);
	}

	/**
	 * Returns the rating
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Rating> $rating
	 */
	public function getRating() {
		return $this->rating;
	}

	/**
	 * Sets the rating
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Rating> $rating
	 * @return void
	 */
	public function setRating(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $rating) {
		$this->rating = $rating;
	}

	/**
	 * Adds a Image
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Image $image
	 * @return void
	 */
	public function addImage(\Telematika\TmChillio\Domain\Model\Image $image) {
		$this->image->attach($image);
	}

	/**
	 * Removes a Image
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Image $imageToRemove The Image to be removed
	 * @return void
	 */
	public function removeImage(\Telematika\TmChillio\Domain\Model\Image $imageToRemove) {
		$this->image->detach($imageToRemove);
	}

	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Image> $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Telematika\TmChillio\Domain\Model\Image> $image
	 * @return void
	 */
	public function setImage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $image) {
		$this->image = $image;
	}

	/**
	 * @return int
	 */
	public function getMinPerson() {
		return $this->minPerson;
	}

	/**
	 * @param int $minPerson
	 *
	 * @return $this
	 */
	public function setMinPerson( $minPerson ) {
		$this->minPerson = $minPerson;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getMaxPerson() {
		return $this->maxPerson;
	}

	/**
	 * @param int $maxPerson
	 *
	 * @return $this
	 */
	public function setMaxPerson( $maxPerson ) {
		$this->maxPerson = $maxPerson;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getMinBedRooms() {
		return $this->minBedRooms;
	}

	/**
	 * @param int $minBedRooms
	 */
	public function setMinBedRooms( $minBedRooms ) {
		$this->minBedRooms = $minBedRooms;
	}

	/**
	 * @return int
	 */
	public function getMaxBedRooms() {
		return $this->maxBedRooms;
	}

	/**
	 * @param int $maxBedRooms
	 */
	public function setMaxBedRooms( $maxBedRooms ) {
		$this->maxBedRooms = $maxBedRooms;
	}

	/**
	 * @return float
	 */
	public function getMinPrice() {
		return $this->minPrice;
	}

	/**
	 * @param float $minPrice
	 */
	public function setMinPrice( $minPrice ) {
		$this->minPrice = $minPrice;
	}

	/**
	 * @return int
	 */
	public function getRatingOverall() {
		return $this->ratingOverall;
	}

	/**
	 * @param int $ratingOverall
	 */
	public function setRatingOverall( $ratingOverall ) {
		$this->ratingOverall = $ratingOverall;
	}

	/**
	 * @return int
	 */
	public function getVotes() {
		return $this->votes;
	}

	/**
	 * @param int $votes
	 */
	public function setVotes( $votes ) {
		$this->votes = $votes;
	}

	/**
	 * @return string
	 */
	public function getLatitude() {
		return $this->latitude;
	}

	/**
	 * @param string $latitude
	 */
	public function setLatitude( $latitude ) {
		$this->latitude = $latitude;
	}

	/**
	 * @return string
	 */
	public function getLongitude() {
		return $this->longitude;
	}

	/**
	 * @param string $longitude
	 */
	public function setLongitude( $longitude ) {
		$this->longitude = $longitude;
	}

}