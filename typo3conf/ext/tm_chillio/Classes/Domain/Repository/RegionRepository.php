<?php
namespace Telematika\TmChillio\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Regions
 */
class RegionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * get Regions for first Level including top level region
	 *
	 * @param int $regionUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByRegionForFirstLevel($regionUid = 0) {
		$query = $this->createQuery();

		$query->matching(
			$query->equals('region', $regionUid)
		)->setOrderings(array('uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING));

		return $query->execute();
	}

	/**
	 * get Regions for third Level including check for available accommodations
	 * does not work because of typo3 bug not allowing a tablename more than once in a query
	 *
	 * @param int $regionUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByRegionHavingAccommodations($regionUid = 0) {
		$query = $this->createQuery();

		$query->matching(
			$query->logicalAnd(
				$query->equals('region', $regionUid),
				$query->logicalOr(
					$query->greaterThan('accommodation', 0),
					$query->greaterThan('region.accommodation', 0),
					$query->greaterThan('region.region.accommodation', 0)
				)
			)
		)->setOrderings(array('uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING));

		return $query->execute();
	}

	/**
	 * get Regions and parent Regions bei search string
	 * @param string $search
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findBySearchString($search = '') {
		if(sizeof($search) > 0) {
			$query = $this->createQuery();

			/*
			$query->matching(
				$query->logicalOr(
					$query->like('name', '%'.$search.'%'),
					$query->like('region.name', '%'.$search.'%')
				)
			);*/

			$sql = 'select region.name from tx_tmchillio_domain_model_region region where name like ' . '"%'.$search.'%"' . ' group by name order by uid limit 10;';

			/*
			$query->setOrderings(
				array('uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING)
			)->setLimit(10);*/
			return $query->statement($sql)->execute(TRUE);
		}
		else return Array();
	}
	
}