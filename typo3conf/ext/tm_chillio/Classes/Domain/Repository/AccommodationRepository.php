<?php
namespace Telematika\TmChillio\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Tests\Exception;

/**
 * The repository for Accommodations
 */
class AccommodationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * categoryRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;

	/**
	 * imageRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\ImageRepository
	 * @inject
	 */
	protected $imageRepository = NULL;

	/**
	 * featureRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\FeatureRepository
	 * @inject
	 */
	protected $featureRepository = NULL;

	/**
	 * ratingRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\RatingRepository
	 * @inject
	 */
	protected $ratingRepository = NULL;

	/**
	 * persistence manager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = NULL;

	/**
	 * wildeast connector
	 * @var \Telematika\TmChillio\Services\WildeastConnector
	 * @inject
	 */
	protected $wildeastConnector = NULL;

	/**
	 * search for accommodations
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function searchForRooms($params = Array()) {
		$result = $this->wildeastConnector->wildeastRequest('get[search]', $params);

		return $result;
	}

	/**
	 * @param array $params
	 */
	public function updateAccommodationRatings($params = Array()) {
		$this->ratingRepository->removeAll();

		$allAccommodations = $this->findAll();

		//$query = $this->createQuery();

		//$sql = 'SELECT acc.uid, acc.object_id FROM tx_tmchillio_domain_model_accommodation acc';
		//$allAccommodations = $query->statement($sql)->execute(TRUE);

		$i = 0;

		foreach($allAccommodations as $accommodation) {
			$params['para']['object'][$accommodation['object_id']] = 1;
			$i++;
			if($i%100 == 0) {
				$result = $this->wildeastConnector->wildeastRequest('get[ratings]', $params);
				$result = json_decode(json_encode($result), 1);

				foreach($result['ratings']['rating'] as $rating) {
					$acc = $this->findByObjectId($rating['@attributes']['objekt_id']);
					$acc = $acc[0];

					if($acc instanceof \Telematika\TmChillio\Domain\Model\Accommodation) {
						$acc->setRatingOverall($rating['average']);
						$acc->setVotes($rating['votes']);

						foreach($rating['categories']['category'] as $category) {
							$ratingObject = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Rating');
							if($ratingObject instanceof \Telematika\TmChillio\Domain\Model\Rating) {
								$ratingObject->setId($category['@attributes']['id']);
								$ratingObject->setPoints($category['points']);
							}
							$acc->addRating($ratingObject);
						}

						$this->update($acc);
					}
				}
				$params['para']['object'] = Array();
			}
		}
		$result = $this->wildeastConnector->wildeastRequest('get[ratings]', $params);
		$result = json_decode(json_encode($result), 1);

		foreach($result['ratings']['rating'] as $rating) {
			$acc = $this->findByObjectId($rating['@attributes']['objekt_id']);
			$acc = $acc[0];

			if($acc instanceof \Telematika\TmChillio\Domain\Model\Accommodation) {
				$acc->setRatingOverall($rating['average']);
				$acc->setVotes($rating['votes']);

				foreach($rating['categories']['category'] as $category) {
					$ratingObject = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Rating');
					if($ratingObject instanceof \Telematika\TmChillio\Domain\Model\Rating) {
						$ratingObject->setId($category['@attributes']['id']);
						$ratingObject->setPoints($category['points']);
					}
					$acc->addRating($ratingObject);
				}

				$this->update($acc);
			}
		}

	}

	/**
	 * get a accommodation by a object id, if it's older than 1 day
	 *
	 * @param int $objectId
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function checkForUpdateNeededAccommodation($objectId = 0) {
		$query = $this->createQuery();

		$query->matching(
			$query->logicalAnd(
				$query->equals('object_id', $objectId),
				$query->lessThanOrEqual('tstamp', (time() - 24 * 60 * 60))
			)
		);

		return $query->execute();
	}

	/**
	 * get accommodations by a list of object ids
	 *
	 * @param array $objectIds
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByObjectIds($objectIds = Array()) {
		if(sizeof($objectIds) > 0) {
			$query = $this->createQuery();

			$query->matching(
				$query->in('object_id', $objectIds)
			);

			return $query->execute();
		}
		else {
			return Array();
		}
	}

	/**
	 * find accommodations by their rooms(ids)
	 *
	 * @param array $roomIds
	 * @param string $sortby
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByRoomIds($roomIds = Array(), $sortby = '') {
		if(sizeof($roomIds) > 0) {
			$query = $this->createQuery();

			$sql = "SELECT
						acc.title as accommodation_title,
						acc.uid as accommodation_uid,
						acc.object_id as accommodation_object_id,
						acc.title as accommodation_title,
						acc.pets as accommodation_pets,
						acc.city as accommodation_city,
						acc.rating_overall as accommodation_rating_overall,
						acc.longitude as accommodation_longitude,
						acc.latitude as accommodation_latitude,
						MAX(bedrooms) as max_bedrooms,
						MIN(bedrooms) as min_bedrooms,
						MIN(min_price) as min_price,
						MIN(min_person) as min_person,
						MAX(max_person) as max_person,
						count(img.uid)
					FROM tx_tmchillio_domain_model_accommodation acc
					LEFT JOIN tx_tmchillio_domain_model_room ro on ro.accommodation = acc.uid
					LEFT JOIN tx_tmchillio_accommodation_image_mm img_mm on img_mm.uid_local = acc.uid
					LEFT JOIN tx_tmchillio_domain_model_image img on img.uid = img_mm.uid_foreign
					WHERE ro.room_id IN(" . implode(',', $roomIds). ")
					GROUP BY acc.uid
					HAVING count(img.uid) > 2
					LIMIT 10";

			switch($sortby) {
				case 'price':
					$sql .= 'ORDER BY ro.min_price ASC';
					break;
				case 'name':
					$sql .= 'ORDER BY acc.title ASC';
					break;
				case 'rating':
					$sql .= 'ORDER BY acc.rating_overall DESC';
					break;
				default:
					break;
			}

			$sql .= ";";

			return $query->statement($sql)->execute(TRUE);
		}
		else {
			return Array();
		}
	}

	/**
	 * create a new accommodation
	 *
	 * @param array $accommodationInformation
	 *
	 * @return object
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function createAccommodation($accommodationInformation = Array()) {
		$accommodation = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Accommodation');

		if($accommodation instanceof \Telematika\TmChillio\Domain\Model\Accommodation) {
			$accommodation->setObjectId($accommodationInformation['o_id']);
			$accommodation->setTitle($accommodationInformation['o_bezeichnung']);
			$accommodation->setStreet($accommodationInformation['o_strasse']);
			$accommodation->setPlz($accommodationInformation['o_plz']);
			$accommodation->setCity($accommodationInformation['o_ort']);
			$accommodation->setRemark($accommodationInformation['o_bemerkung']);
			$accommodation->setPets($accommodationInformation['o_haustiere_erlaubt']);
			if(!empty($accommodationInformation['o_gps']['longitude']) && !empty($accommodationInformation['o_gps']['latitude'])) {
				$accommodation->setLongitude((string)($accommodationInformation['o_gps']['longitude'] * -1));
				$accommodation->setLatitude($accommodationInformation['o_gps']['latitude']);
			}

			if(is_array($accommodationInformation['o_bild']) && is_array($accommodationInformation['o_bild']['bild'])) {
				foreach($accommodationInformation['o_bild']['bild'] as $o_bild) {
					$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
					if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
						$image->setPath($o_bild);
					}
					$this->imageRepository->add($image);
					$accommodation->addImage($image);
				}
			}
			else if(is_string($accommodationInformation['o_bild'])) {
				$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
				if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
					$image->setPath($accommodationInformation['o_bild']);
				}
				$this->imageRepository->add($image);
				$accommodation->addImage($image);
			}
			if(is_array($accommodationInformation['o_ausstattung']) && is_array($accommodationInformation['o_ausstattung']['ausstattung'])) {
				foreach($accommodationInformation['o_ausstattung']['ausstattung'] as $o_ausstattung) {

					$feature = NULL;
					$feature = $this->featureRepository->findById($o_ausstattung['@attributes']['position']);
					$feature = $feature[0];

					if($feature instanceof \Telematika\TmChillio\Domain\Model\Feature) {
						$accommodation->addFeature($feature);
					}
				}
			}
			//$category = $this->categoryRepository->findByIdAndType($accommodationInformation['o_category'], 'accommodation');
			//$accommodation->setCategory($category[0]);

			$this->add($accommodation);
		}

		return $accommodation;
	}

	/**
	 * create a new accommodation
	 *
	 * @param array $accommodationInformation
	 * @param \Telematika\TmChillio\Domain\Model\Accommodation $accommodation
	 *
	 * @return object|\Telematika\TmChillio\Domain\Model\Accommodation
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function updateAccommodation($accommodationInformation = Array(), \Telematika\TmChillio\Domain\Model\Accommodation $accommodation) {
		$accommodation->setObjectId($accommodationInformation['o_id']);
		$accommodation->setTitle($accommodationInformation['o_bezeichnung']);
		$accommodation->setStreet($accommodationInformation['o_strasse']);
		$accommodation->setPlz($accommodationInformation['o_plz']);
		$accommodation->setCity($accommodationInformation['o_ort']);
		$accommodation->setRemark($accommodationInformation['o_bemerkung']);
		$accommodation->setPets($accommodationInformation['o_haustiere_erlaubt']);
		if(!empty($accommodationInformation['o_gps']['longitude']) && !empty($accommodationInformation['o_gps']['latitude'])) {
			$accommodation->setLongitude((string)($accommodationInformation['o_gps']['longitude'] * -1));
			$accommodation->setLatitude($accommodationInformation['o_gps']['latitude']);
		}
		//$accommodation->setCategory($this->categoryRepository->findByIdAndType($accommodationInformation['o_category'], 'accommodation'));

		$this->imageRepository->removeByAccommodation($accommodation);

		if(is_array($accommodationInformation['o_bild']) && is_array($accommodationInformation['o_bild']['bild'])) {
			foreach($accommodationInformation['o_bild']['bild'] as $o_bild) {
				$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
				if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
					$image->setPath($o_bild);
				}
				$this->imageRepository->add($image);
				$accommodation->addImage($image);
			}
		}
		else if(is_string($accommodationInformation['o_bild'])) {
			$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
			if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
				$image->setPath($accommodationInformation['o_bild']);
			}
			$this->imageRepository->add($image);
			$accommodation->addImage($image);
		}

		if(is_array($accommodationInformation['o_ausstattung'])) {
			foreach($accommodationInformation['o_ausstattung'] as $o_ausstattung) {
				$feature = NULL;
				$feature = $this->featureRepository->findById($o_ausstattung['@attributes']['position']);

				if($feature instanceof \Telematika\TmChillio\Domain\Model\Feature) {
					$accommodation->addFeature($feature);
				}

			}
		}

		$this->update($accommodation);

		return $accommodation;
	}
}