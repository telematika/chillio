<?php
namespace Telematika\TmChillio\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Images
 */
class ImageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * remove alle images by accommodation
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Accommodation $accommodation
	 *
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function removeByAccommodation(\Telematika\TmChillio\Domain\Model\Accommodation $accommodation) {
		$query = $this->createQuery();

		$query->matching(
			$query->equals('accommodation', $accommodation->getUid())
		);

		foreach($query->execute() as $image) {
			$this->remove($image);
		}
	}

	/**
	 * remove alle images by room
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Room $room
	 *
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function removeByRoom(\Telematika\TmChillio\Domain\Model\Room $room) {
		$query = $this->createQuery();

		$query->matching(
			$query->equals('room', $room->getUid())
		);

		foreach($query->execute() as $image) {
			$this->remove($image);
		}
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return mixed
	 */
	public function findByAccommodationUid($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT img.uid, img.path
				FROM tx_tmchillio_domain_model_image img
				LEFT JOIN tx_tmchillio_accommodation_image_mm img_mm on img.uid = img_mm.uid_foreign
				LEFT JOIN tx_tmchillio_domain_model_accommodation acc on img_mm.uid_local = acc.uid
				WHERE acc.uid = " . $accommodationUid . ";";

		return $query->statement($sql)->execute(TRUE);

	}
}