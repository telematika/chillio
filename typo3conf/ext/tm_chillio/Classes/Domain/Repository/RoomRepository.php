<?php
namespace Telematika\TmChillio\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Tests\Exception;

/**
 * The repository for Rooms
 */
class RoomRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * wildeast connector
	 * @var \Telematika\TmChillio\Services\WildeastConnector
	 * @inject
	 */
	protected $wildeastConnector = NULL;

	/**
	 * categoryRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;

	/**
	 * imageRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\ImageRepository
	 * @inject
	 */
	protected $imageRepository = NULL;

	/**
	 * featureRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\FeatureRepository
	 * @inject
	 */
	protected $featureRepository = NULL;

	/**
	 * search for accommodations
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function searchForRooms($params = Array()) {
		$result = $this->wildeastConnector->wildeastSearchRequest($params);

		return $result;
	}

	/**
	 * get specific informations about accommodations
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function getRoomInformation($params = Array()) {
		$result = $this->wildeastConnector->wildeastRequest('get[rooms]', $params);

		return $result;
	}

	/**
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function createBooking($params = Array()) {
		$result = $this->wildeastConnector->wildeastRequest('get[reservation]', $params);

		return $result;
	}

	/**
	 * get a room by a object id, if it's older than 1 day
	 *
	 * @param int $roomId
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function checkForUpdateNeededRoom($roomId = 0) {
		$query = $this->createQuery();

		$query->matching(
			$query->logicalAnd(
				$query->equals('room_id', $roomId),
				$query->lessThanOrEqual('tstamp', (time() - 24 * 60 * 60))
			)
		);

		return $query->execute();
	}

	/**
	 * get accommodations by a list of object ids
	 *
	 * @param array $roomIds
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByRoomIds($roomIds = Array()) {
		if(sizeof($roomIds) > 0) {
			$query = $this->createQuery();

			$query->matching(
				$query->in('room_id', $roomIds)
			);

			return $query->execute();
		}
		else {
			return Array();
		}
	}

	/**
	 * create room
	 *
	 * @param array $roomInformation
	 *
	 * @return object
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function createRoom($roomInformation = Array()) {
		$room = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Room');

		if($room instanceof \Telematika\TmChillio\Domain\Model\Room) {
			$room->setTitle($roomInformation['z_bezeichnung']);
			$room->setRemark($roomInformation['z_bemerkung']);
			$room->setRoomId($roomInformation['@attributes']['id']);
			$room->setMinPerson($roomInformation['k_min_personen']);
			$room->setMaxPerson($roomInformation['k_max_personen']);
			$room->setBedrooms($roomInformation['k_bedrooms']);
			$room->setMinPrice($roomInformation['ab_preis']);
			//$room->setCategory($this->findByIdAndType($roomInformation['z_category'], 'room'));

			if(is_array($roomInformation['z_bild']) && is_array($roomInformation['z_bild']['bild'])) {
				foreach($roomInformation['z_bild']['bild'] as $z_bild) {
					$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
					if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
						$image->setPath($z_bild);
					}
					$this->imageRepository->add($image);
					$room->addImage($image);
				}
			}
			else if(is_string($roomInformation['z_bild'])) {
				$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
				if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
					$image->setPath($roomInformation['z_bild']);
				}
				$this->imageRepository->add($image);
				$room->addImage($image);
			}
			if(is_array($roomInformation['k_ausstattung']) && is_array($roomInformation['k_ausstattung']['ausstattung'])) {
				foreach($roomInformation['k_ausstattung']['ausstattung'] as $k_ausstattung) {

					$feature = NULL;
					$feature = $this->featureRepository->findById($k_ausstattung['@attributes']['position']);
					$feature = $feature[0];

					if($feature instanceof \Telematika\TmChillio\Domain\Model\Feature) {
						$room->addFeature($feature);
					}
				}
			}

			$this->add($room);
		}

		return $room;
	}

	/**
	 * update room
	 *
	 * @param array $roomInformation
	 * @param \Telematika\TmChillio\Domain\Model\Room $room
	 *
	 * @return \Telematika\TmChillio\Domain\Model\Room
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function updateRoom($roomInformation = Array(), \Telematika\TmChillio\Domain\Model\Room $room) {
		$room->setTitle($roomInformation['z_bezeichnung']);
		$room->setRemark($roomInformation['z_bemerkung']);
		$room->setRoomId($roomInformation['@attributes']['id']);
		$room->setMinPerson($roomInformation['k_min_personen']);
		$room->setMaxPerson($roomInformation['k_max_personen']);
		$room->setBedrooms($roomInformation['k_bedrooms']);
		$room->setMinPrice($roomInformation['ab_preis']);
		//$room->setCategory($this->findByIdAndType($roomInformation['z_category'], 'room'));

		$this->imageRepository->removeByRoom($room);

		if(is_array($roomInformation['z_bild']) && is_array($roomInformation['z_bild']['bild'])) {
			foreach($roomInformation['z_bild']['bild'] as $z_bild) {
				$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
				if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
					$image->setPath($z_bild);
				}
				$this->imageRepository->add($image);
				$room->addImage($image);
			}
		}
		else if(is_string($roomInformation['z_bild'])) {
			$image = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\Image');
			if($image instanceof \Telematika\TmChillio\Domain\Model\Image) {
				$image->setPath($roomInformation['z_bild']);
			}
			$this->imageRepository->add($image);
			$room->addImage($image);
		}
		if(is_array($roomInformation['k_ausstattung']) && is_array($roomInformation['k_ausstattung']['ausstattung'])) {
			foreach($roomInformation['k_ausstattung']['ausstattung'] as $k_ausstattung) {

				$feature = NULL;
				$feature = $this->featureRepository->findById($k_ausstattung['@attributes']['position']);
				$feature = $feature[0];

				if($feature instanceof \Telematika\TmChillio\Domain\Model\Feature) {
					$room->addFeature($feature);
				}
			}
		}

		$this->update($room);

		return $room;
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return int
	 */
	public function getMinPersonRoom($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT MIN(min_person) as min_person FROM tx_tmchillio_domain_model_room WHERE accommodation = " . $accommodationUid . ";";

		$result = $query->statement($sql)->execute(TRUE);

		if(empty($result['min_person'])) {
			return 1;
		}
		else {
			return $result['min_person'];
		}
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return int
	 */
	public function getMaxPersonRoom($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT MAX(min_person) as max_person FROM tx_tmchillio_domain_model_room WHERE accommodation = " . $accommodationUid . ";";

		$result = $query->statement($sql)->execute(TRUE);

		if(empty($result['max_person'])) {
			return 1;
		}
		else {
			return $result['max_person'];
		}
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return int
	 */
	public function getMinBedrooms($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT MIN(bedrooms) as min_bedrooms FROM tx_tmchillio_domain_model_room WHERE accommodation = " . $accommodationUid . ";";

		$result = $query->statement($sql)->execute(TRUE);

		if(empty($result['min_bedrooms'])) {
			return 1;
		}
		else {
			return $result['min_bedrooms'];
		}
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return int
	 */
	public function getMaxBedrooms($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT MAX(bedrooms) as max_bedrooms FROM tx_tmchillio_domain_model_room WHERE accommodation = " . $accommodationUid . ";";

		$result = $query->statement($sql)->execute(TRUE);

		if(empty($result['max_bedrooms'])) {
			return 1;
		}
		else {
			return $result['max_bedrooms'];
		}
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return float
	 */
	public function getMinPrice($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT MIN(min_price) as min_price FROM tx_tmchillio_domain_model_room WHERE accommodation = " . $accommodationUid . ";";

		$result = $query->statement($sql)->execute(TRUE);

		if(empty($result['min_price'])) {
			return 0.00;
		}
		else {
			return $result['min_price'];
		}
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return mixed
	 */
	public function getAggregateValuesForRoom($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT
					MAX(bedrooms) as max_bedrooms,
					MIN(bedrooms) as min_bedrooms,
					MIN(min_price) as min_price,
					MIN(min_person) as min_person,
					MAX(max_person) as max_person
				FROM tx_tmchillio_domain_model_room
				WHERE accommodation = " . $accommodationUid . ";";

		return $query->statement($sql)->execute(TRUE);
	}

	/**
	 * @param int $accommodationUid
	 *
	 * @return mixed
	 */
	public function findByAccommodationUid($accommodationUid = 0) {
		$query = $this->createQuery();

		$sql = "SELECT
					ro.title as room_title,
					ro.min_person as room_min_person,
					ro.max_person as room_max_person,
					ro.bedrooms as room_bedrooms,
					ro.min_price as room_min_price,
					ro.room_id as room_id,
					ro.size as room_size,
					ro.uid as room_uid
				FROM tx_tmchillio_domain_model_room ro
				WHERE accommodation = " . $accommodationUid . "
				GROUP BY ro.uid;";

		return $query->statement($sql)->execute(TRUE);
	}
}