<?php
namespace Telematika\TmChillio\ViewHelpers;

	/*                                                                        *
	 * This script is backported from the TYPO3 Flow package "TYPO3.Fluid".   *
	 *                                                                        *
	 * It is free software; you can redistribute it and/or modify it under    *
	 * the terms of the GNU Lesser General Public License, either version 3   *
	 *  of the License, or (at your option) any later version.                *
	 *                                                                        *
	 * The TYPO3 project - inspiring people to share!                         *
	 *                                                                        */
/**
 * View Helper which creates a simple checkbox (<input type="checkbox">).
 *
 * does the same, as api viewhelper, but doesn't render f****** hidden input field
 */
class CheckboxViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Form\CheckboxViewHelper {
	/**
	 * Initialize the arguments.
	 *
	 * @return void
	 * @api
	 */
	public function initializeArguments() {
		parent::initializeArguments();
	}

	/**
	 * Renders the checkbox.
	 *
	 * @param boolean $checked Specifies that the input element should be preselected
	 * @param boolean $multiple Specifies whether this checkbox belongs to a multivalue (is part of a checkbox group)
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string
	 * @api
	 */
	public function render($checked = NULL, $multiple = NULL) {
		$this->tag->addAttribute('type', 'checkbox');

		$nameAttribute = $this->getName();
		$valueAttribute = $this->getValue();
		if ($this->isObjectAccessorMode()) {
			if ($this->hasMappingErrorOccurred()) {
				$propertyValue = $this->getLastSubmittedFormData();
			} else {
				$propertyValue = $this->getPropertyValue();
			}

			if ($propertyValue instanceof \Traversable) {
				$propertyValue = iterator_to_array($propertyValue);
			}
			if (is_array($propertyValue)) {
				if ($checked === NULL) {
					$checked = in_array($valueAttribute, $propertyValue);
				}
				$nameAttribute .= '[]';
			} elseif ($multiple === TRUE) {
				$nameAttribute .= '[]';
			} elseif ($checked === NULL && $propertyValue !== NULL) {
				$checked = (bool)$propertyValue === (bool)$valueAttribute;
			}
		}
		$this->registerFieldNameForFormTokenGeneration($nameAttribute);
		$this->tag->addAttribute('name', $nameAttribute);
		$this->tag->addAttribute('value', $valueAttribute);
		if ($checked) {
			$this->tag->addAttribute('checked', 'checked');
		}
		$this->setErrorClassAttribute();
		return $this->tag->render();
	}
}