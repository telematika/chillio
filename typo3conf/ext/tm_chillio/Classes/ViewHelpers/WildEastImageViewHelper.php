<?php
namespace Telematika\TmChillio\ViewHelpers;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Tests\Exception;

/**
 * Resizes a given image (if required) and renders the respective img tag
 */
class WildEastImageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper  {
	/**
	 * @var string
	 */
	protected $tagName = 'img';

	/**
	 * @var \TYPO3\CMS\Extbase\Service\ImageService
	 * @inject
	 */
	protected $imageService;

	/**
	 * image cache path
	 * @var string
	 */
	protected $cachePath = 'fileadmin/_wildeast_/';

	/**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
	}

	/**
	 * Resizes a given image (if required) and renders the respective img tag
	 *
	 * @see http://typo3.org/documentation/document-library/references/doc_core_tsref/4.2.0/view/1/5/#id4164427
	 * @param string $src a path to a file, a combined FAL identifier or an uid (integer). If $treatIdAsReference is set, the integer is considered the uid of the sys_file_reference record. If you already got a FAL object, consider using the $image parameter instead
	 * @param string $width width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
	 * @param string $height height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
	 * @param integer $minWidth minimum width of the image
	 * @param integer $minHeight minimum height of the image
	 * @param integer $maxWidth maximum width of the image
	 * @param integer $maxHeight maximum height of the image
	 * @param boolean $treatIdAsReference given src argument is a sys_file_reference record
	 *
	 * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
	 * @return string Rendered tag
	 */
	public function render($src = NULL, $width = NULL, $height = NULL, $minWidth = NULL, $minHeight = NULL, $maxWidth = NULL, $maxHeight = NULL, $treatIdAsReference = FALSE) {
		$file_headers = @get_headers($src);

		$filename = Array();

		if(is_null($src) || $file_headers[0] == 'HTTP/1.1 404 Not Found' || !preg_match('/((?!\/)\w*(\.){1}(\w){3})/', $src, $filename)) {
			throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception('You must specify a string src to a valid image url.', 2382284106);
		}

		if(!is_file($this->cachePath . $filename[0])) {
			copy($src, $this->cachePath . $filename[0]);
		}

		$copySrc = $this->cachePath . $filename[0];

		if((int)$width > 0) {
			if(!is_file($this->cachePath. str_replace('.', '_w' . (int)$width . '.', $filename[0]))) {
				$newsrc = $this->resizeImageByIM($copySrc, $this->cachePath. str_replace('.', '_w' . (int)$width . '.', $filename[0]), $width, -1);
			}
			else {
				$newsrc = $this->cachePath. str_replace('.', '_w' . (int)$width . '.', $filename[0]);
			}
		}
		else if((int)$height > 0) {
			if(!is_file($this->cachePath. str_replace('.', '_h' . (int)$height . '.', $filename[0]))) {
				$newsrc = $this->resizeImageByIM($copySrc, $this->cachePath. str_replace('.', '_h' . (int)$height . '.', $filename[0]), $height, -2);
			}
			else {
				$newsrc = $this->cachePath. str_replace('.', '_h' . (int)$height . '.', $filename[0]);
			}
		}
		else {
			$newsrc = '';
		}

		$this->tag->addAttribute('src', $newsrc);
		$this->tag->addAttribute('width', (($width) ? $width.'px' : ''));
		$this->tag->addAttribute('height', (($height) ? $height.'px' : ''));

		$alt = $this->arguments['alt'];
		$title = $this->arguments['title'];

		// The alt-attribute is mandatory to have valid html-code, therefore add it even if it is empty
		if (empty($this->arguments['alt'])) {
			$this->tag->addAttribute('alt', $alt);
		}
		if (empty($this->arguments['title']) && $title) {
			$this->tag->addAttribute('title', $title);
		}

		return $this->tag->render();
	}

	/**
	 * @param $filepath_old
	 * @param $filepath_new
	 * @param $image_dimension
	 * @param int $scale_mode
	 *
	 * @return bool
	 */
	protected function resizeImage ($filepath_old, $filepath_new, $image_dimension, $scale_mode = 0) {
		if (is_file($filepath_new)) return $filepath_new;

		$image_attributes = getimagesize($filepath_old);
		$image_width_old = $image_attributes[0];
		$image_height_old = $image_attributes[1];
		$image_filetype = $image_attributes[2];

		if ($image_width_old <= 0 || $image_height_old <= 0) return '';
		$image_aspectratio = $image_width_old / $image_height_old;

		if ($scale_mode == 0) {
			$scale_mode = ($image_aspectratio > 1 ? -1 : -2);
		} elseif ($scale_mode == 1) {
			$scale_mode = ($image_aspectratio > 1 ? -2 : -1);
		}

		if ($scale_mode == -1) {
			$image_width_new = $image_dimension;
			$image_height_new = round($image_dimension / $image_aspectratio);
		} elseif ($scale_mode == -2) {
			$image_height_new = $image_dimension;
			$image_width_new = round($image_dimension * $image_aspectratio);
		} else {
			return '';
		}

		switch ($image_filetype) {
			case 1:
				$image_old = imagecreatefromgif($filepath_old);
				$image_new = imagecreate($image_width_new, $image_height_new);
				imagecopyresampled($image_new, $image_old, 0, 0, 0, 0, $image_width_new, $image_height_new, $image_width_old, $image_height_old);
				imagegif($image_new, $filepath_new);
				break;

			case 2:
				$image_old = imagecreatefromjpeg($filepath_old);
				$image_new = imagecreatetruecolor($image_width_new, $image_height_new);
				imagecopyresampled($image_new, $image_old, 0, 0, 0, 0, $image_width_new, $image_height_new, $image_width_old, $image_height_old);
				imagejpeg($image_new, $filepath_new);
				break;

			case 3:
				$image_old = imagecreatefrompng($filepath_old);
				$image_colordepth = imagecolorstotal($image_old);

				if ($image_colordepth == 0 || $image_colordepth > 255) {
					$image_new = imagecreatetruecolor($image_width_new, $image_height_new);
				} else {
					$image_new = imagecreate($image_width_new, $image_height_new);
				}

				imagealphablending($image_new, false);
				imagecopyresampled($image_new, $image_old, 0, 0, 0, 0, $image_width_new, $image_height_new, $image_width_old, $image_height_old);
				imagesavealpha($image_new, true);
				imagepng($image_new, $filepath_new);
				break;

			default:
				return '';
		}

		imagedestroy($image_old);
		imagedestroy($image_new);
		return $filepath_new;
	}

	/**
	 * @param $filepath_old
	 * @param $filepath_new
	 * @param $image_dimension
	 * @param int $scale_mode
	 *
	 * @return string
	 */
	protected function resizeImageByIM($filepath_old, $filepath_new, $image_dimension, $scale_mode = 0) {
		if (is_file($filepath_new)) return $filepath_new;

		$image_attributes = getimagesize($filepath_old);
		$image_width_old = $image_attributes[0];
		$image_height_old = $image_attributes[1];
		$image_filetype = $image_attributes[2];

		if ($image_width_old <= 0 || $image_height_old <= 0) return '';
		$image_aspectratio = $image_width_old / $image_height_old;

		if ($scale_mode == 0) {
			$scale_mode = ($image_aspectratio > 1 ? -1 : -2);
		} elseif ($scale_mode == 1) {
			$scale_mode = ($image_aspectratio > 1 ? -2 : -1);
		}

		if ($scale_mode == -1) {
			$image_width_new = $image_dimension;
			$image_height_new = round($image_dimension / $image_aspectratio);
		} elseif ($scale_mode == -2) {
			$image_height_new = $image_dimension;
			$image_width_new = round($image_dimension * $image_aspectratio);
		} else {
			return '';
		}

		if($image_filetype > 0 && $image_filetype < 4) {
			$thumb = new \Imagick($filepath_old);

			$thumb->resizeImage($image_width_new,$image_height_new,\Imagick::FILTER_LANCZOS,1);
			$thumb->writeImage($filepath_new);

			$thumb->destroy();
		}
		else {
			return '';
		}
		return $filepath_new;
	}

}
