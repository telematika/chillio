<?php
namespace Telematika\TmChillio\ViewHelpers;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Fluid\ViewHelpers\Form\TextfieldViewHelper;

/**
 * Class TmTextfieldViewHelper
 * @package Telematika\TmChillio\ViewHelpers
 */
class TmTextfieldViewHelper extends TextfieldViewHelper {


	/**
	 * Initialize the arguments.
	 *
	 * @return void
	 * @api
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerTagAttribute('minlength', 'string', 'HTML5 - define minlength for javascript validation');
	}
}