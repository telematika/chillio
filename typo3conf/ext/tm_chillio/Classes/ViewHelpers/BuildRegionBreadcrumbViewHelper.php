<?php
namespace Telematika\TmChillio\ViewHelpers;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class RegionSelectViewHelper
 * @package Telematika\TmChillio\ViewHelpers
 */
class BuildRegionBreadcrumbViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @var mixed
	 */
	protected $selectedValue = NULL;

	/**
	 * Initialize arguments.
	 *
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('class', 'string', 'CSS class for ul tag', FALSE);
		$this->registerArgument('region', 'object', 'starting region', TRUE);
		$this->registerArgument('excludeRegionUids', 'array', 'excluded regions from breadcrumb', FALSE);
		$this->registerArgument('pageUid', 'int', 'uid for target page', TRUE);
	}

	/**
	 * Render the tag.
	 *
	 * @return string rendered tag.
	 * @api
	 */
	public function render() {
		$region = $this->arguments['region'];
		if($region instanceof \Telematika\TmChillio\Domain\Model\Region) {
			$breadcrumb = '';
			$breadcrumb .= ($this->hasArgument('class')) ? '<ul class="' . $this->arguments['class'] . '" itemscope itemtype="http://schema.org/BreadcrumbList">' : '<ul>';
			$breadcrumb .= $this->buildBreadcrumb($region);
			$breadcrumb .= '</ul>';
			return $breadcrumb;
		}
		else {
			return '';
		}
	}

	/**
	 * recursion does not work )(&//§$§%"§$%&%!"§!!!!!!!!!!!
	 * so i have write bad code -.-
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 * @param string $content
	 *
	 * @return string
	 */
	protected function buildBreadcrumb(\Telematika\TmChillio\Domain\Model\Region $region, $content = '') {

		$uri = $this->buildRegionUri(
			$this->arguments['pageUid'],
			0, FALSE, FALSE, '', '', FALSE, array('region' => $region->getUid())
		);

		$content .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $uri . '" title="' . $region->getName() . '" itemprop="item">';
		$content .= '<span itemprop="name">' . $region->getName() . '</span>';
		$content .= '</a></li>';

		if(
			$region->getRegion() instanceof \Telematika\TmChillio\Domain\Model\Region
			&& strlen($region->getRegion()->getName()) > 0
		) {
			$Level2Region = $region->getRegion();
			//$content = $this->buildBreadcrumb($region, $content);

			$uri = $this->buildRegionUri(
				$this->arguments['pageUid'],
				0, FALSE, FALSE, '', '', FALSE, array('region' => $Level2Region->getUid())
			);

			$content .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $uri . '" title="' . $Level2Region->getName() . '" itemprop="item">';
			$content .= '<span itemprop="name">' . $Level2Region->getName() . '</span>';
			$content .= '</a><i class="fa fa-arrow-circle-right"></i></li>';

			if(
				$Level2Region->getRegion() instanceof \Telematika\TmChillio\Domain\Model\Region
				&& strlen($Level2Region->getRegion()->getName()) > 0
			) {
				$Level3Region = $Level2Region->getRegion();

				$uri = $this->buildRegionUri(
					$this->arguments['pageUid'],
					0, FALSE, FALSE, '', '', FALSE, array('region' => $Level3Region->getUid())
				);

				$content .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $uri . '" title="' . $Level3Region->getName() . '" itemprop="item">';
				$content .= '<span itemprop="name">' . $Level3Region->getName() . '</span>';
				$content .= '</a><i class="fa fa-arrow-circle-right"></i></li>';

				if(
					$Level3Region->getRegion() instanceof \Telematika\TmChillio\Domain\Model\Region
					&& strlen($Level3Region->getRegion()->getName()) > 0
				) {
					$Level4Region = $Level3Region->getRegion();

					$uri = $this->buildRegionUri(
						$this->arguments['pageUid'],
						0, FALSE, FALSE, '', '', FALSE, array('region' => $Level4Region->getUid())
					);

					$content .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $uri . '" title="' . $Level4Region->getName() . '" itemprop="item">';
					$content .= '<span itemprop="name">' . $Level4Region->getName() . '</span>';
					$content .= '</a><i class="fa fa-arrow-circle-right"></i></li>';

					if(
						$Level4Region->getRegion() instanceof \Telematika\TmChillio\Domain\Model\Region
						&& strlen($Level4Region->getRegion()->getName()) > 0
					) {
						$Level5Region = $Level4Region->getRegion();
						$uri = $this->buildRegionUri(
							$this->arguments['pageUid'],
							0, FALSE, FALSE, '', '', FALSE, array('region' => $Level5Region->getUid())
						);

						$content .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $uri . '" title="' . $Level5Region->getName() . '" itemprop="item">';
						$content .= '<span itemprop="name">' . $Level5Region->getName() . '</span>';
						$content .= '</a><i class="fa fa-arrow-circle-right"></i></li>';

						if(
							$Level5Region->getRegion() instanceof \Telematika\TmChillio\Domain\Model\Region
							&& strlen($Level5Region->getRegion()->getName()) > 0
						) {
							$Level6Region = $Level5Region->getRegion();
							$uri = $this->buildRegionUri(
								$this->arguments['pageUid'],
								0, FALSE, FALSE, '', '', FALSE, array('region' => $Level6Region->getUid())
							);
							$content .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $uri . '" title="' . $Level6Region->getName() . '" itemprop="item">';
							$content .= '<span itemprop="name">' . $Level6Region->getName() . '</span>';
							$content .= '</a><i class="fa fa-arrow-circle-right"></i></li>';
						}
					}
				}
			}
		}


		return $content;
	}

	/**
	 * @param integer $pageUid target page. See TypoLink destination
	 * @param integer $pageType type of the target page. See typolink.parameter
	 * @param boolean $noCache set this to disable caching for the target page. You should not need this.
	 * @param boolean $noCacheHash set this to supress the cHash query parameter created by TypoLink. You should not need this.
	 * @param string $section the anchor to be added to the URI
	 * @param string $format The requested format, e.g. ".html
	 * @param boolean $linkAccessRestrictedPages If set, links pointing to access restricted pages will still link to the page even though the page cannot be accessed.
	 * @param array $additionalParams additional query parameters that won't be prefixed like $arguments (overrule $arguments)
	 * @param boolean $absolute If set, the URI of the rendered link is absolute
	 * @param boolean $addQueryString If set, the current query parameters will be kept in the URI
	 * @param array $argumentsToBeExcludedFromQueryString arguments to be removed from the URI. Only active if $addQueryString = TRUE
	 * @param string $addQueryStringMethod Set which parameters will be kept. Only active if $addQueryString = TRUE
	 * @return string Rendered link
	 */
	public function buildRegionUri($pageUid = NULL, $pageType = 0, $noCache = FALSE, $noCacheHash = FALSE, $section = '', $format = '', $linkAccessRestrictedPages = FALSE, array $additionalParams = array(), $absolute = FALSE, $addQueryString = FALSE, array $argumentsToBeExcludedFromQueryString = array(), $addQueryStringMethod = NULL) {
		$uriBuilder = $this->controllerContext->getUriBuilder();
		$uri = $uriBuilder->reset()
		                  ->setTargetPageUid($pageUid)
		                  ->setTargetPageType($pageType)
		                  ->setNoCache($noCache)
		                  ->setUseCacheHash(!$noCacheHash)
		                  ->setSection($section)
		                  ->setFormat($format)
		                  ->setLinkAccessRestrictedPages($linkAccessRestrictedPages)
		                  ->setArguments($additionalParams)
		                  ->setCreateAbsoluteUri($absolute)
		                  ->setAddQueryString($addQueryString)
		                  ->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)
		                  ->setAddQueryStringMethod($addQueryStringMethod)
			              ->build();
		return $uri;
	}

}