<?php

namespace Telematika\TmChillio\ViewHelpers;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class AfratingViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * requires awesome font library!!!!
	 *
	 * @param int $rating
	 * @param int $maxrating
	 *
	 * @return string
	 */
	public function render($rating = 0, $maxrating = 0) {
		$result = '';
		for($i = 0; $i < (int)$maxrating; $i++) {
			if($i < (int)$rating) {
				$result .= '<i class="fa fa-star"></i>';
			}
			else {
				$result .= '<i class="fa fa-star-o"></i>';
			}
		}
		return $result;
	}
}