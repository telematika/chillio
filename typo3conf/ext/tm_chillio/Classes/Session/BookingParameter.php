<?php
namespace Telematika\TmChillio\Session;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class BookingParameter implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * salutation
	 *
	 * @var string
	 */
	protected $salutation = '';

	/**
	 * lastname
	 *
	 * @var string
	 */
	protected $lastname = '';

	/**
	 * firstname
	 *
	 * @var string
	 */
	protected $firstname = '';

	/**
	 * street
	 *
	 * @var string
	 */
	protected $street = '';

	/**
	 * zipcode
	 *
	 * @var string
	 */
	protected $zipcode = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $city = '';

	/**
	 * country
	 *
	 * @var string
	 */
	protected $country = '';

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

	/**
	 * phone
	 *
	 * @var string
	 */
	protected $phone = '';

	/**
	 * message
	 *
	 * @var string
	 */
	protected $message = '';

	/**
	 * room
	 *
	 * @var \Telematika\TmChillio\Domain\Model\Room
	 */
	protected $room = null;

	/**
	 * room id
	 *
	 * @var int
	 */
	protected $roomId = 0;

	/**
	 * room information
	 *
	 * @var array
	 */
	protected $roomInformation = Array();

	/**
	 * @var string
	 */
	protected $datestart = '';

	/**
	 * @var string
	 */
	protected $dateend = '';

	/**
	 * @var int
	 */
	protected $adults = 1;

	/**
	 * children information
	 * @var array
	 */
	protected $children = Array();

	/**
	 * @var \Telematika\TmChillio\Domain\Model\Region
	 */
	protected $region = null;

	/**
	 * @var array
	 */
	protected $exclusives = Array();

	/**
	 * @return string
	 */
	public function getSalutation() {
		return $this->salutation;
	}

	/**
	 * @param string $salutation
	 */
	public function setSalutation( $salutation ) {
		$this->salutation = $salutation;
	}

	/**
	 * @return string
	 */
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * @param string $lastname
	 */
	public function setLastname( $lastname ) {
		$this->lastname = $lastname;
	}

	/**
	 * @return string
	 */
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * @param string $firstname
	 */
	public function setFirstname( $firstname ) {
		$this->firstname = $firstname;
	}

	/**
	 * @return string
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet( $street ) {
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getZipcode() {
		return $this->zipcode;
	}

	/**
	 * @param string $zipcode
	 */
	public function setZipcode( $zipcode ) {
		$this->zipcode = $zipcode;
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity( $city ) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry( $country ) {
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone( $phone ) {
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage( $message ) {
		$this->message = $message;
	}

	/**
	 * @return int
	 */
	public function getRoomId() {
		return $this->roomId;
	}

	/**
	 * @param int $roomId
	 */
	public function setRoomId( $roomId ) {
		$this->roomId = $roomId;
	}

	/**
	 * @return array
	 */
	public function getRoomInformation() {
		return $this->roomInformation;
	}

	/**
	 * @param array $roomInformation
	 */
	public function setRoomInformation( $roomInformation ) {
		$this->roomInformation = $roomInformation;
	}

	/**
	 * @return \Telematika\TmChillio\Domain\Model\Room
	 */
	public function getRoom() {
		return $this->room;
	}

	/**
	 * @param \Telematika\TmChillio\Domain\Model\Room $room
	 */
	public function setRoom( $room ) {
		$this->room = $room;
	}

	/**
	 * @return string
	 */
	public function getDatestart() {
		return $this->datestart;
	}

	/**
	 * @param string $datestart
	 */
	public function setDatestart( $datestart ) {
		$this->datestart = $datestart;
	}

	/**
	 * @return string
	 */
	public function getDateend() {
		return $this->dateend;
	}

	/**
	 * @param string $dateend
	 */
	public function setDateend( $dateend ) {
		$this->dateend = $dateend;
	}

	/**
	 * @return int
	 */
	public function getAdults() {
		return $this->adults;
	}

	/**
	 * @param int $adults
	 */
	public function setAdults( $adults ) {
		$this->adults = $adults;
	}

	/**
	 * @return array
	 */
	public function getChildren() {
		return $this->children;
	}

	/**
	 * @param array $children
	 */
	public function setChildren( $children ) {
		$this->children = $children;
	}

	/**
	 * @return \Telematika\TmChillio\Domain\Model\Region
	 */
	public function getRegion() {
		return $this->region;
	}

	/**
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 */
	public function setRegion( $region ) {
		$this->region = $region;
	}

	/**
	 * @return array
	 */
	public function getExclusives() {
		return $this->exclusives;
	}

	/**
	 * @param array $exclusives
	 */
	public function setExclusives( $exclusives ) {
		$this->exclusives = $exclusives;
	}
}