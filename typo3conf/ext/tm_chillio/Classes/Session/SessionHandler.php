<?php
namespace Telematika\TmChillio\Session;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class SessionHandler implements \TYPO3\CMS\Core\SingletonInterface {


    const SESSIONNAMESPACE = 'tx_tmchillio_p1';

    /**
     * constructor
     */
    public function __construct() {}

    /**
     * Returns the object stored in the users PHP session
     * @return Object the stored object
     */
    public function restoreFromSession() {
        $sessionData = $GLOBALS['TSFE']->fe_user->getKey('ses', self::SESSIONNAMESPACE);
        return unserialize($sessionData);
    }

    /**
     * Writes an object into the PHP session
     * @param object $object any serializable object to store into the session
     * @return SessionHandler this
     */
    public function writeToSession($object) {
        $sessionData = serialize($object);
        $GLOBALS['TSFE']->fe_user->setKey('ses', self::SESSIONNAMESPACE, $sessionData);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
        return $this;
    }

    /**
     * Cleans up the session: removes the stored object from the PHP session
     * @return SessionHandler this
     */
    public function cleanUpSession() {
        $GLOBALS['TSFE']->fe_user->setKey('ses', self::SESSIONNAMESPACE, NULL);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
        return $this;
    }
}