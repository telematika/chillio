<?php
namespace Telematika\TmChillio\Session;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class Wishlist implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * objectIds
	 *
	 * @var array
	 */
	protected $objectIds = Array();

	/**
	 * attach object
	 *
	 * @param int $objectId
	 */
	public function attachObject($objectId) {
		$this->objectIds[$objectId] = 1;
	}

	/**
	 * remove object
	 *
	 * @param int $objectId
	 */
	public function removeObject($objectId) {
		$this->objectIds[$objectId] = null;
	}

	/**
	 * check, if a object is in wishlist
	 *
	 * @param int $objectId
	 *
	 * @return bool
	 */
	public function isObjectInWishlist($objectId) {
		if($this->$objectId[$objectId] === 1) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * get all object ids
	 * @return array
	 */
	public function getObjectIds() {
		$objectIds = Array();
		foreach($this->objectIds as $key => $val) {
			$objectIds[] = $key;
		}
		return $objectIds;
	}
}