<?php
namespace Telematika\TmChillio\Session;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class SearchParameter implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * @var string
	 */
	protected $datestart = '';

	/**
	 * @var string
	 */
	protected $dateend = '';

	/**
	 * @var int
	 */
	protected $adults = 1;

	/**
	 * @var int
	 */
	protected $region = 0;

	/**
	 * @var array
	 */
	protected $children = Array();

	/**
	 * @return int
	 */
	public function getAdults() {
		return $this->adults;
	}

	/**
	 * @param int $adults
	 */
	public function setAdults( $adults ) {
		$this->adults = $adults;
	}

	/**
	 * @return string
	 */
	public function getDateend() {
		return $this->dateend;
	}

	/**
	 * @param string $dateend
	 */
	public function setDateend( $dateend ) {
		$this->dateend = $dateend;
	}

	/**
	 * @return string
	 */
	public function getDatestart() {
		return $this->datestart;
	}

	/**
	 * @param string $datestart
	 */
	public function setDatestart( $datestart ) {
		$this->datestart = $datestart;
	}

	/**
	 * @return int
	 */
	public function getRegion() {
		return $this->region;
	}

	/**
	 * @param int $region
	 */
	public function setRegion( $region ) {
		$this->region = $region;
	}

	/**
	 * @return array
	 */
	public function getChildren() {
		return $this->children;
	}

	/**
	 * @param array $children
	 */
	public function setChildren( $children ) {
		$this->children = $children;
	}
}