<?php
namespace Telematika\TmChillio\Session;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


class SessionStorage implements \TYPO3\CMS\Core\SingletonInterface {

    /**
     * @var array
     */
    protected $objectContainer = Array();

    /**
     * add a new object
     *
     * @param $object
     * @param string $key
     */
    public function attachObject($object, $key) {
        $this->objectContainer[$key] = $object;
    }

    /**
     * remove an object
     *
     * @param string $key
     */
    public function removeObject($key) {
        $this->objectContainer[$key] = null;
    }

    /**
     * reset container to empty array
     */
    public function removeAllObjects() {
        $this->objectContainer = Array();
    }

    /**
     * return an object
     *
     * @param string $key
     */
    public function getObjectByKey($key) {
       return $this->objectContainer[$key];
    }
}