<?php
namespace Telematika\TmChillio\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Tests\Exception;

/**
 * AccommodationController
 */
class AccommodationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * accommodationRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\AccommodationRepository
	 * @inject
	 */
	protected $accommodationRepository = NULL;

	/**
	 * regionRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\RegionRepository
	 * @inject
	 */
	protected $regionRepository = NULL;

	/**
	 * categoryRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;

	/**
	 * roomRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\RoomRepository
	 * @inject
	 */
	protected $roomRepository = NULL;

	/**
	 * imageRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\ImageRepository
	 * @inject
	 */
	protected $imageRepository = NULL;

	/**
	 * featureRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\FeatureRepository
	 * @inject
	 */
	protected $featureRepository = NULL;

	/**
	 * persistence manager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager = NULL;

	/**
	 * @var int
	 */
	protected $parentRegion = NULL;

	/**
	 * session handler
	 *
	 * @var \Telematika\TmChillio\Session\SessionHandler
	 * @inject
	 */
	protected $sessionHandler = NULL;

	/**
	 * session storage
	 *
	 * @var \Telematika\TmChillio\Session\SessionStorage
	 */
	protected $sessionStorage = NULL;

	/**
	 * helper functions
	 *
	 * @var \Telematika\TmChillio\Services\HelperFunc
	 * @inject
	 */
	protected $helperFunc = NULL;

	/**
	 * @var \Telematika\TmChillio\Domain\Repository\WildeastSessionRepository
	 * @inject
	 */
	protected $wildeastSessionRepository = NULL;

	/**
	 * @var \Telematika\TmChillio\Session\SearchParameter
	 */
	protected $searchParameter = NULL;

	/**
	 * @var \Telematika\TmChillio\Session\BookingParameter
	 */
	protected $bookingParameter = NULL;

	/**
	 * @var \Telematika\TmChillio\Session\Wishlist
	 */
	protected $wishlist = NULL;

	/**
	 * wildeast session id for requests
	 * @var string
	 */
	protected $wildeastSessionId = '';

	/**
	 * initialize
	 */
	protected function initializeAction() {
		parent::initializeAction();
		$this->parentRegion = $this->settings['highestLvlRegion'];

		if (TYPO3_MODE === 'FE') {
			$this->sessionStorage = $this->sessionHandler->restoreFromSession();
			if(!($this->sessionStorage instanceof \Telematika\TmChillio\Session\SessionStorage)) {
				$this->sessionStorage = $this->objectManager->get('\Telematika\TmChillio\Session\SessionStorage');
				$this->sessionHandler->writeToSession($this->sessionStorage);
			}
		}
	}

	/**
	 * create an searchparameter object and store it in session
	 * return \Telematika\TmChillio\Session\SearchParameter
	 */
	protected function createSearchParameterSession() {
		$searchParameters = $this->objectManager->get('\Telematika\TmChillio\Session\SearchParameter');
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->attachObject($searchParameters, 'search');

		return $searchParameters;
	}

	/**
	 * restore search parameter
	 * @return \Telematika\TmChillio\Session\SearchParameter
	 */
	protected function getSearchParametersFromSession() {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$searchParameter = $this->sessionStorage->getObjectByKey('search');

		if($searchParameter instanceof \Telematika\TmChillio\Session\SearchParameter) {
			return $searchParameter;
		}
		else {
			return $this->createSearchParameterSession();
		}
	}

	/**
	 * save search parameters
	 * @param \Telematika\TmChillio\Session\SearchParameter $searchParameter
	 */
	protected function saveSearchParameterToSession(\Telematika\TmChillio\Session\SearchParameter $searchParameter) {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->attachObject($searchParameter, 'search');
		$this->sessionHandler->writeToSession($this->sessionStorage);
	}

	/**
	 * create booking session
	 * @return object
	 */
	protected function createBookingParameterSession() {
		$bookingParameters = $this->objectManager->get('\Telematika\TmChillio\Session\BookingParameter');
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->attachObject($bookingParameters, 'booking');

		return $bookingParameters;
	}

	/**
	 * restore booking parameter
	 * @return \Telematika\TmChillio\Session\BookingParameter
	 */
	protected function getBookingParametersFromSession() {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$bookingParameter = $this->sessionStorage->getObjectByKey('booking');

		if($bookingParameter instanceof \Telematika\TmChillio\Session\BookingParameter) {
			return $bookingParameter;
		}
		else {
			return $this->createBookingParameterSession();
		}
	}

	/**
	 * save booking parameters
	 * @param \Telematika\TmChillio\Session\BookingParameter $bookingParameter
	 */
	protected function saveBookingParameterToSession(\Telematika\TmChillio\Session\BookingParameter $bookingParameter) {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->attachObject($bookingParameter, 'booking');
		$this->sessionHandler->writeToSession($this->sessionStorage);
	}

	/**
	 * delete booking session
	 */
	protected function deleteBookingParameterSession() {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->removeObject('booking');
		$this->sessionHandler->writeToSession($this->sessionStorage);
	}

	/**
	 * restore wishlist
	 * @return \Telematika\TmChillio\Session\Wishlist
	 */
	protected function getWishlistsFromSession() {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$bookingParameter = $this->sessionStorage->getObjectByKey('wishlist');

		if($bookingParameter instanceof \Telematika\TmChillio\Session\Wishlist) {
			return $bookingParameter;
		}
		else {
			return $this->createWishlistSession();
		}
	}

	/**
	 * save wishlist
	 * @param \Telematika\TmChillio\Session\Wishlist $bookingParameter
	 */
	protected function saveWishlistToSession(\Telematika\TmChillio\Session\Wishlist $bookingParameter) {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->attachObject($bookingParameter, 'wishlist');
		$this->sessionHandler->writeToSession($this->sessionStorage);
	}

	/**
	 * delete wishlist
	 */
	protected function deleteWishlistSession() {
		$this->sessionStorage = $this->sessionHandler->restoreFromSession();
		$this->sessionStorage->removeObject('wishlist');
		$this->sessionHandler->writeToSession($this->sessionStorage);
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$accommodations = $this->accommodationRepository->findAll();
		$this->view->assign('accommodations', $accommodations);
	}

	/**
	 * init search form action params
	 */
	public function initializeSearchFormAction() {
		$this->searchParameter = $this->getSearchParametersFromSession();
	}

	/**
	 * display search form
	 *
	 * @param string $regionSword
	 * @param string $arrival
	 * @param string $departure
	 */
	public function searchFormAction($regionSword = '', $arrival = '', $departure = '') {
		// look for preselected region given by url
		$preselectedRegion = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('region');
		$preselectedRegionUid = (int)($preselectedRegion);

		// if region by search word exists, set it as preselected
		if(strlen($regionSword) > 0) {
			$preselectedRegion = $this->regionRepository->findByName($regionSword);
			$preselectedRegion = $preselectedRegion->getFirst();

			if($preselectedRegion instanceof \Telematika\TmChillio\Domain\Model\Region) {
				$preselectedRegionUid = $preselectedRegion->getUid();
			}
		}

		// if date is correct assign it
		if($this->datesAreValid($arrival, $departure)) {
			$this->view->assignMultiple(
				Array(
					'arrival' => $arrival,
					'departure' => $departure
				)
			);
		}
		else {
			$this->view->assignMultiple(
				Array(
					'arrival' => date('d.m.Y', time() + (1*24*60*60)),
					'departure' => date('d.m.Y', time() + (8*24*60*60))
				)
			);
		}

		// region is selected
		if($preselectedRegionUid > 0) {
			// get region model
			$region = $this->regionRepository->findByUid($preselectedRegionUid);

			// if it exists
			if($region instanceof \Telematika\TmChillio\Domain\Model\Region) {

				// set meta data for landing page
				$regionPageTitle = $region->getPageTitle();
				if(strlen($regionPageTitle) > 0) {
					$GLOBALS['TSFE']->page['title'] = $regionPageTitle;
				}
				$regionMetaDescription = $region->getMetaDescription();
				if(strlen($regionMetaDescription) > 0) {
					$metaDescription = '<meta name="description" content="' . $regionMetaDescription . '">';
					$this->response->addAdditionalHeaderData($metaDescription);
				}
				$indexing = $region->getIndexing();
				if($indexing == 1) {
					$this->response->addAdditionalHeaderData('<meta name="robots" content="index, follow">');
				}
				else {
					$this->response->addAdditionalHeaderData('<meta name="robots" content="noindex, follow">');
				}

				// no time to write good code
				// assign just first level region, if parent region is selected
				if($region->getUid() == $this->parentRegion) {
					$regions = $this->regionRepository->findByRegionForFirstLevel($this->parentRegion);
					$this->view->assign('regions', $regions);
				}
				// assign firstlevel region for generating first level of select menu
				else if($region->getRegion()->getUid() == $this->parentRegion) {
					$firstLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getUid());
					$secondLevelRegions = $this->regionRepository->findByRegion($region->getUid());

					$this->view->assignMultiple(
						Array(
							'preselectedFirstLevelRegion' => $region,
							'firstLevelRegions' => $firstLevelRegions,
							'secondLevelRegions' => $secondLevelRegions
						)
					);
				}
				// assign secondlevel region for generating first level and second level of select menu
				else if($region->getRegion()->getRegion()->getUid() == $this->parentRegion) {
					$firstLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getUid());
					$secondLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getUid());
					$thirdLevelRegions = $this->regionRepository->findByRegion($region->getUid());

					$this->view->assignMultiple(
						Array(
							'preselectedSecondLevelRegion' => $region,
							'firstLevelRegions' => $firstLevelRegions,
							'secondLevelRegions' => $secondLevelRegions,
							'thirdLevelRegions' => $thirdLevelRegions
						)
					);
				}
				// assign third region for generating first level, second level and third level of select menu
				else if($region->getRegion()->getRegion()->getRegion()->getUid() == $this->parentRegion) {
					$firstLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getRegion()->getUid());
					$secondLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getUid());
					$thirdLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getUid());
					$fourthLevelRegions = $this->regionRepository->findByRegion($region->getUid());

					$this->view->assignMultiple(
						Array(
							'preselectedThirdLevelRegion' => $region,
							'firstLevelRegions' => $firstLevelRegions,
							'secondLevelRegions' => $secondLevelRegions,
							'thirdLevelRegions' => $thirdLevelRegions,
							'fourthLevelRegions' => $fourthLevelRegions
						)
					);
				}
				// assign forth region for generating all levels up to first of select menu
				else if($region->getRegion()->getRegion()->getRegion()->getRegion()->getUid() == $this->parentRegion) {
					$firstLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getRegion()->getRegion()->getUid());
					$secondLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getRegion()->getUid());
					$thirdLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getUid());
					$fourthLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getUid());
					$fithLevelRegions = $this->regionRepository->findByRegion($region->getUid());

					$this->view->assignMultiple(
						Array(
							'preselectedFourthLevelRegion' => $region,
							'firstLevelRegions' => $firstLevelRegions,
							'secondLevelRegions' => $secondLevelRegions,
							'thirdLevelRegions' => $thirdLevelRegions,
							'fourthLevelRegions' => $fourthLevelRegions,
							'fithLevelRegions' => $fithLevelRegions
						)
					);
				}
				// assign forth region for generating all levels up to first of select menu
				else if($region->getRegion()->getRegion()->getRegion()->getRegion()->getRegion()->getUid() == $this->parentRegion) {
					$firstLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getRegion()->getRegion()->getRegion()->getUid());
					$secondLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getRegion()->getRegion()->getUid());
					$thirdLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getRegion()->getUid());
					$fourthLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getRegion()->getUid());
					$fithLevelRegions = $this->regionRepository->findByRegion($region->getRegion()->getUid());

					$this->view->assignMultiple(
						Array(
							'preselectedFithLevelRegion' => $region,
							'firstLevelRegions' => $firstLevelRegions,
							'secondLevelRegions' => $secondLevelRegions,
							'thirdLevelRegions' => $thirdLevelRegions,
							'fourthLevelRegions' => $fourthLevelRegions,
							'fithLevelRegions' => $fithLevelRegions
						)
					);
				}
				else {
					// do nothing
				}
			}
		}
		else {
			$regions = $this->regionRepository->findByRegionForFirstLevel($this->parentRegion);
			$this->view->assign('regions', $regions);
			$this->response->addAdditionalHeaderData('<meta name="robots" content="index, follow">');
		}

		// assign options for adult filter
		$adultSelectFields = Array(1 => '1 Erwachsener');
		for($i = 2; $i <= 20; $i++) {
			$adultSelectFields[$i] = $i . ' Erwachsene';
		}

		// assign options for child filter
		$childSelectFields = Array('kein Kind');
		$childSelectFields[] = '1 Kind';
		for($j = 2; $j <= 20; $j++) {
			$childSelectFields[] = $j . ' Kinder';
		}

		// assign options for child age
		$childSelectAges = Array(0 => 'Alter wählen');
		for($k = 1; $k <= 17; $k++) {
			$childSelectAges[] = $k;
		}

		// find categories for filter
		$categories = $this->categoryRepository->findByType('room');

		// find features for filter
		$features = $this->featureRepository->findFeaturesForSearch();

		$this->view->assignMultiple(Array(
			'adultSelectFields' => $adultSelectFields,
			'childSelectFields' => $childSelectFields,
			'childSelectAges' => $childSelectAges,
			'categories' => $categories,
			'features' => $features
		));
	}

	/**
	 * init search action params
	 */
	public function initializeSearchAction() {
		$this->searchParameter = $this->getSearchParametersFromSession();
	}

	/**
	 * @TODO 4th and 5th level for regions are missing
	 * search for accommodations
	 *
	 * @param int $firstLevelRegion
	 * @param int $secondLevelRegion
	 * @param int $thirdLevelRegion
	 * @param int $fourthLevelRegion
	 * @param int $fifthLevelRegion
	 * @param string $datefrom
	 * @param string $dateto
	 * @param int $adults
	 * @param int $childs
	 * @param int $pets
	 * @param int $restoreSession
	 * @param array $childs_age
	 * @param array $searchCategory
	 * @param array $searchFacility
	 * @param string $sortby
	 *
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function searchAction(
		$firstLevelRegion = 0,
		$secondLevelRegion = 0,
		$thirdLevelRegion = 0,
		$fourthLevelRegion = 0,
		$fifthLevelRegion = 0,
		$datefrom = '',
		$dateto = '',
		$adults = 0,
		$childs = 0,
		$pets = 0,
		$restoreSession = 0,
		$childs_age = Array(),
		$searchCategory = Array(),
		$searchFacility = Array(),
		$sortby = ''
	) {
		$time_start = $this->microtime_float();

		// look for existing wildeast session id
		$wildeastSession = $this->wildeastSessionRepository->getLastSession();
		if($wildeastSession[0] instanceof \Telematika\TmChillio\Domain\Model\WildeastSession) {
			$this->wildeastSessionId = $wildeastSession[0]->getSessionId();
			$wildeastSession[0]->setCreatedAt(time());
			$this->wildeastSessionRepository->update($wildeastSession[0]);
		}

		// collect parameters
		if($restoreSession) {
			$startdate = $this->searchParameter->getDatestart();
			$enddate = $this->searchParameter->getDateend();
			$adults = $this->searchParameter->getAdults();
			//$childs = $this->searchParameter->getChilds();
			$regionUid = $this->searchParameter->getRegion();
			$region = $this->regionRepository->findByUid($regionUid);
			$childParams = Array();
			if($region instanceof \Telematika\TmChillio\Domain\Model\Region) {
				$regionRegId = $region->getRegid();
			}
			else {
				$region = $this->regionRepository->findByUid($this->parentRegion);
				$regionRegId = $region->getRegid();
			}
		}
		else {
			if($this->datesAreValid($datefrom, $dateto)) {
				$startdate = date('Ymd', strtotime($datefrom));
				$enddate = date('Ymd', strtotime($dateto));
			}
			else {
				// @TODO set default date range from tomorrow to 7 days in future in front- and backend
				$startdate = date('Ymd', time() + (1*24*60*60));
				$enddate = date('Ymd', time() + (8*24*60*60));
				$this->addFlashMessage(
					'Es wurde kein korrektes Anreise- und Abreisedatum angegeben.',
					$messageTitle = '',
					$severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR
				);
			}
			$adults = ($adults > 0) ? (int)$adults : 1;
			$childsAgeAndNumber = Array();
			for($k = 0; $k < $childs; $k++) {
				if($childs_age[$k] == 0 || $childs > 20) {
					unset($childsAgeAndNumber);
					$this->addFlashMessage(
						'Das Alter der Kinder wurde nicht korrekt angegeben. Bitte geben Sie für jedes Kind das Alter an.',
						$messageTitle = '',
						$severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
					);
					break;
				}
				if(empty($childsAgeAndNumber[$childs_age[$k]])) {
					$childsAgeAndNumber[$childs_age[$k]] = 1;
				}
				else {
					$childsAgeAndNumber[$childs_age[$k]]++;
				}
			}
			$childParams = Array();
			if(!empty($childsAgeAndNumber)) {
				foreach($childsAgeAndNumber as $age => $number) {
					$childParams[]['children'] = Array(
						'age' => $age,
						'number' => $number
					);
				}
			}

			// get region
			$region = $this->getRegionByLevel(
				$firstLevelRegion,
				$secondLevelRegion,
				$thirdLevelRegion,
				$fourthLevelRegion,
				$fifthLevelRegion
			);

			// save parameter in session
			$this->searchParameter->setDatestart($startdate);
			$this->searchParameter->setDateend($enddate);
			$this->searchParameter->setAdults($adults);
			$this->searchParameter->setChildren($childParams);
			$this->searchParameter->setRegion($region->getUid());

			$this->view->assign('currentRegion', $region);
			// and get wildeast region id
			$regionRegId = $region->getRegid();
		}


		// make request (params are collected for xml based request)
		// see https://zimmer.im-web.de/Schnittstellen/xmlschema/search.xsd
		$params = Array(
			'datestart' => $startdate,
			'dateend' => $enddate,
			'adults' => $adults,
			'region' => Array('region_ids' => Array('region_id' => $regionRegId)),
			'session_id' => $this->wildeastSessionId,
			'number_of_hits' => 25,
			'animals' => $pets
		);
		if(sizeof($childParams) > 0) $params['childrens'] = $childParams;
		if($pets) $params['animals'] = 1;
		if(sizeof($searchCategory) > 0) {
			$acat_positions = Array();
			foreach($searchCategory as $category => $val) {
				$acat_positions[]['acat_position'] = $category;
			}
			$params['acat'] = $acat_positions;
		}
		if(sizeof($searchFacility) > 0) {
			$facilities = Array();
			foreach($searchFacility as $type => $ids) {
				foreach($ids as $id => $val) {
					$facility = Array();
					$facility['type'] = $type;
					$facility['position'] = $id;
					$facility['value'] = 'Y';
					$facilities[]['facility'] = $facility;
				}
			}
			$params['facilities'] = $facilities;
		}

		$notFoundFlashMessage = false; // print flash message only once
		// loop, if no result, jump to parent region until result is found
		$time_start_we = $this->microtime_float();
		do {
			$roomsXML = $this->roomRepository->searchForRooms($params);
			$rooms = json_decode(json_encode($roomsXML), 1);
			$noResults = !is_array($rooms['content']);
			if($noResults) {
				if(!$notFoundFlashMessage) {
					$this->addFlashMessage(
						'Bitte überprüfen Sie Ihre Eingabe, ändern Sie Ihre Such-Parameter oder nutzen Sie einen unserer Vorschläge.',
						$messageTitle = 'Leider konnten wir keine passenden Unterkünfte für die Region "' . $region->getName() . '" finden.',
						$severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
					);
					$notFoundFlashMessage = true;
				}
				// check, if there's a parent region
				if($region->getRegion() instanceof \Telematika\TmChillio\Domain\Model\Region) {
					$params['region'] = Array('region_ids' => Array('region_id' => $region->getRegion()->getRegid()));
					$region = $region->getRegion();
				}
				// no parent region? break loop!!
				else {
					$this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
					break;
				}
			}
			else {
				$this->searchParameter->setRegion($region->getUid());
				$this->saveSearchParameterToSession($this->searchParameter);
			}
		} while($noResults);
		$time_end_we = $this->microtime_float();
		echo "<p>" .($time_end_we - $time_start_we). "Für die Wildeastabfragen</p>";


		// if no wildeast session id is available create an new session entry
		if(strlen($this->wildeastSessionId) <= 1) {
			$wildeastSession = $this->objectManager->get('\Telematika\TmChillio\Domain\Model\WildeastSession');
			$wildeastSession->setSessionId($rooms['session_id']);
			$this->wildeastSessionRepository->add($wildeastSession);
			$this->wildeastSessionId = $rooms['session_id'];
		}

		// get room informations
		$roomIdsForRequest = Array();  // rooms for get[rooms] request (new rooms and rooms, which need update)
		$roomIdsForAssign = Array();  // rooms, which should be assigned (all from get[search])
		$newRooms = Array();  // rooms, which have to be created

		// loop through all room results
		// collect room ids for get[rooms]
		if(is_array($rooms['content']['resultitem'][0])) {
			foreach($rooms['content']['resultitem'] as $room) {
				// collect room ids for assign / result list
				$roomIdsForAssign[] = $room['roomid'];

				// check, if room with room id not exists
				if($this->roomRepository->countByRoomId($room['roomid']) == 0) {
					$newRooms[] = $room['roomid'];
					$roomIdsForRequest[$room['roomid']] = 1;
				}
			}
		}
		else {
			$room = $rooms['content']['resultitem'];

			// collect room ids for assign / result list
			$roomIdsForAssign[] = $room['roomid'];

			// check, if room with room id not exists
			if($this->roomRepository->countByRoomId($room['roomid']) == 0) {
				$newRooms[] = $room['roomid'];
				$roomIdsForRequest[$room['roomid']] = 1;
			}
		}

		// get information about rooms
		if(sizeof($roomIdsForRequest) > 0) {
			$params = Array();
			// make request
			$params['para'] = Array(
				'session_id' => $this->wildeastSessionId,
				'item' => Array(
					'o_id' => 1,
					'o_bezeichnung' => 1,
					'o_strasse' => 1,
					'o_plz' => 1,
					'o_ort' => 1,
					'o_bemerkung' => 1,
					'o_category' => 1,
					'o_gps' => 1,
					'o_bild' => 1,
					'o_haustiere_erlaubt' => 1,
					'o_ausstattung' => 1,
					'z_bezeichnung' => 1,
					'z_bemerkung' => 1,
					'z_bild' => 1,
					'k_min_personen' => 1,
					'k_max_personen' => 1,
					'k_bedrooms' => 1,
					'k_ausstattung' => 1,
					'ab_preis' => 1,
					'k_normal_personen' => 1,
					'k_normal_personen_max' => 1,
				),
				'room' => $roomIdsForRequest
			);
			$roomInformation = $this->roomRepository->getRoomInformation($params);
			$roomInformation = json_decode(json_encode($roomInformation), 1);

			// update/create accommodations/rooms - multiple results
			if(is_array($roomInformation['content']['room'][0])) {
				foreach($roomInformation['content']['room'] as $room) {

					// create rooms
					if(!empty($room['@attributes']['id']) > 0 && in_array($room['@attributes']['id'], $newRooms)) {
						$createdRoom = $this->roomRepository->createRoom($room);

						// create accommodation
						$accommodation = null;
						if($this->accommodationRepository->countByObjectId($room['o_id']) == 0) {
							$accommodation = $this->accommodationRepository->createAccommodation($room);
							// persist new objects
							$this->persistenceManager->persistAll();
						}
						// get existing accommodation
						else {
							$accommodation = $this->accommodationRepository->findByObjectId($room['o_id']);
							$accommodation = $accommodation[0];
						}
						$accommodation->addRoom($createdRoom);
						unset($createdRoom);
						$this->accommodationRepository->update($accommodation);
					}
				}
			}
			// update/create accommodations/rooms - if there's only one result
			else if(is_array($roomInformation['content']['room'])) {
				$room = $roomInformation['content']['room'];

				// create rooms
				if(!empty($room['@attributes']['id']) > 0 && in_array($room['@attributes']['id'], $newRooms)) {
					$createdRoom = $this->roomRepository->createRoom($room);

					// create accommodation
					if($this->accommodationRepository->countByObjectId($room['o_id']) == 0) {
						$accommodation = $this->accommodationRepository->createAccommodation($room);
						// persist new objects
						$this->persistenceManager->persistAll();
					}
					// get existing accommodation
					else {
						$accommodation = $this->accommodationRepository->findByObjectId($room['o_id']);
						$accommodation = $accommodation[0];
					}

					$accommodation->addRoom($createdRoom);
					unset($createdRoom);
					$this->accommodationRepository->update($accommodation);
				}
			}
		}

		// persist all, to display current/new rooms in list
		//$this->persistenceManager->persistAll();
		// get accommodations from db
		//$accommodationsForAssign = $this->accommodationRepository->findByRoomIds($roomIdsForAssign, $sortby);

		$time_start_db = $this->microtime_float();
		$accommodationsForAssign = $this->accommodationRepository->findByRoomIds($roomIdsForAssign, $sortby);

		foreach($accommodationsForAssign as $key => $accommodationForAssign) {
			$accommodationsForAssign[$key]['rooms'] = $this->roomRepository->findByAccommodationUid($accommodationForAssign['accommodation_uid']);
			$accommodationsForAssign[$key]['images'] = $this->imageRepository->findByAccommodationUid($accommodationForAssign['accommodation_uid']);
		}
		$time_end_db = $this->microtime_float();
		echo '<p>' . ($time_end_db - $time_start_db) . ' DB Abfrage - Informationen Wohneinheiten</p>';

		$this->view->assign('accommodations', $accommodationsForAssign);

		$time_end = $this->microtime_float();
		$time = $time_end - $time_start;

		echo "<p>$time Sekunden zur Speicherung und Abfrage der Objektdaten - inclusive der Wildeastabfragen</p>";
	}

	/**
	 * @param int $firstLevelRegion
	 * @param int $secondLevelRegion
	 * @param int $thirdLevelRegion
	 * @param int $fourthLevelRegion
	 * @param int $fifthLevelRegion
	 *
	 * @return object
	 */
	protected function getRegionByLevel(
		$firstLevelRegion = 0,
		$secondLevelRegion = 0,
		$thirdLevelRegion = 0,
		$fourthLevelRegion = 0,
		$fifthLevelRegion = 0
	) {
		// look for fifth level region
		if($fifthLevelRegion > 0) {
			$region = $this->regionRepository->findByUid($fifthLevelRegion);
		}
		else {
			// look for fourth level region
			if($fourthLevelRegion > 0) {
				$region = $this->regionRepository->findByUid($fourthLevelRegion);
			}
			else {
				// look for third level region
				if($thirdLevelRegion > 0) {
					$region = $this->regionRepository->findByUid($thirdLevelRegion);
				}
				else {
					// look for second level region
					if($secondLevelRegion > 0) {
						$region = $this->regionRepository->findByUid($secondLevelRegion);
					}
					// look for first level region
					else {
						$region = ($firstLevelRegion > 0) ? $this->regionRepository->findByUid($firstLevelRegion) : $this->regionRepository->findByUid($this->parentRegion);
					}
				}
			}
		}

		return $region;
	}

	/**
	 * check if:
	 * date has right format
	 * end date is after start date
	 * dates are after time()
	 *
	 * @param string $startDate
	 * @param string $endDate
	 *
	 * @return bool
	 */
	protected function datesAreValid($startDate = '', $endDate = '') {
		$dateRegexp = '/^(0[1-9]|[12][0-9]|3[01])[\.](0[1-9]|1[012])[\.](19|20)\d\d$/';
		if(preg_match($dateRegexp, $startDate) && preg_match($dateRegexp, $endDate)  // dateformat is correct
		   && (strtotime($startDate) < strtotime($endDate))                          // start date is after end date
		   && (strtotime($startDate) > time() && strtotime($endDate) > time())   ) {
			return TRUE;
		}
		else return FALSE;
	}

	/**
	 * init detail action
	 */
	public function initializeDetailAction() {
		$this->searchParameter = $this->getSearchParametersFromSession();
	}

	/**
	 * show details
	 *
	 * @param int $accommodationUid
	 */
	public function detailAction($accommodationUid = 0) {
		$accommodation = $this->accommodationRepository->findByUid($accommodationUid);
		$this->view->assign('accommodation', $accommodation);
	}

	/**
	 * show room details
	 *
	 * @param int $roomUid
	 */
	public function roomDetailAction($roomUid = 0) {
		$room = $this->roomRepository->findByUid($roomUid);
		$this->view->assign('room', $room);
	}

	/**
	 * get room availabilities via ajax
	 *
	 * @param int $roomUid
	 *
	 * @return null|string
	 */
	public function getRoomAvailabilitiesAction($roomUid = 0) {
		// look for existing wildeast session id
		$wildeastSession = $this->wildeastSessionRepository->getLastSession();

		$room = $this->roomRepository->findByUid($roomUid);

		if($room instanceof \Telematika\TmChillio\Domain\Model\Room) {

			$params = Array();
			// make request
			$params['para'] = Array(
				'session_id' => $wildeastSession[0]->getSessionId(),
				'item' => Array(
					'availability' => 1
				),
				'room' => Array($room->getRoomId() => 1)
			);
			$roomInformation = $this->roomRepository->getRoomInformation($params);
			$roomInformation = json_decode(json_encode($roomInformation), 1);
			$this->view->assign('roomInformation', $roomInformation);

			$availabilities = Array();

			foreach($roomInformation['content']['room']['availability']['available'] as $availability) {
				$availabilities[] = Array(
					'arrival' => $availability['@attributes']['arrival'],
					'departure' => $availability['@attributes']['departure']
				);
			}

			return json_encode($availabilities);
		}
		else {
			return null;
		}
	}

	/**
	 * action to list ratings in BE
	 */
	public function listAccommodationRatingsAction() {
		$accommodations = $this->accommodationRepository->findAll();
		$this->view->assign('accommodations', $accommodations);

		//$overallRating = $this->accommodationRepository->getAccommodationRatingSum();
		//$this->view->assign('overallRating', $overallRating);
	}

	/**
	 * update ratings for each accommodation
	 *
	 * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
	 */
	public function updateAccommodationRatingsAction() {
		$this->accommodationRepository->updateAccommodationRatings(Array());
		$this->redirect('listAccommodationRatings');
	}

	/**
	 * init parameters for first booking Step
	 */
	public function initializeBookingFirstStepAction() {
		$this->searchParameter = $this->getSearchParametersFromSession();
		$this->bookingParameter = $this->getBookingParametersFromSession();
	}

	/**
	 * booking first step
	 * select room by given parameters
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Room $room
	 */
	public function bookingFirstStepAction(\Telematika\TmChillio\Domain\Model\Room $room = NULL) {
		$this->bookingParameter->setRoom($room);

		// assign options for adult filter
		$adultSelectFields = Array(1 => '1 Erwachsener');
		for($i = 2; $i <= 20; $i++) {
			$adultSelectFields[$i] = $i . ' Erwachsene';
		}

		// assign options for child filter
		$childSelectFields = Array('kein Kind');
		$childSelectFields[] = '1 Kind';
		for($j = 2; $j <= 20; $j++) {
			$childSelectFields[] = $j . ' Kinder';
		}

		// assign options for child age
		$childSelectAges = Array(0 => 'Alter wählen');
		for($k = 1; $k <= 17; $k++) {
			$childSelectAges[] = $k;
		}

		$this->view->assignMultiple(Array(
			'adultSelectFields' => $adultSelectFields,
			'childSelectFields' => $childSelectFields,
			'childSelectAges' => $childSelectAges
		));

		$datestart = $this->searchParameter->getDatestart();
		$this->bookingParameter->setDatestart($datestart);
		$dateend = $this->searchParameter->getDateend();
		$this->bookingParameter->setDateend($dateend);
		$adults = $this->searchParameter->getAdults();
		$this->bookingParameter->setAdults($adults);
		$childParams = $this->searchParameter->getChildren();
		$region = $this->regionRepository->findByUid($this->searchParameter->getRegion());
		$regionRegId = $region->getRegid();
		$this->bookingParameter->setRegion($region);
		//$pets = $this->searchParameter->getPets();

		if(!empty($room)) {
			// look for existing wildeast session id
			$wildeastSession = $this->wildeastSessionRepository->getLastSession();
			if($wildeastSession[0] instanceof \Telematika\TmChillio\Domain\Model\WildeastSession) {
				$this->wildeastSessionId = $wildeastSession[0]->getSessionId();
				$wildeastSession[0]->setCreatedAt(time());
				$this->wildeastSessionRepository->update($wildeastSession[0]);
			}

			// make request
			$params = Array(
				'datestart' => date('Ymd', strtotime($datestart)),
				'dateend' => date('Ymd', strtotime($dateend)),
				'adults' => $adults,
				'session_id' => $this->wildeastSessionId,
				'region' => Array('region_ids' => Array('region_id' => $regionRegId)),
				'z_ids' => Array('z_id' => $room->getRoomId())
			);
			if(sizeof($childParams) > 0) $params['childrens'] = $childParams;

			$roomInformation = $this->roomRepository->searchForRooms($params);
			$roomInformation = json_decode(json_encode($roomInformation), 1);

			foreach($roomInformation['content']['resultitem']['exclusive']['excl'] as $key => $excl) {
				$roomInformation['content']['resultitem']['exclusive']['excl'][$key]['attributes'] = $excl['@attributes'];
			}
		}
		else {
			$roomInformation = Array();
		}

		$this->view->assignMultiple(
			Array(
				'datestart' => date('d.m.Y', strtotime($datestart)),
				'dateend' => date('d.m.Y', strtotime($dateend)),
				'adults' => $adults,
				'room' => $room,
				'roomInformation' => $roomInformation
			)
		);

		$this->bookingParameter->setRoomInformation($roomInformation);
		$this->saveBookingParameterToSession($this->bookingParameter);
	}

	/**
	 * init second booking step
	 */
	public function initializeBookingSecondStepAction() {
		$this->searchParameter = $this->getSearchParametersFromSession();
		$this->bookingParameter = $this->getBookingParametersFromSession();
	}

	/**
	 * second booking step
	 *
	 * @param string $datestart
	 * @param string $dateend
	 * @param int $adults
	 * @param int $childs
	 * @param array $childs_age
	 * @param \Telematika\TmChillio\Domain\Model\Room $room
	 * @param array $exclusives
	 *
	 * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
	 */
	public function bookingSecondStepAction(
		$datestart = '',
		$dateend = '',
		$adults = 0,
		$childs = 0,
		$childs_age = Array(),
		\Telematika\TmChillio\Domain\Model\Room $room = null,
		$exclusives = Array()
	) {
		// check date and save it
		if($this->datesAreValid($datestart, $dateend)) {
			$this->bookingParameter->setDatestart($datestart);
			$this->bookingParameter->setDateend($dateend);
		}

		// save exclusives
		$this->bookingParameter->setExclusives($exclusives);

		// save adults
		$adults = ($adults > 0) ? (int)$adults : 1;
		$this->bookingParameter->setAdults($adults);

		// save children and count it according to age numbers
		$childsAgeAndNumber = Array();
		for($k = 0; $k < $childs; $k++) {
			if($childs_age[$k] == 0 || $childs > 20) {
				unset($childsAgeAndNumber);
				$this->addFlashMessage(
					'Das Alter der Kinder wurde nicht korrekt angegeben. Bitte geben Sie für jedes Kind das Alter an.',
					$messageTitle = '',
					$severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
				);
				$this->redirect('bookingFirstStep', 'Accommodation', 'tm_chillio', Array('room' => $room));
				break;
			}
			if(empty($childsAgeAndNumber[$childs_age[$k]])) {
				$childsAgeAndNumber[$childs_age[$k]] = 1;
			}
			else {
				$childsAgeAndNumber[$childs_age[$k]]++;
			}
		}
		$childParams = Array();
		if(!empty($childsAgeAndNumber)) {
			foreach($childsAgeAndNumber as $age => $number) {
				$childParams[]['children'] = Array(
					'age' => $age,
					'number' => $number
				);
			}
		}
		$this->bookingParameter->setChildren($childParams);
		$this->saveBookingParameterToSession($this->bookingParameter);
	}

	/**
	 * initialize final step
	 */
	public function initializeBookingFinalStepAction() {
		$this->searchParameter = $this->getSearchParametersFromSession();
		$this->bookingParameter = $this->getBookingParametersFromSession();
	}

	/**
	 * booking final step
	 *
	 * @param array $formData
	 */
	public function bookingFinalStepAction($formData = Array()) {
		if(sizeof($formData) > 0) {
			// collect form data
			$this->bookingParameter->setSalutation($formData['salutation']);
			$this->bookingParameter->setLastname($formData['lastname']);
			$this->bookingParameter->setFirstname($formData['firstname']);
			$this->bookingParameter->setStreet($formData['street']);
			$this->bookingParameter->setZipcode($formData['zip']);
			$this->bookingParameter->setCity($formData['city']);
			$this->bookingParameter->setCountry($formData['country']);
			$this->bookingParameter->setEmail($formData['email']);
			$this->bookingParameter->setPhone($formData['phone']);
			$this->bookingParameter->setMessage($formData['message']);

			$this->saveBookingParameterToSession($this->bookingParameter);

			// calculate number of children
			$numberOfChildren = 0;
			foreach($this->bookingParameter->getChildren() as $children) {
				$numberOfChildren += $children['children']['number'];
			}

			$selectedExclusives = $this->bookingParameter->getExclusives();
			$pricing = Array();
			if(sizeof($selectedExclusives) > 0) {
				$roomInformation = $this->bookingParameter->getRoomInformation();
				$pricing = $this->calculatePricing(
					$selectedExclusives,
					$roomInformation['content']['resultitem']['exclusive']['excl'],
					$roomInformation['content']['resultitem']['sumprice']
				);
			}

			$this->view->assignMultiple(
				Array(
					'bookingParameter' => $this->bookingParameter,
					'overnightStays' => ($this->bookingParameter->getDateend() - $this->bookingParameter->getDatestart()),
					'numberOfChildren' => $numberOfChildren,
					'pricing' => $pricing
				)
			);
		}
	}

	/**
	 * intialize finishing booking proccess
	 */
	public function initializeBookingCompleteStepAction() {
		$this->bookingParameter = $this->getBookingParametersFromSession();
	}

	/**
	 * finish booking proccess
	 */
	public function bookingCompleteStepAction() {
		$params = Array(
			'k_vorname' => $this->bookingParameter->getFirstname(),
			'k_name' => $this->bookingParameter->getLastname(),
			'k_strasse' => $this->bookingParameter->getStreet(),
			'k_plz' => $this->bookingParameter->getZipcode(),
			'k_ort' => $this->bookingParameter->getCity(),
			'b_bemerkung' => $this->bookingParameter->getMessage(),
			'datestart' => Array($this->bookingParameter->getDatestart()),
			'dateend' => Array($this->bookingParameter->getDatestart()),
			'adults' => Array($this->bookingParameter->getAdults()),
			'zusatz' => $this->bookingParameter->getExclusives(),
			'children' => $this->bookingParameter->getChildren()
		);

		//$this->roomRepository->createBooking($params);
	}

	/**
	 * initialize get pricing for booking
	 */
	public function initializeGetPricingForRoomAction() {
		$this->bookingParameter = $this->getBookingParametersFromSession();
	}

	/**
	 * get pricing for a room
	 */
	public function getPricingForRoomAction() {
		$datestart = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('datestart');
		$dateend = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('dateend');
		$adults = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('adults');
		$adults = (int)$adults;
		$childs = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('childs');
		$childs = (int)$childs;
		$childs_age = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('child_age');

		if($this->bookingParameter->getRoom() instanceof \Telematika\TmChillio\Domain\Model\Room && $this->bookingParameter->getRoom() != null) {
			$roomId = $this->bookingParameter->getRoom()->getRoomId();


			// save children and count it according to age numbers
			$childsAgeAndNumber = Array();
			for($k = 0; $k < $childs; $k++) {
				if($childs_age[$k] == 0 || $childs > 20) {
					unset($childsAgeAndNumber);
					$this->addFlashMessage(
						'Das Alter der Kinder wurde nicht korrekt angegeben. Bitte geben Sie für jedes Kind das Alter an.',
						$messageTitle = '',
						$severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING
					);
					break;
				}
				if(empty($childsAgeAndNumber[$childs_age[$k]])) {
					$childsAgeAndNumber[$childs_age[$k]] = 1;
				}
				else {
					$childsAgeAndNumber[$childs_age[$k]]++;
				}
			}
			$childParams = Array();
			if(!empty($childsAgeAndNumber)) {
				foreach($childsAgeAndNumber as $age => $number) {
					$childParams[]['children'] = Array(
						'age' => $age,
						'number' => $number
					);
				}
			}

			// look for existing wildeast session id
			$wildeastSession = $this->wildeastSessionRepository->getLastSession();
			if($wildeastSession[0] instanceof \Telematika\TmChillio\Domain\Model\WildeastSession) {
				$this->wildeastSessionId = $wildeastSession[0]->getSessionId();
				$wildeastSession[0]->setCreatedAt(time());
				$this->wildeastSessionRepository->update($wildeastSession[0]);
			}

			// make request
			$params = Array(
				'datestart' => date('Ymd', strtotime($datestart)),
				'dateend' => date('Ymd', strtotime($dateend)),
				'adults' => $adults,
				'session_id' => $this->wildeastSessionId,
				'region' => Array('region_ids' => Array('region_id' => $this->bookingParameter->getRegion()->getRegid())),
				'z_ids' => Array('z_id' => $this->bookingParameter->getRoom()->getRoomId())
			);
			if(sizeof($childParams) > 0) $params['childrens'] = $childParams;

			$roomsXML = $this->roomRepository->searchForRooms($params);
			$roomInformation = json_decode(json_encode($roomsXML), 1);
			foreach($roomInformation['content']['resultitem']['exclusive']['excl'] as $key => $excl) {
				$roomInformation['content']['resultitem']['exclusive']['excl'][$key]['attributes'] = $excl['@attributes'];
			}
			$this->view->assign('roomInformation', $roomInformation);
			$this->bookingParameter->setRoomInformation($roomInformation);
			$this->saveBookingParameterToSession($this->bookingParameter);
		}
	}

	/**
	 * calculate pricing including exclusives for final booking step
	 *
	 * result has structure:
	 *
	 * pricing =>
	 *      positions =>
	 *          0 =>
	 *              name => ...
	 *              number => ...
	 *              price => ...
	 *              totalprice => ...
	 *          1 => ...
	 *      totalPrice => 19.99
	 *
	 * @param array $selectedExclusives
	 * @param array $exclusives
	 * @param $sumPrice
	 *
	 * @return array
	 */
	protected function calculatePricing($selectedExclusives = Array(), $exclusives = Array(), $sumPrice) {
		$pricing = Array();

		foreach($exclusives as $exclusive) {
			if(array_key_exists($exclusive['@attributes']['id'], $selectedExclusives)) {
				$pricing['positions'][] = Array(
					'name' => $exclusive['name'],
					'number' => $selectedExclusives[$exclusive['@attributes']['id']],
					'price' => $exclusive['sumprice'],
					'totalprice' => ($selectedExclusives[$exclusive['@attributes']['id']] * $exclusive['sumprice'])
				);
				$sumPrice += $selectedExclusives[$exclusive['@attributes']['id']] * $exclusive['sumprice'];
			}
		}
		$pricing['totalPrice'] = $sumPrice;

		return $pricing;
	}

	public function initializeWishlistAction() {
		$this->wishlist = $this->getWishlistsFromSession();
	}

	/**
	 * display wishlist
	 */
	public function wishlistAction() {
		$objectIds = $this->wishlist->getObjectIds();

		$accommodations = $this->accommodationRepository->findByObjectIds($objectIds);
	}

	/**
	 * init "add to wishlist"
	 */
	public function initializeAddToWishListAction() {
		$this->wishlist = $this->getWishlistsFromSession();
	}

	/**
	 * add an object by its object id to wish list
	 *
	 * @param int $objectId
	 */
	public function addToWishListAction($objectId = 0) {
		$accommodation = $this->accommodationRepository->findByObjectId($objectId);
		$accommodation = $accommodation[0];

		if($accommodation instanceof \Telematika\TmChillio\Domain\Model\Accommodation) {
			$this->wishlist->attachObject($objectId);
		}
	}

	/**
	 * remove object by its object id from wish list
	 *
	 * @param int $objectId
	 */
	public function removeFromWishlistAction($objectId = 0) {

	}

	/**
	 * search form on start page
	 */
	public function showStartpageFormAction() {}

	/**
	 * get microtime as float
	 * @return float
	 */
	private function microtime_float() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

}