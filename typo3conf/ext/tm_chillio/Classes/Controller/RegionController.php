<?php
namespace Telematika\TmChillio\Controller;


	/***************************************************************
	 *
	 *  Copyright notice
	 *
	 *  (c) 2015 Thomas Reichwein <reichwein@telematika.de>, Telematika GmbH
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * AccommodationController
 */
class RegionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * regionRepository
	 *
	 * @var \Telematika\TmChillio\Domain\Repository\RegionRepository
	 * @inject
	 */
	protected $regionRepository = NULL;

	/**
	 * initialization
	 */
	public function initializeAction() {
		$this->parentRegion = $this->regionRepository->findByUid($this->settings['highestLvlRegion']);
	}

	/**
	 * list regions
	 */
	public function listAction() {
		$regions = $this->regionRepository->findAll();
		$this->view->assign('regions', $regions);
	}

	/**
	 * get sub regions via ajax (difference is in template)
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 */
	public function get2ndLevelRegionsAction(\Telematika\TmChillio\Domain\Model\Region $region) {
		$regions = $this->regionRepository->findByRegion($region);
		$this->view->assign('regions', $regions);
	}

	/**
	 * get sub regions via ajax (difference is in template)
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 */
	public function get3rdLevelRegionsAction(\Telematika\TmChillio\Domain\Model\Region $region) {
		$regions = $this->regionRepository->findByRegion($region);
		$this->view->assign('regions', $regions);
	}

	/**
	 * get sub regions via ajax (difference is in template)
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 */
	public function get4thLevelRegionsAction(\Telematika\TmChillio\Domain\Model\Region $region) {
		$regions = $this->regionRepository->findByRegion($region);
		$this->view->assign('regions', $regions);
	}

	/**
	 * get sub regions via ajax (difference is in template)
	 *
	 * @param \Telematika\TmChillio\Domain\Model\Region $region
	 */
	public function get5thLevelRegionsAction(\Telematika\TmChillio\Domain\Model\Region $region) {
		$regions = $this->regionRepository->findByRegion($region);
		$this->view->assign('regions', $regions);
	}

	/**
	 * show landing text from region object given by url params
	 */
	public function showLandingTextAction() {
		// look for preselected region given by url
		$preselectedRegion = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('region');
		$preselectedRegionUid = (int)($preselectedRegion);

		// region is selected
		if($preselectedRegion) {
			// get region model
			$region = $this->regionRepository->findByUid( $preselectedRegionUid );

			$this->view->assign('region', $region);
		}
	}

	/**
	 * @return string
	 */
	public function searchForRegionsAction() {
		$search = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('search');
		$regions = $this->regionRepository->findBySearchString($search);

		$regionList = Array();
		foreach($regions as $region) {
			$regionList[] = $region['name'];
		}

		return json_encode($regionList, 1);
	}
}