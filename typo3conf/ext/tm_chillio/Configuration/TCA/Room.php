<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_tmchillio_domain_model_room'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_tmchillio_domain_model_room']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'title, remark, room_id, category, min_person, max_person, image, feature',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, title, remark, room_id, min_person, max_person, category, image, feature'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_tmchillio_domain_model_room',
				'foreign_table_where' => 'AND tx_tmchillio_domain_model_room.pid=###CURRENT_PID### AND tx_tmchillio_domain_model_room.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'remark' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.remark',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'room_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.room_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'category' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.category',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_category',
				'minitems' => 0,
				'maxitems' => 9999,
			),
		),
		'min_person' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.min_person',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'max_person' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.max_person',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'bedrooms' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.bedrooms',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'min_price' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.min_price',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.image',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_image',
				'MM' => 'tx_tmchillio_room_image_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_tmchillio_domain_model_image',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'feature' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.feature',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_feature',
				'MM' => 'tx_tmchillio_room_feature_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_tmchillio_domain_model_feature',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),

		'size' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_room.size',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		
		'accommodation' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
