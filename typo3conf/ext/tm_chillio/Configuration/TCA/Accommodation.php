<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_tmchillio_domain_model_accommodation'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_tmchillio_domain_model_accommodation']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, object_id, title, street, plz, city, remark, pets, gps, category, room, image, feature',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, object_id, title, street, plz, city, remark;;;richtext:rte_transform[mode=ts_links], pets, gps, category, room, image, feature, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_tmchillio_domain_model_accommodation',
				'foreign_table_where' => 'AND tx_tmchillio_domain_model_accommodation.pid=###CURRENT_PID### AND tx_tmchillio_domain_model_accommodation.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'object_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.object_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'street' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.street',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'plz' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.plz',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'city' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'remark' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.remark',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'pets' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.pets',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			)
		),
		'longitude' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.longitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'latitude' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.latitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'rating_overall' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.rating_overall',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'votes' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.votes',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'category' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.category',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_category',
				'MM' => 'tx_tmchillio_accommodation_category_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_tmchillio_domain_model_feature',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'room' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.room',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_tmchillio_domain_model_room',
				'foreign_field' => 'accommodation',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'region' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.region',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_tmchillio_domain_model_region',
				'minitems' => 0,
				'maxitems' => 1,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.image',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_image',
				'MM' => 'tx_tmchillio_accommodation_image_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_tmchillio_domain_model_image',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'feature' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.feature',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_feature',
				'MM' => 'tx_tmchillio_accommodation_feature_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_tmchillio_domain_model_feature',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'rating' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_accommodation.rating',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_tmchillio_domain_model_rating',
				'MM' => 'tx_tmchillio_accommodation_rating_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_tmchillio_domain_model_rating',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		
	),
);
