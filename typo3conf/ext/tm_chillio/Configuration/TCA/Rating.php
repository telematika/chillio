<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_tmchillio_domain_model_rating'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_tmchillio_domain_model_rating']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'id, points',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, name, id, type, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_tmchillio_domain_model_rating',
				'foreign_table_where' => 'AND tx_tmchillio_domain_model_rating.pid=###CURRENT_PID### AND tx_tmchillio_domain_model_rating.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_rating.id',
			'config' => array(
				'type' => 'input',
				'size' => 4
			)
		),
		'points' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_rating.points',
			'config' => array(
				'type' => 'input',
				'size' => 4
			),
		),

	),
);
