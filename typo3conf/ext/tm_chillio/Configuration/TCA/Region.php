<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_tmchillio_domain_model_region'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_tmchillio_domain_model_region']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'hidden, name, page_title, indexing, meta_description, landing_text, region',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, name, page_title, indexing, meta_description, landing_text;;;richtext:rte_transform[flag=rte_enabled|mode=ts], region'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_tmchillio_domain_model_region',
				'foreign_table_where' => 'AND tx_tmchillio_domain_model_region.pid=###CURRENT_PID### AND tx_tmchillio_domain_model_region.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'regid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.regid',
			'config' => array(
				'type' => 'input',
				'size' => 255,
				'eval' => 'trim'
			),
		),
		'zipcode' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.zipcode',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'longtitude' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.longtitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'latitude' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.latitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'region' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.region',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_tmchillio_domain_model_region',
				'minitems' => 0,
				'maxitems' => 1,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'accommodation' => array(
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.accommodation',
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'landing_text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.landing_text',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'meta_description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.meta_description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 4,
				'eval' => 'trim'
			),
		),
		'page_title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.page_title',
			'config' => array(
				'type' => 'input',
				'size' => 60,
				'eval' => 'trim'
			),
		),
		'indexing' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:tm_chillio/Resources/Private/Language/locallang_db.xlf:tx_tmchillio_domain_model_region.indexing',
			'config' => array(
				'type' => 'select',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('nicht durch Suchmaschine indexieren', 0),
					array('durch Suchmaschine indexieren', 1)
				),
			),
		),
		
	),
);
