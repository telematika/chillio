#############################################################
##########       Allgemeine Einstellungen          ##########
#############################################################
config {
    doctype = html5
    xmlprologue = none
    xhtml_cleaning = all
    extTarget =
    noPageTitle = 0

    sys_language_overlay = hideNonTranslated
    linkVars = L

    # Standardsprache einstellen
    sys_language_uid = 0
    language = de
    locale_all = de_DE
    htmlTag_langKey = de

    # E-Mail-Adressen im FE verschlüsseln
    spamProtectEmailAddresses = 2
    spamProtectEmailAddresses_atSubst = <span>&#064;</span>

    # RealURL aktivieren
    simulateStaticDocuments = 0
    baseURL = {$baseUrl}
    tx_realurl_enable = 1
    prefixLocalAnchors = all

    #bodyTag = <body class="home">

    index_enable = 1
    compressCss = 0
    concatenateCss = 0
    compressJs = 0
    concatenateJs = 0
}

# doctype switch for ie8; yes, we love ie8
[browser = msie] && [version = 8]
    config.doctype = xhtml_trans
[global]

lib.stdheader.10.setCurrent.htmlSpecialChars.preserveEntities=1