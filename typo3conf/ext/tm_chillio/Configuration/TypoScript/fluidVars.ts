############################################
### search form with filters
############################################
lib.tm_chillio_search_form = USER
lib.tm_chillio_search_form {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = SearchForm
    vendorName = Telematika
    extensionName = TmChillio
    controller = Accommodation
    action = searchForm
    switchableControllerActions {
        Accommodation {
            1 = searchForm
            2 = search
        }
        Region {
            1 = get3rdLevelRegions
            2 = get2ndLevelRegions
        }
    }

    settings =< plugin.tx_tmchillio.settings
    persistence =< plugin.tx_tmchillio.persistence
    view =< plugin.tx_tmchillio.view
}
############################################
### landing text for region urls
############################################
lib.tm_chillio_landing_text = USER
lib.tm_chillio_landing_text {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = LandingPage
    vendorName = Telematika
    extensionName = TmChillio
    controller = Region
    action = showLandingText
    switchableControllerActions {
        Region {
            1 = showLandingText
        }
    }

    settings =< plugin.tx_tmchillio.settings
    persistence =< plugin.tx_tmchillio.persistence
    view =< plugin.tx_tmchillio.view
}
############################################
### start page search
############################################
lib.tm_chillio_startpage_form = USER
lib.tm_chillio_startpage_form {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    pluginName = StartpageForm
    vendorName = Telematika
    extensionName = TmChillio
    controller = Accommodation
    action = showStartpageForm
    switchableControllerActions {
        Accommodation {
            1 = showStartpageForm
        }
        Region {
            1 = searchForRegions
        }
    }

    settings =< plugin.tx_tmchillio.settings
    persistence =< plugin.tx_tmchillio.persistence
    view =< plugin.tx_tmchillio.view
}
############################################
### content columns
############################################
lib.special_first_col = CONTENT
lib.special_first_col {
    table = tt_content
    select {
        where = colPos = 0
        andWhere.dataWrap =  hidden=0 AND (sys_language_uid=-1 OR sys_language_uid={TSFE:sys_language_uid})
    }
    wrap = |
#slide = -1
}
lib.content_second_col = CONTENT
lib.content_second_col {
    table = tt_content
    select {
        where = colPos=1
        andWhere.dataWrap =  hidden=0 AND (sys_language_uid=-1 OR sys_language_uid={TSFE:sys_language_uid})
    }
    wrap = <!--TYPO3SEARCH_begin--> | <!--TYPO3SEARCH_end-->
#slide = -1
}
lib.content_third_col = CONTENT
lib.content_third_col {
    table = tt_content
    select {
        where = colPos=2
        andWhere.dataWrap =  hidden=0 AND (sys_language_uid=-1 OR sys_language_uid={TSFE:sys_language_uid})
    }
    wrap = <!--TYPO3SEARCH_begin--> | <!--TYPO3SEARCH_end-->
#slide = -1
}
lib.content_fourth_col = CONTENT
lib.content_fourth_col {
    table = tt_content
    select {
        where = colPos=3
        andWhere.dataWrap =  hidden=0 AND (sys_language_uid=-1 OR sys_language_uid={TSFE:sys_language_uid})
    }
    wrap = <!--TYPO3SEARCH_begin--> | <!--TYPO3SEARCH_end-->
#slide = -1
}
lib.content_fifth_col = CONTENT
lib.content_fifth_col {
    table = tt_content
    select {
        where = colPos=4
        andWhere.dataWrap =  hidden=0 AND (sys_language_uid=-1 OR sys_language_uid={TSFE:sys_language_uid})
    }
    wrap = <!--TYPO3SEARCH_begin--> | <!--TYPO3SEARCH_end-->
#slide = -1
}


lib.bg_image_top = COA
lib.bg_image_top {
    10 = IMG_RESOURCE
    10 {
        file {
        import.data = levelmedia:-1, slide
            treatIdAsReference = 1
        import.listNum = 0
        }
        renderObj = COA
        renderObj {
            10 = IMG_RESOURCE
            10 {
                required = 1
            }
        }
    }
}
lib.bg_image_bottom = COA
lib.bg_image_bottom {
    10 = IMG_RESOURCE
    10 {
        file {
        import.data = levelmedia:-1, slide
            treatIdAsReference = 1
        import.listNum = 1
        }
        renderObj = COA
        renderObj {
            10 = IMG_RESOURCE
            10 {
                required = 1
            }
        }
    }
}
lib.TopmenuStart = HMENU
lib.TopmenuStart {
    special = list
    special.value = 21, 20, 4

    1 = TMENU
    1 {
        wrap = <ul class="nav navbar-nav">|</ul>
        noBlur = 1
        expAll = 1

        NO = 1
        NO {
            wrapItemAndSub = <li>|</li>
            std.dataWrap= |
        }

    }
}

lib.FooterStart = HMENU
lib.FooterStart {
    special = directory
    special.value = 17
    excludeUidList = 1, 6, 21, 20

    1 = TMENU
    1 {
        noBlur = 1
        expAll = 1
        NO = 1
        NO{
            linkWrap = <div class="footer-column"><h2>|</h2>

        }
    }
    2 < .1
    2{
        NO{
            linkWrap = <li>|</li>
        }
        wrap = <ul>|</ul></div>
    }


}

lib.bgimage = COA
lib.bgimage {
    10 = IMG_RESOURCE
    10 {
        file {
        import.data = levelmedia:-1, slide
            treatIdAsReference = 1
        import.listNum = 0
        }
        renderObj = COA
        renderObj {
            10 = IMG_RESOURCE
            10 {
                required = 1
            }
        }
    }
}

############################################
### logo
############################################
lib.logo = TEXT
lib.logo {
    typolink.parameter = 17
    typolink.ATagParams = class="navbar-brand"
    value (
        <img src="typo3conf/ext/tm_chillio/Resources/Public/img/chillio_logo_slogan.png" alt="Chillio Logo" title="Chillio Logo">
        <h4>Wir machen URLAUB</h4>
        <h5>Buchen Sie hier Ihre Ferienunterkunft</h5>
    )
}

