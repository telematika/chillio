############################################
# Basic config
############################################
page = PAGE
page {
    # Favicon einbinden
    #shortcutIcon = fileadmin/templates/img/favicon.ico

    # utf8
    config.metaCharset = utf-8
    config.additionalHeaders = Content-Type:text/html;charset=utf-8


    # Verwendung von Fluidtemplates aktivieren
    #10 = FLUIDTEMPLATE

    10 = CASE
    10 {
        key.field = backend_layout
        key.ifEmpty.data = levelfield:-2, backend_layout_next_level, slide
        default = FILE
        default.file = typo3conf/ext/tm_chillio/Resources/Private/Templates/Page/t3fluid_standardseite.html

        1 = FLUIDTEMPLATE
        1.file = typo3conf/ext/tm_chillio/Resources/Private/Templates/Page/t3fluid_startseite.html

        2 = FLUIDTEMPLATE
        2.file = typo3conf/ext/tm_chillio/Resources/Private/Templates/Page/t3fluid_suchseite.html

        3 = FLUIDTEMPLATE
        3.file = typo3conf/ext/tm_chillio/Resources/Private/Templates/Page/t3fluid_standardseite.html

        4 = FLUIDTEMPLATE
        4.file = typo3conf/ext/tm_chillio/Resources/Private/Templates/Page/t3fluid_specialseite.html
    }

    meta {
        viewport = width=device-width, initial-scale=1
        keywords.field = keywords
        robots = noindex, nofollow
    }

    config.index_enable = 1
    config.no_cache = 0
}
[globalVar = TSFE:id = 6]
page.meta.robots >
[global]

############################################
# include CSS/JS
############################################
page {
    # js
    includeJS {
        jQuery = typo3conf/ext/tm_chillio/Resources/Public/js/jquery-1.11.2.min.js
        jQueryUi = typo3conf/ext/tm_chillio/Resources/Public/js/jquery-ui.min.js
        jQueryHoverIntent = typo3conf/ext/tm_chillio/Resources/Public/js/jquery.hoverIntent.minified.js
        #openLayer = typo3conf/ext/tm_chillio/Resources/Public/js/OpenLayers.js
        #openStreetMap = typo3conf/ext/tm_chillio/Resources/Public/js/OpenStreetMap.js
        #openStreetMapFunctions = typo3conf/ext/tm_chillio/Resources/Public/js/OpenStreetMapFunctions.js
        #mapbox = https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js
        slick = typo3conf/ext/tm_chillio/Resources/Public/js/slick.min.js
        easing = typo3conf/ext/tm_chillio/Resources/Public/js/jquery.easing.js
        bootstrapJS = typo3conf/ext/tm_chillio/Resources/Public/js/bootstrap.min.js
        validateJS = typo3conf/ext/tm_chillio/Resources/Public/js/jquery.validate.min.js
        localizaionValidateJS = typo3conf/ext/tm_chillio/Resources/Public/js/localization/messages_de.min.js
        common = typo3conf/ext/tm_chillio/Resources/Public/js/common.js
        #jQueryMobile = typo3conf/ext/tm_chillio/Resources/Public/js/jquery.mobile-1.4.5.min.js

    }

    # css
    includeCSS {
        #jQueryUi = typo3conf/ext/tm_chillio/Resources/Public/css/jquery-ui.css
        #jQueryUiTheme = typo3conf/ext/tm_chillio/Resources/Public/css/jquery-ui.theme.css
        #mapboxCSS = https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css
        slick = typo3conf/ext/tm_chillio/Resources/Public/css/slick.css
        bootstrapCSS = typo3conf/ext/tm_chillio/Resources/Public/css/bootstrap.min.css
       # colorboxCSS = typo3conf/ext/tm_chillio/Resources/Public/css/colorbox.css
        markerclusterCSS = typo3conf/ext/tm_chillio/Resources/Public/css/MarkerCluster.css
        fontAwesomeCSS = typo3conf/ext/tm_chillio/Resources/Public/font-awesome/css/font-awesome.min.css
        mainCSS = typo3conf/ext/tm_chillio/Resources/Public/css/style.css
    }
}

############################################
# Layout Switch
############################################
[globalVar = TSFE:page|backend_layout = 2]
page.10.file = typo3conf/ext/tm_chillio/Resources/Private/Templates/Page/t3fluid_suchseite.html
page.includeJS {
    leaflet = http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js
    #chillioMap = typo3conf/ext/tm_chillio/Resources/Public/js/chillioMap.js
    markercluster = typo3conf/ext/tm_chillio/Resources/Public/js/leaflet.markercluster-src.js
}
page.includeCSS {
    leafletCSS = http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css
}
[global]