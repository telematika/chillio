<?php

/**
 * Base TCA generation for the table tt_content
 */

$GLOBALS['TCA']['tt_content'] = \HDNET\Autoloader\Utility\ModelUtility::getTcaOverrideInformation('chillio_content', 'tt_content');

// custom manipulation calls here

$custom = array(
	'columns' => array(
		'chillio_title' => array(
			'label' => 'Überschrift'
		),
		'bodytext' => array(
			'defaultExtras' => 'richtext[]'
		),
		'image' => array(
			'config' => array(
				'maxitems' => 1
			)
		)
	)
);

$GLOBALS['TCA']['tt_content'] = \HDNET\Autoloader\Utility\ArrayUtility::mergeRecursiveDistinct($GLOBALS['TCA']['tt_content'], $custom);